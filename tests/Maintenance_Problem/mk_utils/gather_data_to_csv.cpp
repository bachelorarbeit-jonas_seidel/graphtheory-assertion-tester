#include <fstream>
#include <vector>
#include <filesystem>

#include "../../../Specialization/LP_Problems/Maintenance_Problem/Testing/maintenance_problem_testing.h"

// make sure the requested models actually exist in the parsed data. Otherwise this in csv(...) at the latest.
int main(int argc, char** argv){
  assert(argc > 3);
  std::filesystem::path top_level_path(argv[1]);
  std::string x_name(argv[2]);
  std::string y_name(argv[3]);
  std::string out = "data";

  std::vector<std::string> models;
  int index = 4;
  bool spec = false;
  for(; index < argc; index++){
    if(std::string(argv[index]) == "-o") {spec = true; break;}
    models.push_back(argv[index]);
  }
  if(spec){
    assert(argc == index+2);
    out = std::string(argv[index+1]);
  }

  std::vector<Data> data_vector;
  gather_data(top_level_path, data_vector);

  std::fstream file_avg;
  std::stringstream avg_name;
  avg_name << out << "_avg.csv";
  file_avg.open(avg_name.str(), std::ios::out);

  std::fstream file_median;
  std::stringstream median_name;
  median_name << out << "_median.csv";
  file_median.open(median_name.str(), std::ios::out);

  std::cout << "sorting data according to raster positions" << std::endl;
  auto data_sorted_by_pos = sort_data_by_raster_position(data_vector, axis_data{x_name}, axis_data{y_name});

  std::cout << "finished reading; writing averaged gathered data to \"" << avg_name.str() << "\"" << std::endl;
  sorted_data_to_csv(file_avg, std::move(data_sorted_by_pos), axis_data{x_name}, axis_data{y_name}, models, true);
  std::cout << "finished reading; writing medians of gathered data to \"" << median_name.str() << "\"" << std::endl;
  sorted_data_to_csv(file_median, std::move(data_sorted_by_pos), axis_data{x_name}, axis_data{y_name}, models, false);

  file_avg.close();
  file_median.close();
}
