#include "../../../Specialization/LP_Problems/Maintenance_Problem/Testing/maintenance_problem_testing.h"
#include "../mk_utils/probe_test.cpp"
#include <cmath>

int main(int argc, char** argv){
  bool just_probe = false;
  size_t number_of_instances_per_coord = 0;

  assert(argc > 1);
  std::string path(argv[1]);
  if(path == "probe"){
    just_probe = true;
  }else{
    assert(argc == 3);
    number_of_instances_per_coord = std::atoi(argv[2]);
  }

  size_t nnodes = 40;
  size_t nedges_critical = 250;

  std::function<maintenance_problem_generator(double, double)> test_generator = [&nnodes,&nedges_critical](double share_of_critical, double connector_capacity){
    double nedges_non_critical = round(((double)nedges_critical)*(1/share_of_critical-1)/(nnodes-2));
    double nnodes_subnetwork = round(nnodes*nedges_non_critical/nedges_critical)+1;
    return maintenance_problem_generator(
      random_graph_generator(
                    // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
        tipping_policy(false),
                      //   number_of_nodes      number_of_steps   node_attribute_generator
        uniform_node_steps(nnodes,                  1,                {{}}),
                      //          number_of_edges                    fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
        uniform_edge_step_fuzzing(nedges_critical,         0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
        tipping_policy(false),
        //                 number_of_nodes number_of_steps   node_attribute_generator
        uniform_node_steps(nnodes_subnetwork,              1,                {{}}),
        //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
        uniform_edge_step_fuzzing(nedges_non_critical,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   false, true),
        {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
        {{"Nodepotential", Attribute(min, 0)}},
        {{{"Upper", {fix, Integral, 1, connector_capacity}}}},
        {},
        {{{"Upper", {fix, Integral, 1, 100}}}}
      ),
      //                                   critical_edge_candidates
      3, 1, network_connectors
    );
  };

  if(just_probe){
    nnodes = 7;
    nedges_critical = 17;
    probe(test_generator(.5,200));
  }

  return generate_and_execute_2d_plot_test(path, number_of_instances_per_coord, axis_data{"share-of-critical", 4, .2, .5}, axis_data{"connector-capacity", 10, 20, 200}, test_generator);
}
