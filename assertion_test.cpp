#include <cassert>
#include <thread>
#include <cmath>
#include <string>
#include <set>

#include "Graphtheory/Graphtheory.h"

int main(){

  random_graph_generator graph_generator(
                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
    tipping_policy(true,            true,             false),
                  //   number_of_nodes  number_of_steps   node_attribute_generator
    uniform_node_steps(5,               1,                {{}}),
                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator                 at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
    uniform_edge_step_fuzzing(7,              0,                    0,                  {{{"Capacity", {fix, Integral, 0, 5}}}}, false,                     false,                    true,   false, false),
    tipping_policy(true,             true,             false),
    //                 number_of_nodes  number_of_steps   node_attribute_generator
    uniform_node_steps(1,              1,                {{}}),
    //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
    uniform_edge_step_fuzzing(0,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
    {{"Flow", Attribute(max, 0)}, {"Demand", Attribute(fix, 0)}, {"Capacity", Attribute(fix, 1)},{"Cost", Attribute(fix, 0)}}, // default_edge_attributes
    {},
    {{{"Upper", {fix, Integral, 1, 10}}}}
  );

  while(true){
    auto [st, core_network, aggregated_subnetwork_cores, aggregated_subnetworks, network_connectors, g] = graph_generator.next();
    Node* s = st.first;
    Node* t = st.second;

    // Choose arbitrary subset
    random_set_element_generator<Edge*> set_gen (g.export_edges(), random_all_static);
    std::unordered_set<Edge*> critical_edges;
    size_t num_critical_edges = 4;
    assert(num_critical_edges <= g.size().edges);
    for(size_t i = 0; i < num_critical_edges; i++){
      Edge* curr = set_gen.next();
      while(critical_edges.find(curr) != critical_edges.end()){
        set_gen >> curr;
      }

      critical_edges.insert(curr);
    }

    // Check Property
    bool property = true;

    for(Edge* e : critical_edges){
      e->disconnect();
    }
    if(g.directed_admissible_st_path(s, t).exists()) property = false;
    for(Edge* e1 : critical_edges){
      for(Edge* e2 : critical_edges){
        if(e1 == e2) continue;
        if(!property) break;

        if(g.directed_admissible_st_path(e1->to(), e2->from()).exists()) property = false;
      }
    }
    for(Edge* e : critical_edges){
      e->reconnect();
    }

    if(!property) continue;

    // std::cout << g << std::endl;
    // std::cout << "critical edges: " << std::endl;
    // for(Edge* e : critical_edges){
    //   std::cout << *e << std::endl;
    // }
    //
    // break;

    // Determine 1-Optimal and 2-Optimal Edges
    std::pair<double, std::set<Edge*>> opt1 = {0, {}};
    for(Edge* e1 : critical_edges){
      e1->disconnect();

      double tmp = g.ford_fulkerson(s, t, "Flow", "Demand", "Capacity");
      g.reset_attribute_values({"Flow"});

      if(std::abs(tmp-opt1.first) < 1e-100){
        opt1.second.insert(e1);
      }else if(tmp > opt1.first){
        opt1 = {tmp, {e1}};
      }

      e1->reconnect();
    }

    std::pair<double, std::set<std::set<Edge*>> > opt2 = {0,{}};
    for(Edge* e1 : critical_edges){
      e1->disconnect();

      for(Edge* e2 : critical_edges){
        if(e1 == e2) continue;

        e2->disconnect();

        double tmp = g.ford_fulkerson(s, t, "Flow", "Demand", "Capacity");
        g.reset_attribute_values({"Flow"});

        if(std::abs(tmp-opt2.first) < 1e-100 ){
          opt2.second.insert({e1,e2});
        }else if(tmp > opt2.first){
          opt2 = {tmp,{{e1,e2}}};
        }

        e2->reconnect();
      }

      e1->reconnect();
    }

    /*
      checking whether 1-optimal edge stays 2-optimal when no two critical edges are reachable
    */
    bool assertion = true;
    for(Edge* e : opt1.second){
      for(std::set<Edge*> c : opt2.second){
        if(c.find(e) != c.end()){
          assertion = true;
        }
        if(assertion){
          break;
        }
      }
    }

    if(assertion) {
      std::cout << "graph checks out" << std::endl;
      continue;
    }

    std::cout << "Counter example found! \n Graph:" << std::endl;
    std::cout << g << std::endl;

    std::cout << "critical edges: " << std::endl;
    for(Edge* e : critical_edges){
      std::cout << *e << std::endl;
    }

    std::cout << "1-optimal: \n Value: " << opt1.first << std::endl;
    for(Edge* e : opt1.second){
      std::cout << *e << std::endl;
    }

    std::cout << "2-optimal: \n Value: " << opt2.first << std::endl;
    for(std::set<Edge*> s : opt2.second){
      std::cout << "{" << std::endl;
      std::cout << **s.begin() << std::endl;
      std::cout << **(++s.begin()) << std::endl;
      std::cout << "}" << std::endl;;
    }

    break;
  }
}
