#include "maintenance_problem_generator.h"

#include <climits>
#include "../../../Common/constants.h"

maintenance_problem_generator::maintenance_problem_generator(const random_graph_generator& graph_generator, size_t number_of_epochs, double share_of_critical, const critical_selection_policy& critical_selection_rules)
  : _graph_generator(graph_generator), _number_of_epochs(number_of_epochs), _share_of_critical(share_of_critical), _critical_selection_rules(critical_selection_rules) {
  // by core_network_problem we mean those where the tip connections are part of a reduction. In these tip connections have infinite capacity and can not be critical
  for(std::string attr : std::vector({"Selected", "Upper", "Flow"})){
    if(!this->_graph_generator.edge_template(attr).first) throw std::invalid_argument("Maintenance_Problem needs Graph with predetermined attributes present");
  }
  if( (share_of_critical < 0) || (share_of_critical > 1) ) throw std::invalid_argument("share_of_critical has to be between 0 and 1");
  if( ( this->_graph_generator.structure_graph_tipping_parameters().shelter_orphans ) && ( this->critical_selection_rules().selection_rule != everywhere ) ) std::cerr << "WARN: if you are trying to simulate multiple sources shelter_orphans should be set to false; otherwise you risk numerical inaccuracies" << std::endl;
}

void maintenance_problem_generator::operator=(const maintenance_problem_generator& other){
  this->_graph_generator = other._graph_generator;
  this->_number_of_epochs = other._number_of_epochs;
  this->_share_of_critical = other._share_of_critical;
  this->_critical_selection_rules = other._critical_selection_rules;
}
void maintenance_problem_generator::operator=(maintenance_problem_generator&& other){
  this->_graph_generator = std::move(other._graph_generator);
  this->_number_of_epochs = std::move(other._number_of_epochs);
  this->_share_of_critical = std::move(other._share_of_critical);
  this->_critical_selection_rules = std::move(other._critical_selection_rules);
}

Maintenance_Problem maintenance_problem_generator::next(){
  auto [st, core_network, network_cores, networks, connectors, g] = this->_graph_generator.next();
  SubGraph complete(g);

  random_set_element_generator<Edge*> set_gen = random_set_element_generator<Edge*>(this->_critical_selection_rules.critical_edge_candidates(g, core_network, network_cores, networks, connectors), random_unused);

  int number_of_critical_edges = floor(this->_share_of_critical*this->_critical_selection_rules.number_of_critical_candidates(g, core_network, network_cores, networks, connectors));

  Edge* curr;
  for(size_t i = 0; i < number_of_critical_edges; i++){
    set_gen >> curr;

    Attribute& attribute_search = curr->attribute_throwing("Selected");

    assert(attribute_search.value() != true);
    attribute_search.value() = true;
  }

  return Maintenance_Problem(g, st.first, st.second, this->_number_of_epochs);
}

bool maintenance_problem_generator::operator==(const maintenance_problem_generator& other) const {
  if( this->_graph_generator != other._graph_generator ) return false;
  if( this->_number_of_epochs != other._number_of_epochs ) return false;
  if( std::abs(this->_share_of_critical - other._share_of_critical) > CMP_EPS ) return false;
  if( this->_critical_selection_rules != other._critical_selection_rules ) return false;
  return true;
}
bool maintenance_problem_generator::operator!=(const maintenance_problem_generator& other) const {
  return !(*this == other);
}

void maintenance_problem_generator::operator>>(Maintenance_Problem& mp){
  mp = this->next();
}

std::string maintenance_problem_generator::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << random_graph_generator::csv_columns(prefix, separator) << separator
                      << prefix << "number_of_epochs" << separator
                      << prefix << "share_of_critical" << separator
                      << prefix << critical_selection_policy::csv_columns(prefix, separator);
  return csv_columns_stream.str();
}

std::string maintenance_problem_generator::csv_data(std::string separator) const {
  std::stringstream csv_columns_stream;
  csv_columns_stream << this->_graph_generator.csv_data(separator) << separator
                      << this->_number_of_epochs << separator
                      << this->_share_of_critical << separator
                      << this->_critical_selection_rules.csv_data(separator);
  return csv_columns_stream.str();
}

std::ostream& operator<<(std::ostream& os, const maintenance_problem_generator& mpg){
  os << "maintenance_problem_generator" << " {" << "\n";
  os << mpg._graph_generator << "\n"
      << mpg._share_of_critical << " "
      << mpg._number_of_epochs << " "
      << mpg._critical_selection_rules << "\n"
      << "} ";
  return os;
}

maintenance_problem_generator::maintenance_problem_generator(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "maintenance_problem_generator");
  is >> curr;
  IO_THROW(curr, "{");
  is >> this->_graph_generator
      >> this->_share_of_critical
      >> this->_number_of_epochs
      >> this->_critical_selection_rules;
  is >> curr;
  IO_THROW(curr, "}");

  if(!is) throw std::invalid_argument("stream did not contain complete maintenance_problem_generator object");
}
std::istream& operator>>(std::istream& is, maintenance_problem_generator& mpg){
  mpg = maintenance_problem_generator(is);
  return is;
}
