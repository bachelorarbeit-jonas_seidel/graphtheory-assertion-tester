#include "nmp_bulksepa_conshdlr.h"

//#define LOG

struct SCIP_ConsData
{
};


struct cons_separator_container
{
  SCIP* cons_separator;
  SCIP_VAR** vars; // [edge*nedges+epoch] for decision [nedges*nepochs] for target variable
  SCIP_CONS** maintenance_select_conss;
};

struct SCIP_ConshdlrData
{
  SCIP* separator;
  SCIP_VAR** separator_vars;
  SCIP_Real* capacities;
  int nedges;
  int nepochs;
  SCIP_Real target_upper_bound;
  int number_of_added_cons;
  SCIP_Bool use_reopt;
  cons_separator_container* cons_separator_container_for_reopt;
  SCIP_RETCODE (*create_cons_separator_function)(SCIP_ConshdlrData*, SCIP_SOL*, cons_separator_container*);
};

// helper functions
SCIP_RETCODE create_cons_separator_benders(SCIP_ConshdlrData* data, SCIP_SOL* solution, cons_separator_container* container){
  SCIP* separator = data->separator;
  SCIP_VAR** separator_vars = data->separator_vars;
  SCIP_Bool use_reopt = data->use_reopt;
  int nedges = data->nedges;
  int nepochs = data->nepochs;
  SCIP_Real target_upper_bound = data->target_upper_bound;
  SCIP_Real* capacities = data->capacities;

  char name[SCIP_MAXSTRLEN];

  SCIP* master;
  SCIP_CALL( SCIPcreate(&master));
  SCIP_CALL( SCIPincludeDefaultPlugins(master));
  SCIP_CALL( SCIPcreateProbBasic(master, "mp"));
  SCIP_CALL( SCIPsetObjsense(master, SCIP_OBJSENSE_MINIMIZE));
  SCIP_CALL( SCIPenableReoptimization(master, use_reopt));
  SCIP_CALL( SCIPsetMessagehdlr(master, NULL));


  SCIP_CALL( SCIPsetIntParam(master, "presolving/maxrestarts",0) );
  SCIP_CALL( SCIPsetIntParam(master, "heuristics/trysol/freq", 1) );

  SCIP_VAR** mp_vars;
  SCIP_CALL( SCIPallocBlockMemoryArray(master, &mp_vars, nedges*nepochs+1));
  for(int edge = 0; edge < nedges; edge++){
    for(int epoch = 0; epoch < nepochs; epoch++){
      SCIPsnprintf(name, SCIP_MAXSTRLEN, "Selected_%d_%d", edge, epoch);
      SCIP_CALL( SCIPcreateVarBasic(master, &mp_vars[edge*nepochs+epoch], name, 0, 1, SCIPgetSolVal(separator, solution, separator_vars[edge*nepochs+epoch]), SCIP_VARTYPE_BINARY));
      SCIP_CALL( SCIPaddVar(master, mp_vars[edge*nepochs+epoch]));
    }
  }
  SCIP_CALL( SCIPcreateVarBasic(master, &mp_vars[nedges*nepochs], "target_variable", 0, target_upper_bound, 1, SCIP_VARTYPE_CONTINUOUS));
  SCIP_CALL( SCIPaddVar(master, mp_vars[nedges*nepochs]));


  // const
  SCIP_CONS** maintenance_select_conss;
  SCIP_CALL( SCIPallocBlockMemoryArray(master, &maintenance_select_conss, nedges));
  for(int edge = 0; edge < nedges; edge++){
    SCIPsnprintf(name, SCIP_MAXSTRLEN, "maintenance_epoch_select_%d", edge);
    SCIP_CALL( SCIPcreateConsBasicLinear(master, &maintenance_select_conss[edge], name, 0, NULL, NULL, 1, 1));
    for( int epoch = 0; epoch < nepochs; epoch++){
      SCIP_CALL( SCIPaddCoefLinear( master, maintenance_select_conss[edge], mp_vars[edge*nepochs+epoch], 1));
    }
    SCIP_CALL( SCIPaddCons(master, maintenance_select_conss[edge]));
  }

  // subproblems
  SCIP** subproblems;
  SCIP_CALL( SCIPallocBufferArray(master, &subproblems, nepochs));

  SCIP_VAR* flow_var;
  SCIP_VAR* decision_var;
  SCIP_CONS* flow_cons;
  SCIP_CONS* capacity_cons;
  for(int epoch = 0; epoch < nepochs; epoch++){
    SCIP_CALL( SCIPcreate(&subproblems[epoch]));
    SCIP_CALL( SCIPincludeDefaultPlugins(subproblems[epoch]));
    SCIPsnprintf( name, SCIP_MAXSTRLEN, "sub_%d", epoch);
    SCIP_CALL( SCIPcreateProbBasic(subproblems[epoch], name));
    SCIP_CALL( SCIPsetObjsense(subproblems[epoch], SCIP_OBJSENSE_MINIMIZE));


    SCIPsnprintf( name, SCIP_MAXSTRLEN, "aggregate_flow_val_cons");

    SCIP_CALL( SCIPcreateConsBasicLinear(subproblems[epoch], &flow_cons, name, 0, NULL, NULL, -SCIPinfinity(subproblems[epoch]), 0));

    //flow vars
    for(int edge = 0; edge < nedges; edge++){
      SCIPsnprintf( name, SCIP_MAXSTRLEN, "flow_edge_%d", edge);

      SCIP_CALL( SCIPcreateVarBasic(subproblems[epoch], &flow_var, name, 0, capacities[edge], 0, SCIP_VARTYPE_CONTINUOUS));
      SCIP_CALL( SCIPaddVar(subproblems[epoch], flow_var));

      SCIP_CALL( SCIPaddCoefLinear(subproblems[epoch], flow_cons, flow_var, -1));

      SCIPsnprintf( name, SCIP_MAXSTRLEN, "Selected_%d_%d", edge, epoch);

      SCIP_CALL( SCIPcreateVarBasic(subproblems[epoch], &decision_var, name, 0, 1, 0, SCIP_VARTYPE_CONTINUOUS));
      SCIP_CALL( SCIPaddVar(subproblems[epoch], decision_var));

      // edge capacity constraints
      SCIPsnprintf( name, SCIP_MAXSTRLEN, "capacity_cons_%d", edge);

      SCIP_CALL( SCIPcreateConsBasicLinear(subproblems[epoch], &capacity_cons, name, 0, NULL, NULL, -SCIPinfinity(subproblems[epoch]), capacities[edge]));
      SCIP_CALL( SCIPaddCoefLinear(subproblems[epoch], capacity_cons, flow_var, 1));
      SCIP_CALL( SCIPaddCoefLinear(subproblems[epoch], capacity_cons, decision_var, capacities[edge]));
      SCIP_CALL( SCIPaddCons(subproblems[epoch], capacity_cons));
      SCIP_CALL( SCIPreleaseCons(subproblems[epoch], &capacity_cons));

      SCIP_CALL( SCIPreleaseVar(subproblems[epoch], &flow_var));
      SCIP_CALL( SCIPreleaseVar(subproblems[epoch], &decision_var));
    }

    // abusing flow var to store target var pointer
    SCIP_CALL( SCIPcreateVarBasic(subproblems[epoch], &flow_var, "target_variable", 0, target_upper_bound, 0, SCIP_VARTYPE_CONTINUOUS));
    SCIP_CALL( SCIPaddVar(subproblems[epoch], flow_var));
    SCIP_CALL( SCIPaddCoefLinear(subproblems[epoch], flow_cons, flow_var, 1));
    SCIP_CALL( SCIPaddCons(subproblems[epoch], flow_cons));
    SCIP_CALL( SCIPreleaseVar(subproblems[epoch], &flow_var));
    SCIP_CALL( SCIPreleaseCons(subproblems[epoch], &flow_cons));
  }

  SCIP_CALL( SCIPcreateBendersDefault(master, subproblems, nepochs));
  SCIP_CALL( SCIPsetBoolParam(master, "constraints/benders/active", TRUE) );
  SCIP_CALL( SCIPsetBoolParam(master, "constraints/benderslp/active", TRUE) );
  SCIP_CALL( SCIPsetIntParam(master, "constraints/benders/maxprerounds", 1) );
  SCIP_CALL( SCIPsetIntParam(master, "presolving/maxrounds", 1) );

  container->cons_separator = master;
  container->vars = mp_vars;
  container->maintenance_select_conss = maintenance_select_conss;

  return SCIP_OKAY;
}

SCIP_RETCODE create_cons_separator_scip(SCIP_ConshdlrData* data, SCIP_SOL* solution, cons_separator_container* container){
  SCIP* separator = data->separator;
  SCIP_VAR** separator_vars = data->separator_vars;
  SCIP_Real* capacities = data->capacities;
  int nedges = data->nedges;
  int nepochs = data->nepochs;
  SCIP_Real target_upper_bound = data->target_upper_bound;
  SCIP_Bool use_reopt = data->use_reopt;
  char name[SCIP_MAXSTRLEN];

  SCIP* cons_separator;
  SCIP_CALL( SCIPcreate(&cons_separator));

  SCIP_CALL( SCIPincludeDefaultPlugins(cons_separator));
  SCIP_CALL( SCIPcreateProbBasic(cons_separator, "ephemeral_bulksepa_separator"));
  SCIP_CALL( SCIPsetObjsense(cons_separator, SCIP_OBJSENSE_MINIMIZE));
  SCIP_CALL( SCIPenableReoptimization(cons_separator, use_reopt));
  SCIP_CALL( SCIPsetMessagehdlr(cons_separator, NULL));

  SCIP_VAR** vars;
  SCIP_CALL( SCIPallocBlockMemoryArray(cons_separator, &vars, nepochs*nedges+1));
  for(int epoch = 0; epoch < nepochs; epoch++){
    for(int edge = 0; edge < nedges; edge++){
      SCIPsnprintf(name, SCIP_MAXSTRLEN, "Selected_%d_%d", edge, epoch);

      SCIP_CALL( SCIPcreateVarBasic(cons_separator, &vars[edge*nepochs+epoch], name, 0, 1, SCIPgetSolVal(separator, solution, separator_vars[edge*nepochs+epoch]), SCIP_VARTYPE_BINARY));
      SCIP_CALL( SCIPaddVar(cons_separator, vars[edge*nepochs+epoch]));
    }
  }

  SCIP_CALL( SCIPcreateVarBasic(cons_separator, &vars[nedges*nepochs], "target_variable", 0, target_upper_bound, -1, SCIP_VARTYPE_CONTINUOUS));
  SCIP_CALL( SCIPaddVar(cons_separator, vars[nedges*nepochs]));


  SCIP_CONS** maintenance_select_conss;
  SCIP_CALL( SCIPallocBlockMemoryArray(cons_separator, &maintenance_select_conss, nedges));
  for(int edge = 0; edge < nedges; edge++){
    SCIPsnprintf(name, SCIP_MAXSTRLEN, "maintenance_epoch_select_%d", edge);
    SCIP_CALL( SCIPcreateConsBasicLinear(cons_separator, &maintenance_select_conss[edge], name, 0, NULL, NULL, 1, SCIPinfinity(cons_separator)));
    for(int epoch = 0; epoch < nepochs; epoch++){
      SCIP_CALL( SCIPaddCoefLinear(cons_separator, maintenance_select_conss[edge], vars[edge*nepochs+epoch], 1));
    }
    SCIP_CALL( SCIPaddCons(cons_separator, maintenance_select_conss[edge]));
  }

  SCIP_CONS* eph_cons;
  SCIP_Real aggr_cap = 0;
  for(int edge = 0; edge < nedges; edge++){
    aggr_cap += capacities[edge];
  }
  for(int epoch = 0; epoch < nepochs; epoch++){
    SCIPsnprintf(name, SCIP_MAXSTRLEN, "target_bound_%d", epoch);
    SCIP_CALL( SCIPcreateConsBasicLinear(cons_separator, &eph_cons, name, 0, NULL, NULL, -SCIPinfinity(cons_separator), aggr_cap));

    for(int edge = 0; edge < nedges; edge++){
      SCIP_CALL( SCIPaddCoefLinear(cons_separator, eph_cons, vars[edge*nepochs+epoch], capacities[edge]));
    }
    SCIP_CALL( SCIPaddCoefLinear(cons_separator, eph_cons, vars[nedges*nepochs], 1));

    SCIP_CALL( SCIPaddCons(cons_separator, eph_cons));
    SCIP_CALL( SCIPreleaseCons(cons_separator, &eph_cons));
  }

  container->cons_separator = cons_separator;
  container->vars = vars;
  container->maintenance_select_conss = maintenance_select_conss;

  // TODO free
  return SCIP_OKAY;
}

SCIP_RETCODE set_container(SCIP* scip, SCIP_CONSHDLRDATA* data, SCIP_SOL* sol, cons_separator_container** container_ptr){
  #ifdef LOG
  std::cout << "separating over cut of " << data->nedges << " edges" << std::endl;
  #endif

  if(data->use_reopt){
    *container_ptr = data->cons_separator_container_for_reopt;
    cons_separator_container* container = *container_ptr;

    SCIP_CALL( SCIPfreeReoptSolve(container->cons_separator));

    SCIP_Real* obj_coeff;
    SCIP_CALL( SCIPallocBufferArray(container->cons_separator, &obj_coeff, data->nedges*data->nepochs+1));
    for(int index = 0; index < data->nedges*data->nepochs; index++){
      obj_coeff[index] = SCIPgetSolVal(data->separator, sol, data->separator_vars[index]);
    }
    obj_coeff[data->nedges*data->nepochs] = 1;
    SCIP_CALL( SCIPchgReoptObjective(container->cons_separator, SCIP_OBJSENSE_MAXIMIZE, container->vars, obj_coeff, data->nedges*data->nepochs+1));
  }else{
    SCIP_CALL( SCIPallocBlockMemory(scip, container_ptr));
    SCIP_CALL( (*data->create_cons_separator_function)(data, sol, *container_ptr));
  }

  return SCIP_OKAY;
}

SCIP_RETCODE free_container(SCIP* scip, SCIP_CONSHDLRDATA* data, cons_separator_container** container_ptr){
  // release container
  if(!data->use_reopt){
    cons_separator_container* container = *container_ptr;

    for(int index = 0; index < data->nedges*data->nepochs+1; index++){
      SCIPreleaseVar(container->cons_separator, &container->vars[index]);
    }
    SCIPfreeBlockMemoryArray(container->cons_separator, &container->vars, data->nepochs*data->nedges+1);
    for(int edge = 0; edge < data->nedges; edge++){
      SCIPreleaseCons(container->cons_separator, &container->maintenance_select_conss[edge]);
    }
    SCIPfreeBlockMemoryArray(container->cons_separator, &container->maintenance_select_conss, data->nedges);
    SCIPfree(&container->cons_separator);
    SCIPfreeBlockMemory(scip, container_ptr);
  }

  return SCIP_OKAY;
}

SCIP_RETCODE sepa(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_SOL* sol, int* nsepa){
  cons_separator_container* container;
  char name[SCIP_MAXSTRLEN];
  SCIP_ConshdlrData* data = SCIPconshdlrGetData(conshdlr);

  SCIP_CALL( set_container(scip, data, sol, &container));

  SCIP_CALL( SCIPsolve(container->cons_separator));


  SCIP_SOL** sols = SCIPgetSols(container->cons_separator);
  int nsols = SCIPgetNSols(container->cons_separator);
  int sol_index = 0;
  while( SCIPisLT(data->separator, SCIPgetSolOrigObj(container->cons_separator, sols[sol_index]), 0) && sol_index < nsols){
    // add new cons
    SCIP_Real* coeffs;
    SCIP_CALL( SCIPallocBufferArray(data->separator, &coeffs, data->nedges*data->nepochs));
    for(int epoch = 0; epoch < data->nepochs; epoch++){
      for(int edge = 0; edge < data->nedges; edge++){
        coeffs[edge*data->nepochs+epoch] = SCIPgetSolVal(container->cons_separator, sols[sol_index], container->vars[edge*data->nepochs + epoch]);
      }
    }

    SCIP_CONS* new_cons;
    SCIPsnprintf(name, SCIP_MAXSTRLEN, "cons_%d", data->number_of_added_cons);
    SCIP_CALL( SCIPcreateConsBasicLinear(data->separator, &new_cons, name, data->nedges*data->nepochs, data->separator_vars, coeffs, SCIPgetSolVal(container->cons_separator, sols[sol_index], container->vars[data->nepochs*data->nedges]), SCIPinfinity(data->separator)));
    SCIPfreeBufferArray(data->separator, &coeffs);
    SCIP_CALL( SCIPaddCons(data->separator, new_cons));
    SCIP_CALL( SCIPreleaseCons(data->separator, &new_cons));

    sol_index++;
  }
  *nsepa = sol_index;

  SCIP_CALL( free_container(scip, data, &container));

  return SCIP_OKAY;
}

// conshdlr impl
static
SCIP_RETCODE sepa_lp (SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_RESULT* result)
{
  int nsepa;
  SCIP_CALL( sepa(scip, conshdlr, NULL, &nsepa));

  if(nsepa){
    *result = SCIP_CONSADDED;
    #ifdef LOG
    std::cout << "conshdlr added " << nsepa << " cons (sepa_lp)" << std::endl;
    #endif
  }else{
    *result = SCIP_DIDNOTFIND;
  }

  return SCIP_OKAY;
}

static
SCIP_RETCODE sepa_sol(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_SOL* sol, SCIP_RESULT* result)
{
  int nsepa;
  SCIP_CALL( sepa(scip, conshdlr, sol, &nsepa));

  if(nsepa){
    *result = SCIP_CONSADDED;
    #ifdef LOG
    std::cout << "conshdlr added " << nsepa << " cons (sepa_sol)" << std::endl;
    #endif
  }else{
    *result = SCIP_DIDNOTFIND;
  }

  return SCIP_OKAY;
}

static
SCIP_RETCODE enforce_lp (SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_RESULT* result)
{
  int nsepa;
  SCIP_CALL( sepa(scip, conshdlr, NULL, &nsepa));

  if(nsepa){
    *result = SCIP_CONSADDED;
    #ifdef LOG
    std::cout << "conshdlr added " << nsepa << " cons (enforce_lp)" << std::endl;
    #endif
  }else{
    *result = SCIP_FEASIBLE;
  }

  return SCIP_OKAY;
}


static
SCIP_RETCODE enforce_pseudo(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_Bool objinfeasible, SCIP_RESULT* result)
{
  cons_separator_container* container;
  SCIP_ConshdlrData* data = SCIPconshdlrGetData(conshdlr);

  SCIP_CALL( set_container(scip, data, NULL, &container));

  SCIP_CALL( SCIPsolve(container->cons_separator));


  SCIP_SOL* opt_sol = SCIPgetBestSol(container->cons_separator);
  if( SCIPisGE(scip, SCIPgetSolOrigObj(container->cons_separator, opt_sol), 0) ){
    *result = SCIP_FEASIBLE;
  }else{
    *result = SCIP_INFEASIBLE;
  }

  SCIP_CALL( free_container(scip, data, &container));

  return SCIP_OKAY;
}


static
SCIP_RETCODE check(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, SCIP_SOL* sol, SCIP_Bool checkintegrality, SCIP_Bool checklprows, SCIP_Bool printreason, SCIP_Bool completely, SCIP_RESULT* result)
{
  cons_separator_container* container;
  SCIP_ConshdlrData* data = SCIPconshdlrGetData(conshdlr);

  SCIP_CALL( set_container(scip, data, NULL, &container));
  SCIP_CALL( SCIPsolve(container->cons_separator));


  SCIP_SOL* opt_sol = SCIPgetBestSol(container->cons_separator);
  if( SCIPisGE(scip, SCIPgetSolOrigObj(container->cons_separator, opt_sol), 0) ){
    *result = SCIP_FEASIBLE;
  }else{
    *result = SCIP_INFEASIBLE;
  }

  SCIP_CALL( free_container(scip, data, &container));

  return SCIP_OKAY;
}

static
SCIP_RETCODE lock(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS* cons, SCIP_LOCKTYPE locktype, int nlockspos, int nlocksneg)
{
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);
  for(int var_index = 0; var_index < data->nepochs*data->nedges; var_index++){
     SCIP_CALL( SCIPaddVarLocksType(scip, data->separator_vars[var_index], SCIP_LOCKTYPE_MODEL, nlockspos, nlocksneg) );
  }

  return SCIP_OKAY;
}

SCIP_RETCODE free_conshdlr(SCIP* scip, SCIP_CONSHDLR* conshdlr){
  SCIP_CONSHDLRDATA* data;
  data = SCIPconshdlrGetData(conshdlr);
  assert(data != NULL);

  for(int index = 0; index < data->nedges*data->nepochs; index++){
    SCIPreleaseVar(data->separator, &data->separator_vars[index]);
  }

  SCIPfreeBlockMemoryArray(data->separator, &data->separator_vars, data->nedges*data->nepochs);
  SCIPfreeBlockMemoryArray(data->separator, &data->capacities, data->nedges);
  SCIPfreeBlockMemory(data->separator, &data->cons_separator_container_for_reopt);

  SCIPfreeBlockMemory(scip, &data);
  SCIPconshdlrSetData(conshdlr, NULL);
  std::cin.ignore();

  return SCIP_OKAY;
}

SCIP_RETCODE SCIPincludeConshdlrBulkSepa(
   SCIP*                  scip,
   SCIP_VAR**             separator_vars,
   SCIP_Real*             capacities,
   int                    nedges,
   int                    nepochs,
   SCIP_Real              target_upper_bound,
   SCIP_Bool              use_reopt
   )
{
  SCIP_CONSHDLRDATA* conshdlrdata;
  SCIP_CONSHDLR* conshdlr;

  conshdlrdata = NULL;

  conshdlr = NULL;

  SCIP_CALL( SCIPallocBlockMemory(scip, &conshdlrdata) );

  conshdlrdata->create_cons_separator_function = &create_cons_separator_scip;
  conshdlrdata->separator = scip;
  conshdlrdata->separator_vars = separator_vars;
  conshdlrdata->capacities = capacities;
  conshdlrdata->nedges = nedges;
  conshdlrdata->nepochs = nepochs;
  conshdlrdata->target_upper_bound = target_upper_bound;
  conshdlrdata->number_of_added_cons = 0;
  conshdlrdata->use_reopt = use_reopt;
  SCIP_CALL( SCIPallocBlockMemory(scip, &conshdlrdata->cons_separator_container_for_reopt));
  SCIP_SOL* zero_sol;
  SCIP_CALL( SCIPcreateSol(scip, &zero_sol, NULL));
  if(use_reopt){
    SCIP_CALL( (*conshdlrdata->create_cons_separator_function)(conshdlrdata, zero_sol, conshdlrdata->cons_separator_container_for_reopt));
  }

  SCIP_CALL( SCIPincludeConshdlrBasic(scip, &conshdlr, "bulksepa conshdlr", "separates model inequalities for basic bulksepa separtation program",
         1000, 0, 0, FALSE,
         enforce_lp, enforce_pseudo, check, lock,
         conshdlrdata) );

  SCIP_CALL( SCIPsetConshdlrSepa(scip, conshdlr, sepa_lp, sepa_sol, 1, 1, 1) );
  SCIP_CALL( SCIPsetConshdlrFree(scip, conshdlr, free_conshdlr) );

  assert(conshdlr != NULL);

  return SCIP_OKAY;
}
