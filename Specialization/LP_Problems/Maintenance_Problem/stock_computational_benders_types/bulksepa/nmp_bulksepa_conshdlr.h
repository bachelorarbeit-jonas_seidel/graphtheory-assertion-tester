#ifndef NMP_BULKSEPA_CONSHDLR_H

#include <vector>
#include <iostream>

#include <scip/scip.h>
#include <scip/scipdefplugins.h>


extern "C"{
  SCIP_RETCODE SCIPincludeConshdlrBulkSepa(
     SCIP*                  scip,
     SCIP_VAR**             separator_vars,
     SCIP_Real*             capacities,
     int                    ncapacities,
     int                    nepochs,
     SCIP_Real              target_upper_bound,
     SCIP_Bool              use_reopt
  );
}

#endif
