#include "nmp_bulksepa.h"

struct nmp_bulksepa_container{
  SCIP* bulksepa_scip;
  SCIP_VAR**  coefficient_vars;
  int nedges;
  int nepochs;
};


struct SCIP_VarData{
  int nepochs;
  SCIP_VAR** vars_of_repr_edge;
};

struct SCIP_BenderscutData
{
  SCIP* master;
  SCIP_BENDERS* benders;
  std::vector<SCIP_CONS*>* capture_inequalities;
};

SCIP_RETCODE create_bulksepa_scip(SCIP* master, SCIP_SOL* solution, SCIP_Real* min_cut_capacities, SCIP_VAR** mp_cut_vars, SCIP_VAR* target_var, int nepochs, int nedges, nmp_bulksepa_container* container){
  SCIP* nmp_bulksepa;
  SCIPcreate(&nmp_bulksepa);
  char name[SCIP_MAXSTRLEN];
  SCIP_Real bound = SCIPgetDualbound(master);

  SCIP_CALL( SCIPincludeDefaultPlugins(nmp_bulksepa));
  SCIP_CALL( SCIPcreateProbBasic(nmp_bulksepa, "ephemeral_bulksepa"));
  SCIP_CALL( SCIPsetObjsense(nmp_bulksepa, SCIP_OBJSENSE_MINIMIZE));
  SCIP_CALL( SCIPsetMessagehdlr(nmp_bulksepa, NULL));

  // create vars
  SCIP_VAR**  coefficient_vars;
  SCIP_CALL( SCIPallocBlockMemoryArray(nmp_bulksepa, &coefficient_vars, nepochs*nedges));
  for(int epoch = 0; epoch < nepochs; epoch++){
    for(int edge = 0; edge < nedges; edge++){
      SCIPsnprintf(name, SCIP_MAXSTRLEN, "coefficient_of_%s", SCIPvarGetName(mp_cut_vars[edge*nepochs+epoch]));
      SCIP_CALL( SCIPcreateVarBasic(nmp_bulksepa, &coefficient_vars[edge*nepochs+epoch], name, 0, bound, SCIPgetSolVal(master, solution, mp_cut_vars[edge*nepochs+epoch]), SCIP_VARTYPE_CONTINUOUS));
      SCIP_CALL( SCIPaddVar(nmp_bulksepa, coefficient_vars[edge*nepochs+epoch]));
    }
  }

  SCIP_CALL( SCIPsetPresolving(nmp_bulksepa, SCIP_PARAMSETTING_OFF, TRUE));

  // add cut_sepa_sepa_conshdlr
  SCIP_CALL( SCIPincludeConshdlrBulkSepa(nmp_bulksepa, coefficient_vars, min_cut_capacities, nedges, nepochs, bound, FALSE));
  container->bulksepa_scip = nmp_bulksepa;
  container->coefficient_vars = coefficient_vars;
  container->nedges = nedges;
  container->nepochs = nepochs;

  // TODO free
  return SCIP_OKAY;
}

static
SCIP_RETCODE exec_sepa(SCIP* scip, SCIP_BENDERS* benders, SCIP_BENDERSCUT* benderscut, SCIP_SOL* sol, int probnumber, SCIP_BENDERSENFOTYPE type, SCIP_RESULT* result)
{
  assert(benders != NULL);
  SCIP* subproblem = SCIPbendersSubproblem(benders, probnumber);
  SCIPprintTransProblem(scip, NULL, NULL, FALSE);
  //SCIPprintTransSol(scip, sol, NULL, TRUE);

  // finding target_var
  SCIP_VAR* target_var_subproblem = SCIPfindVar(subproblem, "target_variable");
  SCIP_VAR* target_var;
  SCIPgetBendersMasterVar(scip, benders, target_var_subproblem, &target_var);
  assert(target_var != NULL);


  std::cout << "SCIPgetVarSol(scip, target_var) = " << SCIPgetVarSol(scip, target_var) << std::endl;
  std::cout << "!SCIPisLPDualReliable(scip) = " << !SCIPisLPDualReliable(scip) << std::endl;
  if(SCIPgetPrimalbound(scip) > 0 && !SCIPisLPDualReliable(scip)){
    *result = SCIP_DIDNOTRUN;
    return SCIP_OKAY;
  }


  // gathering cons with dual val "!= 0" which form a min cut
  SCIP_CONS** conss = SCIPgetConss(subproblem);
  std::vector<SCIP_Real> min_cut_capacities;
  std::vector<SCIP_CONS*> min_cut_conss;
  for(int cons_index = 0; cons_index < SCIPgetNConss(subproblem); cons_index++){
    if(std::string(SCIPconsGetName(conss[cons_index])).find("capacity") != std::string::npos){
      SCIP_Real dual_val;
      SCIP_Bool result;
      SCIPconsGetDualfarkas(subproblem, conss[cons_index], &dual_val, &result);
      if(!SCIPisFeasZero(subproblem, dual_val)){
        SCIP_Bool rhs_result = FALSE;
        min_cut_capacities.push_back(SCIPconsGetRhs(subproblem, conss[cons_index], &rhs_result));
        assert(rhs_result);
        min_cut_conss.push_back(conss[cons_index]);
      }
    }
  }

  // extracting the corresponding variables
  std::vector<SCIP_VAR*> subproblem_cut_vars;
  subproblem_cut_vars.reserve(min_cut_conss.size());
  for(SCIP_CONS* cons : min_cut_conss){
    SCIP_VAR* cons_vars[2];
    SCIP_Bool result = FALSE;
    SCIP_CALL( SCIPgetConsVars(subproblem, cons, cons_vars, 2, &result));
    if(result == false){
      continue;
    }

    for(int i = 0; i < 2; i++){
      if(std::string(SCIPvarGetName(cons_vars[i])).find("Selected") != std::string::npos){
        subproblem_cut_vars.push_back(cons_vars[i]);
        continue;
      }
    }
  }
  int nedges = min_cut_capacities.size();
  int nepochs = SCIPbendersGetNSubproblems(benders);

  // finding mp decision variables of all epochs
  SCIP_VAR** mp_cut_vars;
  SCIP_CALL( SCIPallocBufferArray(scip, &mp_cut_vars, nedges*nepochs));
  for(int edge = 0; edge < nedges; edge++){
    SCIP_VAR* mp_var;
    SCIP_CALL( SCIPgetBendersMasterVar(scip, benders, subproblem_cut_vars[edge], &mp_var));
    assert(mp_var != NULL);
    SCIP_VarData* vardata = SCIPvarGetData(mp_var);
    assert(vardata != NULL);

    for(int epoch = 0; epoch < nepochs; epoch++){
      mp_cut_vars[edge*nepochs+epoch] = vardata->vars_of_repr_edge[epoch]; // TODO: bulkcpy possible?
    }
  }

  nmp_bulksepa_container container;
  SCIP_CALL( create_bulksepa_scip(scip, sol, min_cut_capacities.data(), mp_cut_vars, target_var, nepochs, nedges, &container));

  SCIPsolve(container.bulksepa_scip);
  SCIP_SOL* opt_sol = SCIPgetBestSol(container.bulksepa_scip);


  // checking if problem separated solution; if yes add contraint
  //std::cout << SCIPgetVarSol(scip, target_var) << ">" << SCIPgetSolOrigObj(container.bulksepa_scip, opt_sol) << "? if so add capture-ineq" << std::endl;
  if(SCIPisGT(scip, SCIPgetVarSol(scip, target_var), SCIPgetSolOrigObj(container.bulksepa_scip, opt_sol))){
    std::cout << "capture inequality found! " << SCIPgetNConss(container.bulksepa_scip) << " cons had to be separated." << std::endl;

    char name[SCIP_MAXSTRLEN];

    // extracting optimal solution from container.bulksepa_scip
    SCIP_Real* best_coeffs;
    SCIP_CALL( SCIPallocBufferArray(scip, &best_coeffs, nedges*nepochs));
    for(int edge = 0; edge < nedges; edge++){
      for(int epoch = 0; epoch < nepochs; epoch++){
        best_coeffs[edge*nepochs+epoch] = SCIPgetSolVal(container.bulksepa_scip, opt_sol, container.coefficient_vars[edge*nepochs+epoch]);
      }
    }

    SCIP_CONS* cons;
    SCIP_BENDERSCUTDATA* benderscut_data = SCIPbenderscutGetData(benderscut);
    assert(benderscut_data != NULL);
    SCIPsnprintf(name, SCIP_MAXSTRLEN, "capture_inequality_%d", benderscut_data->capture_inequalities->size());
    SCIP_CALL( SCIPcreateConsBasicLinear(scip, &cons, name, nedges*nepochs, mp_cut_vars, best_coeffs, 0, SCIPinfinity(scip))); // TODO: cut? SCIPstoreBenderscutCons function does not exist?
    SCIP_CALL( SCIPaddCoefLinear(scip, cons, target_var, -1));
    SCIP_CALL( SCIPaddCons(scip, cons));

    SCIP_CALL( SCIPprintCons(scip, cons, NULL));
    SCIP_CALL( SCIPprintOrigProblem(scip, NULL, NULL, FALSE));
    SCIP_CALL( SCIPprintTransProblem(scip, NULL, NULL, FALSE));

    SCIP_CALL( SCIPreleaseCons(scip, &cons));

    benderscut_data->capture_inequalities->push_back(cons);

    // release buffer
    SCIPfreeBufferArray(scip, &best_coeffs);
    SCIPfreeBufferArray(scip, &mp_cut_vars);

    (*result) = SCIP_CONSADDED;
  }else{
    //std::cout << "no admissible capture inequality found" << std::endl;
    *result = SCIP_DIDNOTFIND;
  }


  for(int index = 0; index < nepochs*nedges; index++){
    SCIPreleaseVar(container.bulksepa_scip, &container.coefficient_vars[index]);
  }
  SCIPfreeBlockMemoryArray(container.bulksepa_scip, &container.coefficient_vars, nedges*nepochs);
  //SCIPfree(&container.bulksepa_scip);

  return SCIP_OKAY;
}

SCIP_RETCODE free_benderscut(SCIP* scip, SCIP_BENDERSCUT* benderscut){
  SCIP_BENDERSCUTDATA* data;
  data = SCIPbenderscutGetData(benderscut);
  assert(data != NULL);

  delete data->capture_inequalities;

  SCIPfreeBlockMemory(scip, &data);
  SCIPbenderscutSetData(benderscut, NULL);
  std::cin.ignore();

  return SCIP_OKAY;
}

SCIP_RETCODE SCIPincludeBenderscutBasicSepa(
   SCIP*                 scip,
   SCIP_BENDERS*         benders
   )
{
  SCIP_BENDERSCUTDATA* benderscutdata;
  SCIP_BENDERSCUT* benderscut;

  assert(benders != NULL);

  SCIP_CALL( SCIPallocBlockMemory(scip, &benderscutdata) );
  benderscutdata->master = scip;
  benderscutdata->benders = benders;
  benderscutdata->capture_inequalities = new std::vector<SCIP_CONS*>();

  SCIP_CALL( SCIPincludeBenderscutBasic(scip, benders, &benderscut, "cut capture", "uses min cut from subproblem to separate facets of the corresponding maintenance problem contrained to cut network",
       -100000, TRUE, exec_sepa, benderscutdata) );

  SCIP_CALL( SCIPsetBenderscutFree(scip, benderscut, &free_benderscut));

  assert(benderscut != NULL);
  return SCIP_OKAY;
}
