#ifndef MAINTENANCE_PROBLEM_GENERATOR_H
#define MAINTENANCE_PROBLEM_GENERATOR_H

#include <stdexcept>

#include "Maintenance_Problem.h"
#include "../../../Graphtheory/Graphtheory.h"
#include "critical_selection_policy.h"

class maintenance_problem_generator{
  random_graph_generator _graph_generator;
  size_t _number_of_epochs;
  double _share_of_critical;
  critical_selection_policy _critical_selection_rules;
public:
  maintenance_problem_generator(){}
  maintenance_problem_generator(const maintenance_problem_generator& other){
    *this = other;
  }
  maintenance_problem_generator(maintenance_problem_generator&& other){
    *this = std::move(other);
  }
  maintenance_problem_generator(const random_graph_generator& graph_generator, size_t number_of_epochs, double share_of_critical, const critical_selection_policy& critical_selection_rules);

  Maintenance_Problem next();

  void operator>>(Maintenance_Problem& mp);

  void operator=(const maintenance_problem_generator& other);
  void operator=(maintenance_problem_generator&& other);

  bool operator==(const maintenance_problem_generator& other) const ;
  bool operator!=(const maintenance_problem_generator& other) const ;

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  maintenance_problem_generator(std::istream& is);

  const random_graph_generator& graph_generator() const {
    return this->_graph_generator;
  }
  double share_of_critical() const {
    return this->_share_of_critical;
  }
  size_t number_of_epochs() const {
    return this->_number_of_epochs;
  }
  const critical_selection_policy& critical_selection_rules() const {
    return this->_critical_selection_rules;
  }

  friend std::ostream& operator<<(std::ostream& os, const maintenance_problem_generator& mpg);
  friend std::istream& operator>>(std::istream& is, maintenance_problem_generator& mpg);
};

std::ostream& operator<<(std::ostream& os, const maintenance_problem_generator& mpg);
std::istream& operator>>(std::istream& is, maintenance_problem_generator& mpg);

#endif
