#include "nmp_benders_conshdlr.h"

#include <cassert>
#include <climits>
#include <iostream>

#include <vector>
#include <map>
#include <unordered_set>
#include <cstdint>
#include <iterator>
#include <numeric>
#include <cmath>

#define LOG_CALLS
//#define PRINTROWS
//#define PRINTSOL

#include <ortools/algorithms/knapsack_solver.h>

enum BendersHdlrType{
  LP,
  BRANCH
};

struct SCIP_ConshdlrData
{
  BendersHdlrType hdlrtype;
  int nepochs;
  int nedges;
  int number_of_added_cons;
  int number_of_added_cuts;
  int min_depth;
  int max_depth;
  SCIP** benders_separators_lpsol;
  SCIP** benders_separators_heursol;
  SCIP_Real* min_cuts_size;
  SCIP_VAR** min_cuts_vars;
  int node_max;
  int node_exec_count;
  SCIP_NODE* last_node;
  int stalling_max;
  int stalling_count;
  SCIP_Real last_dual_bound;
  std::unordered_set<int>* unchecked_trysol_solutions;
  SCIP_SOL* opt; // for debug purposes execution may be stopped if opt found to be inadmissible
};

struct SCIP_Probdata_Subproblem{
  int epoch;
  int nepochs;
  int nedges;
  int number_of_added_cons;
};

struct SCIP_VarData_Subproblem{
  SCIP_Real capacity;
  SCIP* master;
  SCIP_VAR* decision_of_master;
};

SCIP_RETCODE update_objective_benders(SCIP* scip, SCIP_SOL* sol, SCIP* separator){

  SCIPfreeReoptSolve(separator);

  // creating of new target coeffs to set reopt direction
  SCIP_VAR** separator_vars = SCIPgetOrigVars(separator);
  int nseparator_vars = SCIPgetNOrigVars(separator);
  SCIP_Real* separator_coeff;
  SCIP_CALL( SCIPallocCleanBufferArray(separator, &separator_coeff, nseparator_vars));
  for(int var_index = 0; var_index < nseparator_vars; var_index++){
    SCIP_VarData_Subproblem* vardata = (SCIP_VarData_Subproblem*) SCIPvarGetData(separator_vars[var_index]);
    if(vardata != NULL){
      separator_coeff[var_index] = vardata->capacity * (1 - SCIPgetSolVal(scip, sol, vardata->decision_of_master));
    }
  }

  // setting said direction
  SCIPchgReoptObjective(separator, SCIP_OBJSENSE_MINIMIZE, separator_vars, separator_coeff, nseparator_vars);
  SCIPfreeCleanBufferArray(separator, &separator_coeff);

  return SCIP_OKAY;
}

SCIP_RETCODE update_objective_max_fractional(SCIP* scip, SCIP_SOL* sol, SCIP* separator){

  SCIPfreeReoptSolve(separator);

  // creating of new target coeffs to set reopt direction
  SCIP_VAR** separator_vars = SCIPgetOrigVars(separator);
  int nseparator_vars = SCIPgetNOrigVars(separator);
  SCIP_Real* separator_coeff;
  SCIP_CALL( SCIPallocCleanBufferArray(separator, &separator_coeff, nseparator_vars));
  for(int var_index = 0; var_index < nseparator_vars; var_index++){
    SCIP_VarData_Subproblem* vardata = (SCIP_VarData_Subproblem*) SCIPvarGetData(separator_vars[var_index]);
    if(vardata != NULL){
      separator_coeff[var_index] = std::min(1 - SCIPgetSolVal(scip, sol, vardata->decision_of_master), SCIPgetSolVal(scip, sol, vardata->decision_of_master));
    }
  }

  // setting said direction ( we want to maximize in this case )
  SCIPchgReoptObjective(separator, SCIP_OBJSENSE_MAXIMIZE, separator_vars, separator_coeff, nseparator_vars);
  SCIPfreeCleanBufferArray(separator, &separator_coeff);

  return SCIP_OKAY;
}

SCIP_RETCODE update_objective_max_posfrac(SCIP* scip, SCIP_SOL* sol, SCIP* separator){

  SCIPfreeReoptSolve(separator);

  // creating of new target coeffs to set reopt direction
  SCIP_VAR** separator_vars = SCIPgetOrigVars(separator);
  int nseparator_vars = SCIPgetNOrigVars(separator);
  SCIP_Real* separator_coeff;
  SCIP_CALL( SCIPallocCleanBufferArray(separator, &separator_coeff, nseparator_vars));
  for(int var_index = 0; var_index < nseparator_vars; var_index++){
    SCIP_VarData_Subproblem* vardata = (SCIP_VarData_Subproblem*) SCIPvarGetData(separator_vars[var_index]);
    if(vardata != NULL){
      separator_coeff[var_index] = SCIPgetSolVal(scip, sol, vardata->decision_of_master);
    }
  }

  // setting said direction ( we want to maximize in this case )
  SCIPchgReoptObjective(separator, SCIP_OBJSENSE_MAXIMIZE, separator_vars, separator_coeff, nseparator_vars);
  SCIPfreeCleanBufferArray(separator, &separator_coeff);

  return SCIP_OKAY;
}

SCIP_RETCODE update_objective_fractionality_slack(SCIP* scip, SCIP_SOL* sol, SCIP* separator){

  SCIPfreeReoptSolve(separator);

  // creating of new target coeffs to set reopt direction
  SCIP_VAR** separator_vars = SCIPgetOrigVars(separator);
  int nseparator_vars = SCIPgetNOrigVars(separator);
  SCIP_Real* separator_coeff;
  SCIP_CALL( SCIPallocCleanBufferArray(separator, &separator_coeff, nseparator_vars));
  for(int var_index = 0; var_index < nseparator_vars; var_index++){
    SCIP_VarData_Subproblem* vardata = (SCIP_VarData_Subproblem*) SCIPvarGetData(separator_vars[var_index]);
    if(vardata != NULL){       //  c_ij              * (1 - x_ij^t)                                                * ( 1/2 - frac(x_ij^t) )
      separator_coeff[var_index] = vardata->capacity * (1 - SCIPgetSolVal(scip, sol, vardata->decision_of_master)) * (  ((SCIP_Real) 1)/2 - std::min(1 - SCIPgetSolVal(scip, sol, vardata->decision_of_master), SCIPgetSolVal(scip, sol, vardata->decision_of_master))  );
    }
  }

  // setting said direction ( we want to maximize in this case )
  SCIPchgReoptObjective(separator, SCIP_OBJSENSE_MINIMIZE, separator_vars, separator_coeff, nseparator_vars);
  SCIPfreeCleanBufferArray(separator, &separator_coeff);

  return SCIP_OKAY;
}




SCIP_RETCODE sepa_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_SOL* sol, SCIP* separator, int* nsepa){
  #ifdef LOG_CALLS
  std::cout << "-------> separating cons" << std::endl;
  #endif
  char name[SCIP_MAXSTRLEN];
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);



  SCIP_CALL( update_objective_benders(scip, sol, separator));

  SCIP_CALL( SCIPsolve(separator)); // TODO: use event handler to stop if a cut smaller than target_var val has been found?

  #ifdef LOG_CALLS
  std::cout << "optimal solution of separation problem is given by (activate msghdlr to print)" << std::endl;
  SCIPprintBestSol(separator, NULL, TRUE);
  #endif

  SCIP_VAR** separator_vars = SCIPgetOrigVars(separator);
  int nseparator_vars = SCIPgetNOrigVars(separator);
  SCIP_SOL** sols = SCIPgetSols(separator);
  int nsols = SCIPgetNSols(separator);
  int sol_index = 0;

  SCIP_VAR* target = SCIPfindVar(scip, "target_variable"); // todo store in probdata
  assert(target != NULL);
  SCIP_Real min_cut_val = SCIPgetSolOrigObj(separator, sols[0]);
  if( SCIPisLT(scip, min_cut_val, SCIPgetSolVal(scip, sol, target)) ){
    SCIP_Probdata_Subproblem* separator_data = (SCIP_Probdata_Subproblem*) SCIPgetProbData(separator);

    SCIP_VAR** master_vars;
    SCIP_CALL( SCIPallocBufferArray(scip, &master_vars, separator_data->nedges));
    SCIP_Real* coeffs;
    SCIP_CALL( SCIPallocBufferArray(scip, &coeffs, separator_data->nedges));
    while( SCIPisEQ(scip, SCIPgetSolOrigObj(separator, sols[sol_index]), min_cut_val) && sol_index < nsols){
      // add new cons for every optimal solution

      // determine coeffs of master_var
      int edge = 0;
      int aggregate_capacity = 0;

      SCIP_VarData_Subproblem* vardata;
      for(int var_index = 0; var_index < nseparator_vars; var_index++){
        vardata = (SCIP_VarData_Subproblem*) SCIPvarGetData(separator_vars[var_index]);

        // vardata != null => variable models Edgepotential
        if(vardata != NULL){
          assert(edge < separator_data->nedges);

          master_vars[edge] = vardata->decision_of_master;
          coeffs[edge] = vardata->capacity * SCIPgetSolVal(separator, sols[sol_index], separator_vars[var_index]);
          aggregate_capacity += coeffs[edge];

          edge++;
        }
      }
      assert(edge == separator_data->nedges);

      // create cons
      SCIP_CONS* feascut;
      SCIPsnprintf(name, SCIP_MAXSTRLEN, "feascut_subp_%d_cons_%d", separator_data->epoch, separator_data->number_of_added_cons); //              init?  sep?   enfo?  check? prop?  local? modif? age?   clean? stick?
      SCIP_CALL( SCIPcreateConsLinear(scip, &feascut, name, separator_data->nedges, master_vars, coeffs, -SCIPinfinity(scip), aggregate_capacity, TRUE , TRUE , TRUE , TRUE , TRUE , FALSE, FALSE, TRUE , TRUE , FALSE));
      SCIP_CALL( SCIPaddCoefLinear(scip, feascut, target, 1));
      SCIP_CALL( SCIPaddCons(scip, feascut));

      #ifdef LOG_CALLS
      SCIPinfoMessage(scip, NULL, "Constraint has been added:");
      #endif
      #ifdef PRINTROWS
      SCIPprintCons(scip, feascut, NULL);
      SCIPinfoMessage(scip, NULL, "\n");
      #endif

      SCIP_CALL( SCIPreleaseCons(scip, &feascut));

      separator_data->number_of_added_cons++;
      data->number_of_added_cons++;

      sol_index++;
    }
    SCIPfreeBufferArray(scip, &coeffs);
    SCIPfreeBufferArray(scip, &master_vars);
  }
  *nsepa = sol_index;

  return SCIP_OKAY;
}

SCIP_RETCODE sepa_cuts(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_SOL* sol, int* cuts_size, SCIP_Real* cuts_capacities, SCIP_Real* cuts_values, SCIP_VAR** cuts_vars, int* aggr_cuts){
  // TODO: case of fractional cuts_capacities and dual_bounds
  *aggr_cuts = 0;
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);
  SCIP_Real global_dual_bound = SCIPgetDualbound(scip); // TODO: create constraint type to update strength based on dual bound improvement
  SCIP_VAR* target_var = SCIPfindVar(scip, "target_variable");
  SCIP_Real target_var_value = SCIPgetSolVal(scip, sol, target_var);

  //#ifdef LOG_CALLS
  std::cout << "-------> separating cuts" << std::endl;

  for(int epoch = 0; epoch < data->nepochs; epoch++){
    std::cout << "cut of epoch " << epoch << std::endl;
    for(int edge = 0; edge < cuts_size[epoch]; edge++){
      std::cout << SCIPvarGetName(cuts_vars[epoch*data->nedges + edge]) << " ";
    }
    std::cout << std::endl;
  }
  //#endif


  /*
    WE ARE ASSUMING INTEGRALITY OF ALL EDGE CAPACITIES!
    WE ASSUME (though this is guarateed by usecase) THAT ALL cuts_values entries ARE IN [0,1]
  */

  // Deriving some constants
  //  determining scaling factor for capacities
  const long double values_scalar = (long double) INT64_MAX/(10000*target_var_value);
  assert(FLT_RADIX == 2);
  assert(LDBL_MANT_DIG >= 64); // choose long double since the mantissa of standard doubles is (on most systems) less than 64 bits conversion would not be exact
  // determining scaled target_var_value
  const int64 scaled_target_var_value = round(values_scalar * target_var_value);
  const int64 scaled_global_dual_bound = round(values_scalar * global_dual_bound);

  // Setting up problem data
  std::vector<std::vector<int64>> capacities[data->nepochs];
  std::vector<int64> values[data->nepochs];
  int64 aggregate_values[data->nepochs];

  for(int epoch = 0; epoch < data->nepochs; epoch++){
    capacities[epoch].push_back( std::vector<int64>() );
    capacities[epoch][0].reserve( cuts_size[epoch] );
    values[epoch].reserve( cuts_size[epoch] );
    aggregate_values[epoch] = 0;

    for(int index = 0; index < cuts_size[epoch]; index++){
      capacities[epoch][0].push_back( round(cuts_capacities[epoch*data->nedges + index]) );
      values[epoch].push_back( round(values_scalar * cuts_values[epoch*data->nedges + index]) );
      aggregate_values[epoch] += values[epoch][index];
    }
  }

  /*
    For various amounts of flow flowing through our cuts we can assert that for subsets of edges whos capacity is less than this flow
    there has to be flow over some edge outside of this subset.

    We will use this knowledge to derive valid inequalities of the form "target_var <= sum_{f = 1}^{floor(target_var_value)} 1_f^tr * x", where 1_f is some
    indicator vector of those edges previously determined to have some active edge among them.

    We can improve the efficacy of these inequalities by choosing our sets such that the rhs summands will be as small as possible for every f.
    To do this we will need to solve knapsacks. In order to save on problem instances we will refrain from solving one for every flow val:
    Because summands remain valid for heigher flow values we decide to only solve a few at first and improve our estimate if necessary.
  */


  // we will be improving the inequality progressively until primal_bound is below target_var_value or dual_bound is greater than target_var_value
  //  saves data for every flow level. We gradually add new levels to this _ordered_ map to improve our bounds. and recover the optimal decision later
  struct LevelStruct{
    std::vector<operations_research::KnapsackSolver*> solved_instance;
    std::vector<int64> solution;
    std::vector<SCIP_Real> complement_sol;
    size_t best_epoch;
  };
  std::map< SCIP_Real, LevelStruct > flow_level_data;
  //  define lamda to make code more readable
  auto add_level = [&](int64 flow_level){
    auto [iterator, success] = flow_level_data.insert({
      (SCIP_Real) flow_level,  // key
      LevelStruct{   // value
        std::vector<operations_research::KnapsackSolver*>(),
        std::vector<int64>(data->nepochs, 0),
        std::vector<SCIP_Real>(data->nepochs, 0),
        0
      }
    });

    assert(success);
    LevelStruct& current_level = iterator->second;

    for(int epoch = 0; epoch < data->nepochs; epoch++){
      current_level.solved_instance.push_back(new operations_research::KnapsackSolver{
                                                          operations_research::KnapsackSolver::KNAPSACK_MULTIDIMENSION_BRANCH_AND_BOUND_SOLVER,
                                                          "flow level knapsack"
                                                        });
      current_level.solved_instance[epoch]->Init( values[epoch], capacities[epoch], {flow_level} );
      current_level.solution[epoch] = current_level.solved_instance[epoch]->Solve();
      current_level.complement_sol[epoch] = aggregate_values[epoch] - current_level.solution[epoch];
      if(current_level.complement_sol[epoch] < current_level.complement_sol[current_level.best_epoch]){
        current_level.best_epoch = epoch;
      }

      if(current_level.complement_sol[epoch] <= 0){
        std::cout << "lvl " << flow_level << " appears to be not necessary for epoch " << epoch << std::endl;
      }
    }



    return iterator;
  };

  // initialize for extreme flow levels to enable bounding
  for(int64 flow_level : {(int64) 0, (int64) floor(global_dual_bound)}){
    add_level(flow_level);
  }


  // initialize bounds
  assert(flow_level_data.size() == 2);
  auto iterator_start = flow_level_data.begin();
  auto iterator_end = std::next(iterator_start);
  assert(iterator_start->first == 0);
  assert(iterator_end->first == floor(global_dual_bound));
  int64 primal_bound = iterator_start->second.complement_sol[iterator_start->second.best_epoch] * (iterator_end->first - iterator_start->first);
  int64 dual_bound = iterator_end->second.complement_sol[iterator_end->second.best_epoch] * (iterator_end->first - iterator_start->first);

  std::cout << target_var_value << " [" << ((long double) dual_bound)/values_scalar << ", " << ((long double) primal_bound)/values_scalar << "]" << std::endl;
  assert( primal_bound >= dual_bound );


  // We queue levels for calculation and complete them in order of expected improvement
  struct ComputationNode{
    SCIP_Real from;
    SCIP_Real to;
  };
  std::map<SCIP_Real, ComputationNode> computation_queue;
  // we estimate the priority (small = better) by the trapezoidal rule (ignoring constant coefficients)
  computation_queue.insert({(iterator_end->first - iterator_start->first) * (iterator_end->second.complement_sol[iterator_end->second.best_epoch] - iterator_start->second.complement_sol[iterator_start->second.best_epoch]), ComputationNode{iterator_start->first, iterator_end->first}});


  // iterate
  int64 level;
  SCIP_Bool timeout = FALSE; // TODO: impl proper timeout logic or at least skip generation if efficacy of previous cut was too low
  while( dual_bound < scaled_target_var_value && primal_bound >= scaled_target_var_value - (int64) (((long double).05)*scaled_target_var_value) && !timeout ){
    if( primal_bound < scaled_target_var_value && dual_bound > scaled_target_var_value - (int64) (((long double).05)*scaled_target_var_value) ) break; // TODO: or at least apply some stricter timeout
    if(computation_queue.begin() == computation_queue.end()) break;

    // determine which level is to be computed

    auto [priority, node] = *computation_queue.begin();
    level = round( ((double) (node.to+node.from))/2 );

    // insert new level (iterator references the newly inserted LevelStruct)

    std::cout << "adding level for " << level << std::endl;
    auto iterator = add_level( level );

    // update bounds
    auto prev = std::prev(iterator);
    auto next = std::next(iterator);

    int64 primal_change = round(-(next->first-iterator->first)*prev->second.complement_sol[prev->second.best_epoch] + (next->first-iterator->first)*iterator->second.complement_sol[iterator->second.best_epoch]);
    primal_bound = primal_bound + primal_change;
    int64 dual_change   = round(-(iterator->first-prev->first)*next->second.complement_sol[next->second.best_epoch] + (iterator->first-prev->first)*iterator->second.complement_sol[iterator->second.best_epoch]);
    dual_bound   = dual_bound + dual_change;

    std::cout << "prev_val: " << prev->second.complement_sol[prev->second.best_epoch]/values_scalar << " iterator_val: " << iterator->second.complement_sol[iterator->second.best_epoch]/values_scalar << " next_val: " << next->second.complement_sol[next->second.best_epoch]/values_scalar << std::endl;
    std::cout << level << ": " << target_var_value << " [" << dual_bound/values_scalar << " (change: " << dual_change/values_scalar << "), " << primal_bound/values_scalar << " (change: " << primal_change/values_scalar << ")" << "]" << std::endl;


    // update computational_queue
    computation_queue.erase(computation_queue.begin());
    int64 new_priority = (next->first-iterator->first) * (next->second.complement_sol[next->second.best_epoch] - iterator->second.complement_sol[iterator->second.best_epoch]);
    if(new_priority && next->first-iterator->first > 1) computation_queue.insert({new_priority, ComputationNode{iterator->first, next->first} });
    new_priority = (iterator->first-prev->first) * (iterator->second.complement_sol[iterator->second.best_epoch] - prev->second.complement_sol[prev->second.best_epoch]);
    if(new_priority && iterator->first-prev->first > 1) computation_queue.insert({new_priority, ComputationNode{prev->first, iterator->first} });
  }
  if( primal_bound >= scaled_target_var_value ) return SCIP_OKAY; // no cut was found
  if( timeout ) return SCIP_OKAY;                                // no cut was found


  // start calculating the data for our new cut
  // for each calculated level increment the coefficients of variables not packed in the optimal solution by the distance to the next level.
  SCIP_Real aggr_coeffs = 0;
  SCIP_Real* coeffs;
  SCIP_CALL( SCIPallocCleanBufferArray(scip, &coeffs, data->nedges * data->nepochs));
  for(int index = 0; index < data->nedges * data->nepochs; index++){
    coeffs[index] = 0;
  } // TODO: why is this supposedly clean buffer infact not so?
  auto current = flow_level_data.begin();
  auto next = std::next(current);
  for( ; next != flow_level_data.end(); next++){
    auto& [level, level_struct] = *current;
    auto& [next_level, next_level_struct] = *next;
    SCIP_Real interval_length = next_level-level;



    bool valid_level = false;


    for(int var_index = 0; var_index < cuts_size[level_struct.best_epoch]; var_index++){
      if(!level_struct.solved_instance[level_struct.best_epoch]->BestSolutionContains(var_index)){
        coeffs[data->nedges*level_struct.best_epoch + var_index] += interval_length;
        aggr_coeffs += interval_length;

        if(level > SCIPgetSolVal(scip, data->opt, target_var)) {valid_level = true; continue;}
        if(SCIPisZero(scip, SCIPgetSolVal(scip, data->opt, cuts_vars[level_struct.best_epoch*data->nedges + var_index]))) valid_level = true;
        std::cout << SCIPvarGetName(cuts_vars[level_struct.best_epoch*data->nedges + var_index]) << "(optsolval: " << SCIPgetSolVal(scip, data->opt, cuts_vars[level_struct.best_epoch*data->nedges + var_index]) << ") ";
      }
    }


    std::cout << std::endl;
    if(!valid_level){
      std::cout << "level: " << level << " of " << SCIPgetSolVal(scip, data->opt, target_var) << std::endl;
      SCIPABORT();
    }

    current = next;
  }


  SCIP_ROW* row;
  char name[SCIP_MAXSTRLEN];
  SCIPsnprintf(name, SCIP_MAXSTRLEN, "capture_cut_%d", data->number_of_added_cuts);
  SCIP_CALL( SCIPcreateEmptyRowConshdlr(scip, &row, conshdlr, name, -SCIPinfinity(scip), aggr_coeffs, FALSE, FALSE, FALSE) );
  SCIP_CALL( SCIPcacheRowExtensions(scip, row) );

  for(int epoch = 0; epoch < data->nepochs; epoch++){
    for(int edge = 0; edge < cuts_size[epoch]; edge++){
      SCIP_CALL( SCIPaddVarToRow(scip, row, cuts_vars[epoch*data->nedges + edge], coeffs[epoch*data->nedges + edge]) );
    }
  }

  //SCIPfreeCleanBufferArray(scip, &coeffs);
  for(auto& [level, level_struct] : flow_level_data){
    for(operations_research::KnapsackSolver* ks : level_struct.solved_instance){
      delete ks;
    }
  }


  SCIP_CALL( SCIPaddVarToRow(scip, row, target_var, 1) );
  SCIP_CALL( SCIPflushRowExtensions(scip, row) );


  if( SCIPisCutEfficacious(scip, sol, row) ){
    SCIP_Bool infeasible;
    SCIP_CALL( SCIPaddRow(scip, row, TRUE, &infeasible) );
    if (infeasible) *aggr_cuts = -1; else *aggr_cuts = 1;
  }

  SCIP_Bool feasible = FALSE;
  //SCIPcheckSol(scip, data->opt, TRUE, TRUE, TRUE, FALSE, TRUE, &feasible);
	if(!feasible){
    SCIP_Real rhs = 0;
    SCIP_Real lhs = 0;
    for(int epoch = 0; epoch < data->nepochs; epoch++){
      for(int edge = 0; edge < cuts_size[epoch]; edge++){
        rhs += coeffs[epoch*data->nedges + edge] * cuts_values[epoch*data->nedges + edge];
      }
    }

    lhs += SCIPgetSolVal(scip, sol, target_var);
    std::cout << lhs << " <= " << rhs << " ? " << std::endl;
  }

  SCIP_CALL( SCIPreleaseRow(scip, &row) );

  return SCIP_OKAY;
}

SCIP_RETCODE sepa_exec_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_SOL* sol, const char* func_prompt, int* aggr_cons, int* aggr_cuts){
  *aggr_cons = 0;
  *aggr_cuts = 0;
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);
  assert(data != NULL);

  // if new node reset node counter
  if(data->last_node != SCIPgetCurrentNode(scip)){
    data->last_node = SCIPgetCurrentNode(scip);
    data->node_exec_count = 0;
  }
  // if new dual bound reset stalling counter
  if(data->last_dual_bound != SCIPgetDualbound(scip)){
    data->last_dual_bound = SCIPgetDualbound(scip);
    data->stalling_count = 0;
  }
  if( data->max_depth < SCIPgetDepth(scip) || data->min_depth > SCIPgetDepth(scip) || (SCIPinProbing(scip) && data->hdlrtype == BRANCH) ) {
    std::cout << data->node_exec_count << ">=" << data->node_max << std::endl;
    if(data->node_exec_count >= data->node_max){
      return SCIP_OKAY;
    }
    if(data->stalling_count < data->stalling_max){
      data->stalling_count++;
      return SCIP_OKAY;
    }
  }

  data->node_exec_count++;
  data->stalling_count = 0;

  #ifdef LOG_CALLS
  std::cout << "========================= " << func_prompt << " =========================" << std::endl;
  if( SCIPgetDepth(scip) > 0 && !SCIPinProbing(scip) ){
    std::cout << "stalling exceeded (" << data->stalling_count << ") node_exec_count = " << data->node_exec_count << std::endl;
  }
  #endif

  // separate feascuts (adds cons)
  SCIP** separators;
  if(sol == NULL){
    separators = data->benders_separators_lpsol;
  }else{
    separators = data->benders_separators_heursol;
  }
  int nsepa = 0;
  for( int epoch = data->number_of_added_cons; epoch < data->number_of_added_cons + data->nepochs; epoch++){
    SCIP_CALL( sepa_benders(scip, conshdlr, sol, separators[epoch % data->nepochs], &nsepa)); // preference for all separators should be equal
    *aggr_cons += nsepa;
    if(*aggr_cons) break; // comment out if all subproblems should be executed
  }

  #ifdef LOG_CALLS
  std::cout << SCIPconshdlrGetName(conshdlr) << " added " << *aggr_cons << " cons (" << func_prompt << ")" << std::endl;
  #endif

  // separate capture inequalities (adds cutting planes)
  if( data->hdlrtype == LP && *aggr_cons == 0 && sol == NULL && FALSE ){

    // determine maximal fractional cuts
    for(int epoch = 0; epoch < data->nepochs; epoch++){
      SCIP_CALL( update_objective_fractionality_slack(scip, sol, data->benders_separators_heursol[epoch]));
      SCIP_CALL( SCIPsolve(data->benders_separators_heursol[epoch]));
    }

    // improve dual bound futher by introducing cutting planes
    // extract maximal fractional cuts data
    SCIP_VAR** subproblem_vars;
    int nsubproblem_vars;
    SCIP_Real solval;

    // allocate the required arrays
    int* cuts_size;
    SCIP_CALL( SCIPallocBufferArray(scip, &cuts_size, data->nepochs));
    SCIP_VAR** cuts_vars;
    SCIP_CALL( SCIPallocBufferArray(scip, &cuts_vars, data->nepochs * data->nedges));
    SCIP_Real* cuts_capacities;
    SCIP_CALL( SCIPallocBufferArray(scip, &cuts_capacities, data->nepochs * data->nedges));
    SCIP_Real* cuts_values;
    SCIP_CALL( SCIPallocBufferArray(scip, &cuts_values, data->nepochs * data->nedges));


    // determine min cuts for all subproblems
    for(int epoch = 0; epoch < data->nepochs; epoch++){
      subproblem_vars = SCIPgetOrigVars(data->benders_separators_heursol[epoch]);
      nsubproblem_vars = SCIPgetNOrigVars(data->benders_separators_heursol[epoch]);
      cuts_size[epoch] = 0;
      for(int index = 0; index < nsubproblem_vars; index++){
        solval = SCIPgetSolVal(data->benders_separators_heursol[epoch], NULL, subproblem_vars[index]);
        if( !SCIPisZero(data->benders_separators_heursol[epoch], solval) ){
          SCIP_VarData_Subproblem* vardata = (SCIP_VarData_Subproblem*) SCIPvarGetData(subproblem_vars[index]);

          if(vardata == NULL) continue; // if == NULL than the variable is a node potential variable. To determine the cut we are interested in edge potentials.
          cuts_vars[epoch*data->nedges + cuts_size[epoch]] = vardata->decision_of_master;
          cuts_capacities[epoch*data->nedges + cuts_size[epoch]] = vardata->capacity;
          cuts_values[epoch*data->nedges + cuts_size[epoch]] = 1 - SCIPgetSolVal(scip, sol, vardata->decision_of_master);
          if(cuts_values[epoch*data->nedges + cuts_size[epoch]] < 0){
            std::cout << SCIPvarGetName(vardata->decision_of_master) << " " << SCIPgetSolVal(scip, sol, vardata->decision_of_master) << std::endl;
            cuts_values[epoch*data->nedges + cuts_size[epoch]] = 0;
          }
          cuts_size[epoch]++;
        }
      }
      assert(cuts_size[epoch] <= data->nedges);
    }

    SCIP_CALL( sepa_cuts(scip, conshdlr, sol, cuts_size, cuts_capacities, cuts_values, cuts_vars, aggr_cuts));

    SCIPfreeBufferArray(scip, &cuts_values);
    SCIPfreeBufferArray(scip, &cuts_capacities);
    SCIPfreeBufferArray(scip, &cuts_vars);
    SCIPfreeBufferArray(scip, &cuts_size);
  }

  #ifdef LOG_CALLS
  std::cout << SCIPconshdlrGetName(conshdlr) << " added " << std::abs(*aggr_cuts) << " cuts (" << func_prompt << ")" << std::endl;
  #endif


  return SCIP_OKAY;
}

SCIP_RETCODE check_exec_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_SOL* sol, const char* func_prompt, int* infeasible){
  *infeasible = FALSE;
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);
  #ifdef LOG_CALLS
  std::cout << "========================= " << func_prompt << " =========================" << std::endl;
  #endif

  // checking if the solution is a candidate for trysol
  {
    auto solution_search = data->unchecked_trysol_solutions->find(SCIPsolGetIndex(sol));
    if(solution_search != data->unchecked_trysol_solutions->end()){
      data->unchecked_trysol_solutions->erase(solution_search);
      return SCIP_OKAY;
    }
  }

  // verify as per usual
  SCIP** separators;
  SCIP_SOL* new_sol;
  SCIP_VAR* target_var = SCIPfindVar(scip, "target_variable");
  SCIP_HEUR* heurtrysol = SCIPfindHeur(scip, "trysol");
  SCIP_Bool new_sol_is_candidate = (data->hdlrtype == BRANCH);
  assert( new_sol_is_candidate == TRUE );
  SCIP_Real global_primal_bound = SCIPgetPrimalbound(scip);
  if(SCIPisLE(scip, SCIPgetSolOrigObj(scip, sol), global_primal_bound)){
    *infeasible = TRUE;
    return SCIP_OKAY;
  }

  SCIP_CALL( SCIPcreateSolCopy(scip, &new_sol, sol));
  SCIP_CALL( SCIPunlinkSol(scip, new_sol));
  if(sol == NULL){
    separators = data->benders_separators_lpsol;
  }else{
    separators = data->benders_separators_heursol;
  }
  SCIP_SOL* sepa_sol;
  for(int epoch = 0; epoch < data->nepochs; epoch++){
    SCIP_CALL( update_objective_benders(scip, sol, separators[epoch]));


    SCIP_CALL( SCIPsolve(separators[epoch]));


    sepa_sol = SCIPgetBestSol(separators[epoch]);
    if( SCIPisLT(scip, SCIPgetSolOrigObj(separators[epoch], sepa_sol), SCIPgetSolOrigObj(scip, new_sol)) ){
      *infeasible = TRUE;
      SCIP_CALL( SCIPsetSolVal(scip, new_sol, target_var, SCIPgetSolOrigObj(separators[epoch], sepa_sol)));

      if( SCIPisLE( scip, SCIPgetSolOrigObj(separators[epoch], sepa_sol), global_primal_bound) || !new_sol_is_candidate ){
        new_sol_is_candidate = FALSE;
        break;
      }
    }
    #ifdef LOG_CALLS
    std::cout << "checking solution of bound " << SCIPgetSolVal(scip, new_sol, target_var) << "/" << global_primal_bound << std::endl;
    #endif
  }

  if(new_sol_is_candidate){
    data->unchecked_trysol_solutions->insert(SCIPsolGetIndex(new_sol));

    SCIP_Bool success;
    //SCIP_CALL( SCIPcheckSolOrig(scip, new_sol, &success, FALSE, TRUE) ); // this will call the check function again, which is why we kept track of new_sol's index in data->unchecked_trysol_solutions
    SCIP_CALL( SCIPcheckSol(scip, new_sol, FALSE, FALSE, TRUE, TRUE, TRUE, &success) ); // this will call the check function again, which is why we kept track of new_sol's index in data->unchecked_trysol_solutions
    if(!success)return SCIP_OKAY;

    #ifdef LOG_CALLS
    std::cout << "new primal solution found! (solval: " << SCIPgetSolVal(scip, new_sol, target_var) << ")" << std::endl;
    #endif
    SCIP_CALL( SCIPheurPassSolAddSol(scip, heurtrysol, new_sol) );
  }
  SCIP_CALL( SCIPfreeSol(scip, &new_sol));

  return SCIP_OKAY;
}


// conshdlr impl
static
SCIP_RETCODE sepa_lp_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_RESULT* result)
{
  int count_sepa_cons = 0;
  int count_sepa_cuts = 0;
  SCIP_CALL( sepa_exec_benders(scip, conshdlr, NULL, "sepa_lp", &count_sepa_cons, &count_sepa_cuts));

  if(count_sepa_cuts == 0 && count_sepa_cons == 0){
    *result = SCIP_DIDNOTFIND;
  }else if(count_sepa_cons > 0){
    *result = SCIP_CONSADDED;
  }else if(count_sepa_cuts > 0){
    *result = SCIP_SEPARATED;
  }else if(count_sepa_cuts < 0){
    *result = SCIP_CUTOFF;
  }else{
    SCIPABORT();
  }

  return SCIP_OKAY;
}

static
SCIP_RETCODE sepa_sol_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_SOL* sol, SCIP_RESULT* result)
{
  int count_sepa_cons = 0;
  int count_sepa_cuts = 0;
  SCIP_CALL( sepa_exec_benders(scip, conshdlr, sol, "sepa_sol", &count_sepa_cons, &count_sepa_cuts));

  if(count_sepa_cons){
    *result = SCIP_CONSADDED;
  }else{
    *result = SCIP_DIDNOTFIND;
  }
  assert(count_sepa_cuts == 0);

  return SCIP_OKAY;
}

static
SCIP_RETCODE enforce_lp_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_RESULT* result)
{
  int count_sepa_cons = 0;
  int count_sepa_cuts = 0;
  SCIP_CALL( sepa_exec_benders(scip, conshdlr, NULL, "enforce_lp", &count_sepa_cons, &count_sepa_cuts));

  if(count_sepa_cuts == 0 && count_sepa_cons == 0){
    *result = SCIP_INFEASIBLE;
  }else if(count_sepa_cons > 0){
    *result = SCIP_CONSADDED;
  }else if(count_sepa_cuts > 0){
    *result = SCIP_SEPARATED;
  }else if(count_sepa_cuts < 0){
    *result = SCIP_CUTOFF;
  }else{
    SCIPABORT();
  }

  return SCIP_OKAY;
}


static
SCIP_RETCODE enforce_pseudo_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_Bool objinfeasible, SCIP_RESULT* result)
{
  int infeasible = 0;

  SCIP_CALL( check_exec_benders(scip, conshdlr, NULL, "enforce_pseudo", &infeasible));

  if(infeasible){
    *result = SCIP_INFEASIBLE;
  }else{
    *result = SCIP_FEASIBLE;
  }

  return SCIP_OKAY;
}


static
SCIP_RETCODE check_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, SCIP_SOL* sol, SCIP_Bool checkintegrality, SCIP_Bool checklprows, SCIP_Bool printreason, SCIP_Bool completely, SCIP_RESULT* result)
{
  int infeasible = 0;
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);
  switch (data->hdlrtype){
    case LP:
      break;
    case BRANCH:
      SCIP_CALL( check_exec_benders(scip, conshdlr, sol, "check (branch)", &infeasible));
      break;
    default:
      SCIPABORT();
  }

  if(infeasible){
    *result = SCIP_INFEASIBLE;
  }else{
    *result = SCIP_FEASIBLE;
  }

  return SCIP_OKAY;
}

static
SCIP_RETCODE lock_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS* cons, SCIP_LOCKTYPE locktype, int nlockspos, int nlocksneg)
{
  #ifdef LOG_CALLS
  std::cout << "========================= lock =========================" << std::endl;
  #endif
  SCIP_VAR** vars = SCIPgetOrigVars(scip);
  int nvars = SCIPgetNOrigVars(scip);
  for(int var_index = 0; var_index < nvars; var_index++){
     SCIP_CALL( SCIPaddVarLocksType(scip, vars[var_index], SCIP_LOCKTYPE_MODEL, nlocksneg, nlockspos) );
  }
  return SCIP_OKAY;
}

SCIP_RETCODE free_conshdlr_benders(SCIP* scip, SCIP_CONSHDLR* conshdlr){
  #ifdef LOG_CALLS
  std::cout << "========================= free_conshdlr =========================" << std::endl;
  #endif
  SCIP_CONSHDLRDATA* data = SCIPconshdlrGetData(conshdlr);
  assert(data != NULL);

  SCIPfreeBlockMemoryArray(scip, &data->benders_separators_lpsol, data->nepochs);
  SCIPfreeBlockMemoryArray(scip, &data->benders_separators_heursol, data->nepochs);
  delete data->unchecked_trysol_solutions;

  SCIPfreeBlockMemory(scip, &data);
  SCIPconshdlrSetData(conshdlr, NULL);

  std::cin.ignore();

  return SCIP_OKAY;
}

SCIP_RETCODE SCIPincludeConshdlrNMPBendersLP(
   SCIP*                  scip,
   SCIP**                 benders_separators_lpsol,
   SCIP**                 benders_separators_heursol,
   int                    nepochs,
   int                    nedges,
   SCIP_SOL*              opt
 )
{
  SCIP_CONSHDLRDATA* conshdlrdata;
  SCIP_CONSHDLR* conshdlr;

  conshdlrdata = NULL;

  conshdlr = NULL;

  SCIP_CALL( SCIPallocBlockMemory(scip, &conshdlrdata) );

  conshdlrdata->hdlrtype = BendersHdlrType::LP;
  conshdlrdata->nepochs = nepochs;
  conshdlrdata->nedges = nedges;
  conshdlrdata->number_of_added_cons = 0;
  conshdlrdata->number_of_added_cuts = 0;
  conshdlrdata->min_depth = 0;
  conshdlrdata->max_depth = 0;
  conshdlrdata->benders_separators_lpsol = benders_separators_lpsol;
  conshdlrdata->benders_separators_heursol = benders_separators_heursol;
  conshdlrdata->node_max = 0;
  conshdlrdata->node_exec_count = 0;
  conshdlrdata->last_node = NULL;
  conshdlrdata->stalling_max = 100;
  conshdlrdata->stalling_count = 0;
  conshdlrdata->last_dual_bound = SCIPinfinity(scip);
  conshdlrdata->unchecked_trysol_solutions = new std::unordered_set<int>();
  conshdlrdata->opt = opt;

  SCIP_CALL( SCIPallocBlockMemoryArray(scip, &conshdlrdata->min_cuts_size, conshdlrdata->nepochs));
  SCIP_CALL( SCIPallocBlockMemoryArray(scip, &conshdlrdata->min_cuts_vars, conshdlrdata->nepochs * conshdlrdata->nedges));



  SCIP_CALL( SCIPincludeConshdlrBasic(scip, &conshdlr, "bendersconslp", "separates nmp feasibility cuts",
         10000000, 10000000, 100, FALSE,
         enforce_lp_benders, enforce_pseudo_benders, check_benders, lock_benders,
         conshdlrdata) );

  //SCIP_CALL( SCIPsetConshdlrSepa(scip, conshdlr, sepa_lp, sepa_sol, 0, 100000000, FALSE) );
  SCIP_CALL( SCIPsetConshdlrFree(scip, conshdlr, free_conshdlr_benders) );

  assert(conshdlr != NULL);

  return SCIP_OKAY;
}

SCIP_RETCODE SCIPincludeConshdlrNMPBendersBranch(
   SCIP*                  scip,
   SCIP**                 benders_separators_lpsol,
   SCIP**                 benders_separators_heursol,
   int                    nepochs,
   int                    nedges
 )
{
  SCIP_CONSHDLRDATA* conshdlrdata;
  SCIP_CONSHDLR* conshdlr;

  conshdlrdata = NULL;

  conshdlr = NULL;

  SCIP_CALL( SCIPallocBlockMemory(scip, &conshdlrdata) );

  conshdlrdata->hdlrtype = BendersHdlrType::BRANCH;
  conshdlrdata->nepochs = nepochs;
  conshdlrdata->nedges = nedges;
  conshdlrdata->number_of_added_cons = 0;
  conshdlrdata->number_of_added_cuts = 0;
  conshdlrdata->min_depth = 1;
  conshdlrdata->max_depth = INT_MAX;
  conshdlrdata->benders_separators_lpsol = benders_separators_lpsol;
  conshdlrdata->benders_separators_heursol = benders_separators_heursol;
  conshdlrdata->node_max = 0;
  conshdlrdata->node_exec_count = 0;
  conshdlrdata->last_node = NULL;
  conshdlrdata->stalling_max = INT_MAX;
  conshdlrdata->stalling_count = 0;
  conshdlrdata->last_dual_bound = SCIPinfinity(scip);
  conshdlrdata->unchecked_trysol_solutions = new std::unordered_set<int>();


  SCIP_CALL( SCIPincludeConshdlrBasic(scip, &conshdlr, "bendersconsbranch", "separates nmp feasibility cuts",
         -100, -5000000, 100, FALSE,
         enforce_lp_benders, enforce_pseudo_benders, check_benders, lock_benders,
         conshdlrdata) );

  //SCIP_CALL( SCIPsetConshdlrSepa(scip, conshdlr, sepa_lp, sepa_sol, 0, 100000000, FALSE) );
  SCIP_CALL( SCIPsetConshdlrFree(scip, conshdlr, free_conshdlr_benders) );

  assert(conshdlr != NULL);

  return SCIP_OKAY;
}
