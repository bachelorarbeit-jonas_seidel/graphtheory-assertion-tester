#include "semiassignment_branch.h"


struct SCIP_BranchruleData
{
  int ncons;
  SCIP_CONS** lor_constraints;
};

SCIP_RETCODE branch_semiassign_exec(SCIP* scip, SCIP_BRANCHRULE* branchrule, SCIP_Bool allowaddcons, SCIP_RESULT* result)
{
  int ncons = branchrule->branchruledata->ncons;
  // values to determine for branching decision:
  int* nfracs = new int[ncons];
  SCIP_Real* totfracs = new SCIP_Real[ncons];
  SCIP_Real* halffracs = new SCIP_Real[ncons];
  // best state:
  int best_nfrac = 0;
  SCIP_Real best_frac_dist = SCIPinfinity(scip);
  int best_nvars;
  SCIP_VAR** best_vars;


  // iterate over all branchable constraints to determine the above mentioned values
  for(size_t constraint_index = 0; constraint_index < ncons; constraint_index++){
    // we need to iterate over all variable values; for this we get:
    // getnvar of constraint:
    int nvars = 0;
    SCIP_CONS* curr_cons = branchrule->branchruledata->lor_constraints[constraint_index];
    SCIP_Bool success_nvars = FALSE;
    SCIP_CALL(curr_cons->conshdlr->consgetnvars(scip, curr_cons->conshdlr, curr_cons, &nvars, &success_nvars));
    if(success_nvars != TRUE){
      SCIPABORT();
    }
    // getvars:
    SCIP_VAR** vars = new SCIP_VAR*[nvars];
    SCIP_Bool success_vars = FALSE;
    SCIP_CALL(curr_cons->conshdlr->consgetvars(scip, curr_cons->conshdlr, curr_cons, vars, nvars, &success_vars));
    if(success_vars != TRUE){
      SCIPABORT();
    }
    // values to determine:
    nfracs[constraint_index] = 0;
    halffracs[constraint_index] = 0;
    totfracs[constraint_index] = 0;

    // sorting the array:
    class fractional_component_ordering{
      SCIP* _scip;
    public:
      fractional_component_ordering(SCIP* scip) : _scip(scip){}
      bool operator()(SCIP_VAR* a, SCIP_VAR* b){
        return SCIPfeasFrac(this->_scip, SCIPvarGetLPSol(a)) > SCIPfeasFrac(this->_scip, SCIPvarGetLPSol(b));
      }
    } fractional_component_ordering_instance(scip);

    std::sort(vars, vars+nvars, fractional_component_ordering_instance);

    // iterating over said array to determine the above sought values:
    int variable_index = 0;
    while(variable_index < nvars && !SCIPisFeasIntegral(scip, SCIPvarGetLPSol(vars[variable_index]))){
      nfracs[constraint_index]++;
      totfracs[constraint_index] += SCIPfeasFrac(scip, SCIPvarGetLPSol(vars[variable_index]));
      if(variable_index % 2 == 0){
        halffracs[constraint_index] += SCIPfeasFrac(scip, SCIPvarGetLPSol(vars[variable_index]));
      }

      variable_index++;
    }

    if(nfracs[constraint_index] > best_nfrac || (nfracs[constraint_index] == best_nfrac && std::abs(totfracs[constraint_index]/2 - halffracs[constraint_index]) < best_frac_dist)){
      best_nfrac = nfracs[constraint_index];
      best_frac_dist = std::abs(totfracs[constraint_index]/2 - halffracs[constraint_index]);
      best_nvars = nvars;
      best_vars = vars;
    }else{
      delete[] vars;
    }
  }

  // branching on determined constraint:
  int nhalfvars_a = (int) std::ceil(((double) best_nvars)/2);
  SCIP_VAR** halfvars_a = new SCIP_VAR*[nhalfvars_a];
  int nhalfvars_b = (int) std::floor(((double) best_nvars)/2);
  SCIP_VAR** halfvars_b = new SCIP_VAR*[nhalfvars_b];

  /*for(int variable_index = 0; variable_index < best_nvars; variable_index++){
    if(variable_index % 2 == 0){
      halfvars_a[variable_index/2] = best_vars[variable_index];
    }else{
      halfvars_b[variable_index/2] = best_vars[variable_index];
    }
  }*/
  halfvars_a[0] = best_vars[1];
  halfvars_a[1] = best_vars[2];
  halfvars_b[0] = best_vars[0];

  SCIP_Real est = SCIPgetLocalOrigEstimate(scip);

  SCIP_CONS* cons_a;
  SCIP_CALL(SCIPcreateConsSemiassign(scip, &cons_a, "", nhalfvars_a, halfvars_a));
  SCIP_NODE* node_a;
  SCIP_CALL(SCIPcreateChild(scip, &node_a, 0, est));

  SCIP_CALL(SCIPaddConsNode(scip, node_a, cons_a,	node_a));


  SCIP_CONS* cons_b;
  SCIPcreateConsSemiassign(scip, &cons_b, "", nhalfvars_b, halfvars_b);
  SCIP_NODE* node_b;
  SCIP_CALL(SCIPcreateChild(scip, &node_b, 0, est));

  SCIP_CALL(SCIPaddConsNode(scip, node_b, cons_b,	node_b));

  *result = SCIP_BRANCHED;
  return SCIP_OKAY;
}

SCIP_RETCODE SCIPincludeBranchruleSemiassign(
   SCIP*                  scip,
   int                    ncons,
   SCIP_CONS**            lor_constraints
   )
{
  std::cout << "included branchrule semiassign" << std::endl;
  SCIP_BRANCHRULEDATA* branchruledata;
  SCIP_BRANCHRULE* branchrule;

  SCIP_CALL( SCIPallocBlockMemory(scip, &branchruledata) );
  branchruledata->ncons = ncons;
  branchruledata->lor_constraints = lor_constraints;


  SCIP_CALL( SCIPincludeBranchruleBasic(scip, &branchrule, "branch_semiassign", "branching on most-num-frac/most-frac constraint by setting half of the fractional part 0", 100000,
            -1, 1, branchruledata) );

  assert(branchrule != NULL);

  SCIP_CALL( SCIPsetBranchruleExecLp(scip, branchrule, branch_semiassign_exec) );

  return SCIP_OKAY;
}
