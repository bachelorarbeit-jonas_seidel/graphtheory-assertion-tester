#ifndef MAINTENANCE_PROBLEM_TEST_DATA_FUNCTIONS_IPP
#define MAINTENANCE_PROBLEM_TEST_DATA_FUNCTIONS_IPP

#include "../../../../Tester_Performance/Generic_Performance_Tester.h"
#include "../maintenance_problem_generator.h"


#include <map>
#include <vector>
#include <cstddef>

// macros to set vals

#define SET_VARIABLE(container, variable, new_val) \
if(check_all_problem_data){ \
  if(container.variable.first && new_val != container.variable.second){ \
    std::cout << "overriding " << #variable << " for " << data.path << "->" << data.name << "(previous: " << container.variable.second << ", new: " << new_val << ")" << std::endl; \
    container.variable.first = false; \
  } \
} \
if(!container.variable.first){ \
  container.variable.second = new_val; \
  container.variable.first = true; \
}

#define PRINT_IF_DEF(stream, container, variable) \
if(container.variable.first){ \
  stream << #variable << ": " << container.variable.second << "\n"; \
}

#define IF_READ_VAR_SET_VAR_VALID_DIRECT(stream, container, variable, current_string) \
if(current_string == #variable ":" ){ \
  stream >> container.variable.second; \
  container.variable.first = true; \
}
#define IF_READ_VAR_SET_VAR_VALID_GETLINE(stream, container, variable, current_string) \
if(current_string == #variable ":" ){ \
  std::getline(stream, current_string); \
  container.variable.second = current_string; \
  container.variable.first = true; \
}
#define IF_READ_VAR_SET_VAR_GETLINE(stream, container, variable, current_string) \
if(current_string == #variable ":" ){ \
  std::getline(stream, current_string); \
  container.variable = current_string; \
}

#define IF_READ_VAR_SET_VAR_DIRECT(stream, container, variable, current_string) \
if(current_string == #variable ":" ){ \
  stream >> container.variable; \
}

#define AVG_PERF_OVER_VEC(vector, attribute) \
for(const Derived_Performance_Data& perf_data : vector){ \
  attribute += perf_data.attribute.second; \
} \
attribute /= vector.size();

#define MEDIAN_PERF_OVER_VEC(vector, attribute) \
{ \
  std::set<double> attribute_vector; \
  for(const Derived_Performance_Data& perf_data : vector){ \
    attribute_vector.insert(perf_data.attribute.second); \
  } \
  auto iter = attribute_vector.begin(); \
  for(int count = 1; count < attribute_vector.size()/2; ++count){ \
    ++iter; \
  } \
  if((attribute_vector.size()/2)*2 == attribute_vector.size()){ \
    attribute = *iter; \
  }else{ \
    attribute = (*iter + *(++iter))/2; \
  } \
}


struct axis_data{
  std::string name;
  size_t      resolution;
  double      lower_bound;
  double      upper_bound;
};

struct Derived_Problem_Data{
  std::pair<bool, size_t> number_of_edges;
  std::pair<bool, size_t> number_of_nodes;
  std::pair<bool, double> avg_incid_per_node;
  std::pair<bool, size_t> number_of_critical_edges;
  std::pair<bool, double> share_of_critical;
  std::pair<bool, size_t> number_of_epochs;
  std::pair<bool, size_t> path_length_lower_bound;

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;
  bool values_complete() const ;
};
std::ostream& operator<<(std::ostream& os, const Derived_Problem_Data& prob_data);
std::istream& operator>>(std::istream& is, Derived_Problem_Data& prob_data);


struct Derived_Performance_Data{
  std::pair<bool, double> time_in_sec;
  std::pair<bool, int>    number_of_bnb_runs;
  std::pair<bool, int>    number_of_reopt_runs;
  std::pair<bool, int>    number_of_nodes_explored;
  std::pair<bool, int>    max_depth;
  std::pair<bool, double> dual_bound;
  std::pair<bool, double> primal_bound;
  std::pair<bool, double> gap;
  std::pair<bool, int>    number_of_primal_sols;

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;
  static std::string csv_data_avg(const std::vector<Derived_Performance_Data>& exec_vector, std::string separator);
  static std::string csv_data_median(const std::vector<Derived_Performance_Data>& exec_vector, std::string separator);
  bool values_complete() const ;
};
std::ostream& operator<<(std::ostream& os, const Derived_Performance_Data& exec_data);
std::istream& operator>>(std::istream& is, Derived_Performance_Data& exec_data);
std::istream& operator>>(std::istream& is, std::vector<Derived_Performance_Data>& exec_data);
std::istream& operator>>(std::istream& is, std::map<std::string, std::vector<Derived_Performance_Data>>& formulation_exec_data);

struct Data{
  bool                                                          marked;
  std::string                                                   path;
  std::string                                                   name;
  maintenance_problem_generator                                 generator;
  Maintenance_Problem                                           mp;
  Derived_Problem_Data                                          derived_problem;
  std::map<std::string, std::vector<Derived_Performance_Data> > derived_performance;

  static std::string csv_columns_incl_pos(const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, std::string prefix, std::string separator);

  static std::string csv_data_avg_or_median_at_given_pos(const std::pair<double, double> position, const std::vector<Data>& data_vector, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, bool avg_or_med, std::string separator);
  static std::string csv_data_avg_unknown_but_coherent_pos(const std::vector<Data>& data_vector, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, std::string separator);
  std::string csv_singular_data_incl_pos(const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, std::string separator) const ;

  bool values_complete() const ;
};
std::ostream& operator<<(std::ostream& os, const Data& data);
std::istream& operator>>(std::istream& is, Data& data);


#endif
