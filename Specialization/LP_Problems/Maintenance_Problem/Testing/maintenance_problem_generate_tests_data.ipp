#include <exception>
#include <filesystem>
#include <cstdlib>

bool generate_tests_data(std::filesystem::path path, int number_of_instances){
  std::cerr << "-----------------> generation started in " << path.string() << std::endl;
  // reading path from arguments
  std::filesystem::path param_path = path/"test_parameters.mpg";
  std::cerr << "problem generation: reading " << path.string() << std::endl;

  // read generation parameters from top level folder
  std::fstream test_generator_file;
  test_generator_file.open(param_path, std::ios::in);
  maintenance_problem_generator test_generator(test_generator_file);

  std::cerr << "problem generation: generator is given by" << std::endl;

  // populate folder with instances
  for(int instance = 0; instance < number_of_instances; ++instance){
    std::filesystem::path current_file_path = param_path.parent_path()/std::to_string(instance)/"instance.test_data";

    // check if the folder has been populated with an instance before
    if(std::filesystem::status(current_file_path.parent_path()).type() == std::filesystem::file_type::directory){
      std::cerr << current_file_path.parent_path() << " already exists, checking validity" << std::endl;


      // try reading said instance
      try{
        std::fstream data_file;
        data_file.open(current_file_path, std::ios::in);

        Data curr_data = Data{};
        data_file >> curr_data;
        data_file.close();

        if( curr_data.generator != test_generator ) throw std::runtime_error("generator mismatch");

        std::cerr << "data appears correct; skipping" << std::endl;
        continue;
      }catch(std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        std::cerr << "data check failed; overriding" << std::endl;
      }catch(std::runtime_error& e){
        std::cerr << e.what() << std::endl;
        std::string curr;
        std::cout << "Do you wish to continue? [y/n]" << std::endl;
        while( (std::cin >> curr) && curr != "y" && curr != "n"){
          std::cout << "Do you wish to continue? [y/n]" << std::endl;
        }
        if(curr == "n"){
          std::cerr << "aborting according to user choice" << std::endl;
          exit(1);
        }else{
          std::cerr << "overriding" << std::endl;
        }
      }
    }else{
      assert(std::filesystem::create_directory(current_file_path.parent_path()));
    }


    Data new_data = {};
    std::cerr << "problem generation: generation " << instance << ": " << "[metadata]" << std::flush;
    new_data.marked = false;
    new_data.path = current_file_path.parent_path().string();
    new_data.name = current_file_path.filename().string();
    new_data.generator = test_generator;

    std::cerr << " [problem]" << std::flush;
    new_data.mp = std::move(test_generator.next());

    std::cerr << " [empty derived data]" << std::endl;
    new_data.derived_problem = Derived_Problem_Data{
      {false, 0},
      {false, 0},
      {false, 0},
      {false, 0},
      {false, 0},
      {false, 0},
      {false, 0}
    };
    new_data.derived_performance = {};

    std::cerr << "problem generation: writing instance data" << "\n" << std::endl;
    std::fstream data_file;
    data_file.open(current_file_path, std::ios::out);
    data_file << new_data << std::endl;
    data_file.close();
  }

  std::cerr << std::endl;

  return true;
}
