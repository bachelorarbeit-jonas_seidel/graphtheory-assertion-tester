#ifndef CRITICAL_SELECTION_POLICY_H
#define CRITICAL_SELECTION_POLICY_H

#include <sstream>
#include <iostream>
#include <exception>
#include <stdexcept>

#include <unordered_set>
#include <iostream>

#include "../../../Graphtheory/Graph.h"

enum critical_selection_policy_enum{
  everywhere, core_network, network_cores, aggregated_networks, network_connectors
};

class critical_selection_policy{
public:
   critical_selection_policy_enum selection_rule;

  critical_selection_policy() : selection_rule(everywhere) {}
  critical_selection_policy(critical_selection_policy_enum param) : selection_rule(param) {}

  std::unordered_set<Edge*> critical_edge_candidates(Graph& g, const std::unordered_set<Edge*>& core_network, const std::unordered_set<Edge*>& network_cores, const std::unordered_set<Edge*>& aggregated_networks, const std::unordered_set<Edge*>& network_connectors) const ;
  size_t number_of_critical_candidates(Graph& g, const std::unordered_set<Edge*>& core_network, const std::unordered_set<Edge*>& network_cores, const std::unordered_set<Edge*>& networks, const std::unordered_set<Edge*>& connectors) const ;

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  bool operator==(const critical_selection_policy& other) const ;
  bool operator!=(const critical_selection_policy& other) const {
    return !(*this == other);
  }

  critical_selection_policy(std::istream& is);
  friend std::ostream& operator<<(std::ostream& os, const critical_selection_policy& tipping_parameters);
  friend std::istream& operator>>(std::istream& is, critical_selection_policy& tipping_parameters);
};

std::ostream& operator<<(std::ostream& os, const critical_selection_policy& tipping_parameters);
std::istream& operator>>(std::istream& is, critical_selection_policy& tipping_parameters);

#endif
