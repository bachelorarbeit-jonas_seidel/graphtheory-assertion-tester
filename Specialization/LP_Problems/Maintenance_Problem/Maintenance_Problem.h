#ifndef MAINTENANCE_PROBLEM_H
#define MAINTENANCE_PROBLEM_H

#include <cstddef>
#include <vector>
#include <sstream>

#include "../../../Graphtheory/Graphtheory.h"
#include "../../../Linear_Programming/Linear_Program.h"
#include "../../../Linear_Programming/lp_generator.h"


#include <scip/scip.h>
#include <scip/scipdefplugins.h>

class Maintenance_Problem : public Linear_Program {
  size_t _number_of_epochs;
  Graph _g;
  Node* _source;
  Node* _target;
public:
  Maintenance_Problem();
  Maintenance_Problem(const Maintenance_Problem& mp);
  Maintenance_Problem(Maintenance_Problem&& mp);
  Maintenance_Problem(const Graph& g, const Node* source, const Node* target, size_t intervals);

  const Graph& network() const ;
  const Node* source() const ;
  const Node* target() const ;
  size_t number_of_epochs() const;

  std::vector<SCIP*> all_computational_models();

  void operator=(const Maintenance_Problem& mp);
  void operator=(Maintenance_Problem&& mp);

  Maintenance_Problem(std::istream& is);
  friend std::ostream& operator<<(std::ostream& os, const Maintenance_Problem& mp);
  friend std::istream& operator>>(std::istream& os, Maintenance_Problem& mp);
};

std::ostream& operator<<(std::ostream& os, const Maintenance_Problem& mp);
std::istream& operator>>(std::istream& iis, Maintenance_Problem& mp);


#endif
