#ifndef PATH_H
#define PATH_H

#include <cstddef>
#include <vector>
#include <unordered_map>
#include <list>
#include <iostream>

#include "Node.h"
#include "Edge.h"

#include "graphtheory_exceptions.h"

class Graph;

// TODO: formulate in terms of SubGraph
class Path{
  Graph* _owner;
  Node* _first;
  Node* _last;
  bool _exists;
  bool _is_loop;
  std::unordered_map<Node*, Edge*> _next_edge;
  std::unordered_map<Node*, Edge*> _prev_edge;
public:
  Path(Graph* g, std::unordered_map<Node*, Edge*>& successor_matrix, Node* start, bool invert_order = true);
  Path(Graph* g, std::unordered_map<Node*, Edge*>& successor_matrix, Node* start, Node* end, bool invert_order = true);


  Graph* graph(){
    return this->_owner;
  }
  const Graph* graph() const {
    return this->_owner;
  }

  Node* first(){
    return this->_first;
  }
  const Node* first() const {
    return this->_first;
  }

  Node* last(){
    return this->_last;
  }
  const Node* last() const {
    return this->_last;
  }

  bool exists() const {
    return this->_exists;
  }

  bool is_loop() const {
    return this->_is_loop;
  }



  std::unordered_map<Node*, Edge*>& next_edge_lookup(){
    return this->_next_edge;
  }

  const std::unordered_map<const Node*, const Edge*> next_edge_lookup() const {
    return std::unordered_map<const Node*, const Edge*>(this->_next_edge.begin(), this->_next_edge.end());
  }

  std::unordered_map<Node*, Edge*>& prev_edge_lookup(){
    return this->_prev_edge;
  }

  const std::unordered_map<const Node*, const Edge*> prev_edge_lookup() const {
    return std::unordered_map<const Node*, const Edge*>(this->_prev_edge.begin(), this->_prev_edge.end());
  }

  int number_of_edges() const {
    size_t length = this->_next_edge.size();
    assert(length == this->_prev_edge.size());
    return this->exists() ? length : -1;
  }

  /*
    optimization
  */
  double worst_along_path(std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr);
  double augment_edges_along_path(std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr);

  friend std::ostream& operator<<(std::ostream& os, const Path& p);
};

std::ostream& operator<<(std::ostream& os, const Path& p);

#include "Graph.h"

#endif
