std::unordered_set<Node*> SubGraph::conditional_bfs_all_reachable(
  const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(Edge* via, Node* node)>& node_exec,
  const std::unordered_set<Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) {
  return Graph::conditional_bfs_all_reachable(edge_exec, node_exec, starting_nodes, all_paths, guide, mask);
}
std::unordered_set<Node*> SubGraph::conditional_bfs_all_reachable(
  const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(const Edge* via, const Node* node)>& node_exec,
  const std::unordered_set<const Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) const {
  std::unordered_set<Node*> mutable_starting_nodes;
  for(const Node* n : starting_nodes){
    mutable_starting_nodes.insert(const_cast<Node*>(n));
  }
  return const_cast<SubGraph*>(this)->conditional_bfs_all_reachable(edge_exec, node_exec, mutable_starting_nodes, all_paths, guide, mask);
}
void SubGraph::conditional_bfs_all_components(
  const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(Edge* via, Node* node)>& node_exec,
  const std::unordered_set<Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) {
  Graph::conditional_bfs_all_components(edge_exec, node_exec, starting_nodes, all_paths, guide, mask);
}
void SubGraph::conditional_bfs_all_components(
  const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(const Edge* via, const Node* node)>& node_exec,
  const std::unordered_set<const Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) const {
  std::unordered_set<Node*> mutable_starting_nodes;
  for(const Node* n : starting_nodes){
    mutable_starting_nodes.insert(const_cast<Node*>(n));
  }
  const_cast<SubGraph*>(this)->conditional_bfs_all_components(edge_exec, node_exec, mutable_starting_nodes, all_paths, guide, mask);
}
bool SubGraph::is_strongly_connected(
  const std::function<bool(const Node* from, const Edge* via)>& guide_forward,
  const std::function<bool(const Node* from, const Edge* via)>& guide_backward,
  const std::function<bool(const Node* node)>& mask
) const {
  return Graph::is_strongly_connected(guide_forward, guide_backward, mask);
}
bool SubGraph::is_weakly_connected(
  const std::function<bool(const Node* from, const Edge* via)>& guide_forward,
  const std::function<bool(const Node* from, const Edge* via)>& guide_backward,
  const std::function<bool(const Node* node)>& mask
) const {
  return Graph::is_weakly_connected(guide_forward, guide_backward, mask);
}
