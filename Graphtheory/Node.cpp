#include "Node.h"
#include "../Common/io_format.h"
#include "Edge.h"

std::unordered_set<Edge*> Node::incoming(){
  std::unordered_set<Edge*> incoming_edges;
  for(Edge* e : this->incident()){
    if(e->to() == this){
      incoming_edges.insert(e);
    }
  }

  return incoming_edges;
}

std::unordered_set<Edge*> Node::outgoing(){
  std::unordered_set<Edge*> outgoing_edges;
  for(Edge* e : this->incident()){
    if(e->from() == this){
      outgoing_edges.insert(e);
    }
  }

  return outgoing_edges;
}


Node::Node(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "Node");

  is >> this->_description;

  is >> curr;
  IO_THROW(curr, "(");
  is >> curr;
  while(curr != ")"){
    std::string attr_name = curr;
    is >> curr;
    IO_THROW(curr, "=");
    Attribute attr(is);
    this->_attributes.insert({attr_name, attr});
    is >> curr;
  }
  is >> curr;
  IO_THROW(curr, "{");
  size_t counter = 1;
  while(counter != 0){

    if( !(is >> curr) ) throw std::invalid_argument("Node as open brackets in formatting");
    if(curr == "{"){
      ++counter;
    }else if(curr == "}"){
      --counter;
    }
  }
}
/*
bool Node::is_target() const {
  for(Edge* e : this->incident()){
    if(this == e->from()) return false;
  }
  return true;
}

bool Node::is_source() const {
  for(Edge* e : this->incident()){
    if(this == e->to()) return false;
  }
  return true;
}
*/
Node::~Node(){
  while(this->incident().begin() != this->incident().end()){
    delete *this->incident().begin();
  }
}

std::ostream& operator<<(std::ostream& os, const Node& n){
  if(&os == &std::cout){
    os << "\033[0;36m";
    os << "Node " << n.description() << " ( ";
    for(std::pair<std::string, Attribute> pair : n.attributes()){
      os << pair.first << " = " << pair.second.value() << ", ";
    }
    os << ") {\n \tEdges {\n";
    for(Edge* e : n.incident()){
      os << *e << "\n";
    }
    os << "\033[0;36m";
    os << "} }";
    os << "\033[0m";
  }else{
    os << "Node " << n.description() << " ( ";
    for(std::pair<std::string, Attribute> pair : n.attributes()){
      os << pair.first << " = " << pair.second << " ";
    }

    os << ") {\n \tEdges {\n";
    for(Edge* e : n.incident()){
      os << *e << "\n";
    }
    os << "} }";
  }
  return os;
}

std::istream& operator>>(std::istream& is, Node& n) { n = Node(is); return is;}

void Node::dump(){
  std::cout << "\033[0;36m";
  std::cout << "Node " << this->description() << "; " << this << " ( ";
  for(std::pair<std::string, Attribute> pair : this->attributes()){
    std::cout << pair.first << " = ";
    pair.second.dump();
    std::cout << ", ";
  }
  std::cout << ") {\n \tEdges {\n";
  for(Edge* e : this->incident()){
    e->dump();
    std::cout << "\n";
  }
  std::cout << "\033[0;36m";
  std::cout << "} }";
  std::cout << "\033[0m";
}
