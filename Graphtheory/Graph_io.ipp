#include "../Common/io_format.h"

Graph::Graph(std::istream& is){
  std::unordered_map<std::string, Node* > name_lookup;
  std::string curr;
  is >> curr;
  IO_THROW(curr, "Graph");
  is >> curr;
  IO_THROW(curr, "{");
  is >> curr;
  IO_THROW(curr, "Nodes");
  is >> curr;
  IO_THROW(curr, "{");
  auto pos = is.tellg();
  while( (is >> curr) && (curr != "}") ){
    is.seekg(pos);
    Node* tmp = new Node(is);
    name_lookup.insert({tmp->description(), tmp});
    this->nodes().insert(tmp);
    pos = is.tellg();
  }
  is >> curr;
  IO_THROW(curr, "Edges");
  is >> curr;
  IO_THROW(curr, "{");
  pos = is.tellg();
  while( (is >> curr) && (curr != "}") ){
    is.seekg(pos);
    Edge* tmp = new Edge(is, name_lookup);
    this->edges().insert(tmp);
    pos = is.tellg();
  }
  is >> curr;
  IO_THROW(curr, "Node_Defaults");
  is >> curr;
  IO_THROW(curr, "{");
  while( (is >> curr) && (curr != "}") ){
    std::string name = curr;
    is >> curr;
    IO_THROW(curr, "=");
    Attribute tmp(is);
    this->template_node_attributes().insert({name, tmp});
  }
  is >> curr;
  IO_THROW(curr, "Edge_Defaults");
  is >> curr;
  IO_THROW(curr, "{");
  while( (is >> curr) && (curr != "}") ){
    std::string name = curr;
    is >> curr;
    IO_THROW(curr, "=");
    Attribute tmp(is);
    this->template_edge_attributes().insert({name, tmp});
  }
  is >> curr;
  IO_THROW(curr, "}");

  this->_borrower = nullptr;
}


std::ostream& operator<<(std::ostream& os, const Graph& g){
  if(&os == &std::cout){
    os << "\033[0;31m";
    os << "Graph { Nodes { \n";
    for(const Node* n : g.local_nodes()){
      os << *n << "\n";
    }
    os << "\033[0;31m";
    os << "} Edges {" << "\n";
    for(const Edge* e : g.local_edges()){
      os << *e << "\n";
    }
    os << "\033[0;31m";
    os << "} \nNode_Defaults { ";
    for(std::pair<std::string, Attribute> attr : g.template_node_attributes()){
      os << attr.first << " = " << attr.second << " ";
    }
    os << "} \nEdge_Defaults { ";
    for(std::pair<std::string, Attribute> attr : g.template_edge_attributes()){
      os << attr.first << " = " << attr.second << " ";
    }
    os << "}\n}";
    os << "\033[0m";
  }else{
    os << "Graph { Nodes { \n";
    for(const Node* n : g.nodes()){
      os << *n << "\n";
    }
    os << "} Edges {" << "\n";
    for(const Edge* e : g.edges()){
      os << *e << "\n";
    }
    os << "} \nNode_Defaults { ";
    for(std::pair<std::string, Attribute> attr : g.template_node_attributes()){
      os << attr.first << " = " << attr.second << " ";
    }
    os << "} \nEdge_Defaults { ";
    for(std::pair<std::string, Attribute> attr : g.template_edge_attributes()){
      os << attr.first << " = " << attr.second << " ";
    }
    os << "}\n}";
  }
  return os;

  // TODO implement defaults output
}

std::istream& operator>>(std::istream& is, Graph& g){g = Graph(is); return is;}


void Graph::dump(){
  std::cout << "\033[0;31m";
  std::cout << "Graph " << this << " { Nodes { \n";
  for(Node* n : this->local_nodes()){
    n->dump();
    std::cout << "\n";
  }
  std::cout << "\033[0;31m";
  std::cout << "} Edges {" << "\n";
  for(Edge* e : this->local_edges()){
    e->dump();
    std::cout << "\n";
  }
  std::cout << "\033[0;31m";
  std::cout << "} }";
  std::cout << "\033[0m";
}
