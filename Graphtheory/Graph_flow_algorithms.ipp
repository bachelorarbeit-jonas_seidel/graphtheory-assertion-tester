double Graph::ford_fulkerson(Node* s, Node* t, std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr){
  // this implementation currently does not respect validity of the resulting flow wrt lower_limit_attr, meaning that if flow 0 for instance the flow might not be valid

  double value = 0;

  while(true){
    Path path = this->directed_admissible_st_path(s,t, opt_attr, lower_limit_attr, upper_limit_attr);

    if(path.exists()) {
      value += path.augment_edges_along_path(opt_attr, lower_limit_attr, upper_limit_attr);
    }else{
      break;
    }
  }

  return value;
}
