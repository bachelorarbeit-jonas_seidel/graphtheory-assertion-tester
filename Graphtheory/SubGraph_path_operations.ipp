Path SubGraph::directed_admissible_st_path(
  Node* s, Node* t,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) {
  return Graph::directed_admissible_st_path(s, t, guide, mask);
}
const Path SubGraph::directed_admissible_st_path(
  const Node* s, const Node* t,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) const {
  return const_cast<SubGraph*>(this)->directed_admissible_st_path(s, t, guide, mask);
}
