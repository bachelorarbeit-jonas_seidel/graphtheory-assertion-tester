// TODO: add edge restriction and keep up to date in add_edge functions
Edge* SubGraph::add_edge(Node* from, Node* to, const std::string& description, const std::unordered_map<std::string,Attribute>& edge_attributes){
  auto tmp = this->borrow(Graph::add_edge(from,to,description, edge_attributes));
  return tmp;
}
Edge* SubGraph::add_edge(const Edge* remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup){
  return this->borrow(Graph::add_edge(remote_edge, node_lookup));
}
std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> SubGraph::add_edges(const std::unordered_set<Edge*>& remote_edges, const std::unordered_map<const Node*, Node*>& node_lookup){
  return this->add_edges(std::unordered_set<const Edge*>(remote_edges.begin(), remote_edges.end()), node_lookup);
}
std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> SubGraph::add_edges(const std::unordered_set<const Edge*>& remote_edges, const std::unordered_map<const Node*, Node*>& node_lookup){
  auto [new_edges, edge_lookup] = Graph::add_edges(remote_edges, node_lookup);
  return {this->borrow(std::move(new_edges)), edge_lookup};
}
Node* SubGraph::add_node(std::string description, const std::unordered_map<std::string, Attribute>& node_attributes){
  return this->borrow(Graph::add_node(description, node_attributes));
}
Node* SubGraph::add_node(const Node* remote_node){
  return this->borrow(Graph::add_node(remote_node));
}
std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*> > SubGraph::add_nodes(const std::unordered_set<Node*>& remote_nodes){
  auto [nodes, lookup] = Graph::add_nodes(remote_nodes);
  return {this->borrow(nodes), lookup};
}
std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*> > SubGraph::add_nodes(const std::unordered_set<const Node*>& remote_nodes){
  auto [nodes, lookup] = Graph::add_nodes(remote_nodes);
  return {this->borrow(nodes), lookup};
}


void SubGraph::remove_node(Node* node){
  Graph::remove_node(node);
}
void SubGraph::remove_edge(Edge* edge){
  Graph::remove_edge(edge);
}

std::ostream& operator<<(std::ostream& os, const SubGraph& g){
  os << *(Graph*) &g;
  return os;
}
