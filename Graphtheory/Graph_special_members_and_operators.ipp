Graph::Graph(std::unordered_map<std::string, Attribute> default_values_edge_attributes, std::unordered_map<std::string, Attribute> default_values_node_attributes) : _lifetime_node_counter(0), _lifetime_edge_counter(0), _template_node_attributes(std::move(default_values_node_attributes)), _template_edge_attributes(std::move(default_values_edge_attributes)), _borrower(nullptr) {}

Graph::Graph(const Graph& graph){
  *this = graph;
}

Graph::Graph(Graph&& graph){
  this->_borrower = nullptr;
  this->_nodes = {};
  this->_edges = {};
  this->_template_node_attributes = {};
  this->_template_edge_attributes = {};
  *this = std::move(graph);
}

std::tuple< Graph, std::unordered_map<const Node*, Node*>, std::unordered_map<const Edge*, Edge*> > Graph::copy_ret_lookups() const {
  Graph copy (this->_template_edge_attributes, this->_template_node_attributes);

  const auto& [added_nodes, node_lookup] = copy.add_nodes(this->_nodes);
  const auto& [added_edges, edge_lookup] = copy.add_edges(this->_edges, node_lookup);

  copy._borrower = nullptr;

  return {std::move(copy), std::move(node_lookup), std::move(edge_lookup)};
}

void Graph::operator=(const Graph& graph){
  *this = std::move(std::get<0>(graph.copy_ret_lookups()));
}

void Graph::operator=(Graph&& graph){
  if(this->is_borrowed()) throw std::runtime_error("cannot copy/move into a graph with active subgraphs");
  if(graph.is_borrowed()) throw std::runtime_error("cannot move out of graph with active subgraphs");

  // releasing presently stored graph elements
  while(this->nodes().begin() != this->nodes().end()){
    this->remove_node(*this->nodes().begin());
  }

  this->_edges = std::move(graph._edges);
  this->_lifetime_edge_counter = graph._lifetime_edge_counter;
  this->_nodes = std::move(graph._nodes);
  this->_lifetime_node_counter = graph._lifetime_node_counter;
  this->_template_node_attributes = std::move(graph._template_node_attributes);
  this->_template_edge_attributes = std::move(graph._template_edge_attributes);
  graph._nodes = {};
  graph._edges = {};
  graph._template_node_attributes = {};
  graph._template_edge_attributes = {};
}

Graph::Graph(const std::unordered_set<Graph>& graphs){
  *this = *graphs.begin();
  for(auto it = ++graphs.begin(); it != graphs.end(); ++it){
    this->join_with(*it);
  }
}

Graph::Graph(std::unordered_set<Graph>&& graphs){
  for(const Graph g : graphs){
    if(g.is_borrowed()) throw std::runtime_error("cannot move out of graph with active subgraphs");
  }

  *this = std::move(*graphs.begin());

  typedef std::unordered_set<Graph>::iterator Iter;

  for(auto it = std::move_iterator<Iter>(++graphs.begin()); it != std::move_iterator<Iter>(graphs.end()); ++it){
    this->join_with(*it);
  }
}

Graph::~Graph(){
  if(this->is_borrowed()) assert(false);
  this->remove_nodes(this->nodes());
}
