#include "random_attribute_generator.h"
#include "../../Common/io_format.h"
#include <climits>

std::default_random_engine random_attribute_generator::_engine((std::random_device())());

void random_attribute_generator::operator=(const random_attribute_generator& other){
  this->_data = other._data;
}
void random_attribute_generator::operator=(random_attribute_generator&& other){
  this->_data = std::move(other._data);
}

std::unordered_map<std::string, Attribute> random_attribute_generator::next(){
  std::unordered_map<std::string, Attribute> generated;

  for(auto [name, prop] : this->_data){
    auto [direction, integr, lower, upper] = prop;

    switch(integr){
      case Continuous:
        generated.insert({
          name,
          {
            direction,
            std::uniform_real_distribution<double>(lower, upper)(random_attribute_generator::_engine),
            Continuous
          }
        });
        break;
      case Integral:
        generated.insert({
          name,
          {
            direction,
            (double)std::uniform_int_distribution<int>(lower, upper)(random_attribute_generator::_engine),
            Integral
          }
        });
        break;
      default:
        throw std::runtime_error("invalid integrality type");
    }
  }

  return generated;
}

void random_attribute_generator::operator>>(std::unordered_map<std::string, Attribute>& var){
  var = this->next();
}

bool random_attribute_generator::operator==(const random_attribute_generator& other) const {
  for( auto& [oname, otpl] : other._data){
    for( auto& [name, tpl] : this->_data){
      auto [odir, ointe, olow, oup] = otpl;
      auto [dir, inte, low, up] = tpl;
      if( dir != odir ) return false;
      if( ointe != inte ) return false;
      if( std::abs((olow - low)/low) > CMP_EPS ) return false;
      if( std::abs((oup - up)/up) > CMP_EPS ) return false;
    }
  }
  return true;
}

std::string random_attribute_generator::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "random_attribute_generator";
  return csv_columns_stream.str();
}
std::string random_attribute_generator::csv_data(std::string separator) const {
  std::stringstream csv_data;
  csv_data << *this;
  return csv_data.str();
}

random_attribute_generator::random_attribute_generator(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "random_attribute_generator");
  is >> curr;
  IO_THROW(curr, "{");
  while( (is >> curr) && (curr != "}") ){
    std::string name;
    name = curr;
    is >> curr;
    IO_THROW(curr, "=");
    is >> curr;
    IO_THROW(curr, "{");
    opt_dir direction;
    is >> direction;
    integrality integr;
    is >> integr;
    double lower;
    is >> lower;
    double upper;
    is >> upper;
    is >> curr;
    IO_THROW(curr, "}");
    this->_data.insert({name, {direction, integr, lower, upper}});
  }
  if(!is) throw std::invalid_argument("stream did not contain complete random_attribute_generator object");
}

std::ostream& operator<<(std::ostream& os, const random_attribute_generator& attribute_generator){
  auto prev_precision = os.precision();
  os.precision(std::numeric_limits<double>::digits10 + 1);

  os << "random_attribute_generator" << " { ";
  for(auto [name, prop] : attribute_generator._data){
    auto [direction, integr, lower, upper] = prop;
    os << name << " = " << " { " << direction << " " << integr << " " << lower << " " << upper << " } ";
  }
  os << " } ";

  os.precision(prev_precision);
  return os;
}
std::istream& operator>>(std::istream& is, random_attribute_generator& attribute_generator){
  attribute_generator = random_attribute_generator(is);
  return is;
}
