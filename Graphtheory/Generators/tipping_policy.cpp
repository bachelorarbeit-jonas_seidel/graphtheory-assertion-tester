#include "tipping_policy.h"

std::string tipping_policy::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "tip" << separator
                      << prefix << "shelter_orphans" << separator
                      << prefix << "only_tip_fringes" << separator
                      << prefix << "only_tip_extreme_layer";
  return csv_columns_stream.str();
}

std::string tipping_policy::csv_data(std::string separator) const {
  std::stringstream csv_data;
  csv_data << this->tip << separator
            << this->shelter_orphans << separator
            << this->only_tip_fringes << separator
            << this->only_tip_extreme_layer;
  return csv_data.str();
}

bool tipping_policy::operator==(const tipping_policy& other) const {
  if(this->tip != other.tip) return false;
  if(this->shelter_orphans != other.shelter_orphans) return false;
  if(this->only_tip_fringes != other.only_tip_fringes) return false;
  if(this->only_tip_extreme_layer != other.only_tip_extreme_layer) return false;
  return true;
}

tipping_policy::tipping_policy(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "tipping_policy");
  is >> curr;
  IO_THROW(curr, "{");
  is >> this->tip
      >> this->shelter_orphans
      >> this->only_tip_fringes
      >> this->only_tip_extreme_layer;
  is >> curr;
  IO_THROW(curr, "}");
  if(!is) throw std::invalid_argument("stream did not contain complete tipping_policy object");
}

std::ostream& operator<<(std::ostream& os, const tipping_policy& tipping_parameters){
  os << "tipping_policy" << " { "
      << tipping_parameters.tip << " "
      << tipping_parameters.shelter_orphans << " "
      << tipping_parameters.only_tip_fringes << " "
      << tipping_parameters.only_tip_extreme_layer << " } ";
  return os;
}

std::istream& operator>>(std::istream& is, tipping_policy& tipping_parameters){
  tipping_parameters = tipping_policy(is);
  return is;
}
