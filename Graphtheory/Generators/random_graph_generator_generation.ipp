
random_graph_generator& random_graph_generator::operator>>(Graph& var){
  var = std::get<5>(this->next());
  return *this;
}

std::tuple<std::pair<Node*, Node*>, std::unordered_set<Edge*>, std::unordered_set<Edge*>, std::unordered_set<Edge*>, std::unordered_set<Edge*>, Graph> random_graph_generator::next(){
  auto [st, network_connectors, structure_graph] = this->generate_flow_network(this->_structure_graph_tipping_parameters, this->_structure_graph_edge_generator, this->_structure_graph_node_generator, this->_structure_graph_edge_tipping_attribute_generator, this->_structure_graph_node_tipping_attribute_generator);
  std::unordered_set<Edge*> core_network = network_connectors;
  std::unordered_set<Edge*> aggregated_subnetwork_cores;
  std::unordered_set<Edge*> aggregated_subnetworks;

  for(Node* n : structure_graph.export_nodes()){
    if(n->attribute("Source").first || n->attribute("Target").first) continue;
    auto [cluster_st, local_core_network, cluster] = this->generate_flow_network(this->_cluster_tipping_parameters, this->_cluster_edge_generator, this->_cluster_node_generator, this->_cluster_edge_tipping_attribute_generator, this->_cluster_node_tipping_attribute_generator, structure_graph.lifetime_node_count());

    // updating edge_sets
    core_network.insert(cluster.export_edges().begin(), cluster.export_edges().end());
    aggregated_subnetworks.insert(cluster.export_edges().begin(), cluster.export_edges().end());
    aggregated_subnetwork_cores.insert(local_core_network.begin(), local_core_network.end());


    cluster_st.first->attribute_throwing("Source").value() = 0;
    cluster_st.second->attribute_throwing("Target").value() = 0;
    // replacing repr_node with cluster
    structure_graph.join_with(std::move(cluster));
    for(auto iter = n->incident().begin(); iter != n->incident().end(); ){
      Edge* e = *iter;
      ++iter;
      if( n == e->to() ){
        e->reconnect(e->from(), cluster_st.first);
      }else if( e->to() == e->from() ){
        e->reconnect(cluster_st.second, cluster_st.first);
      }else{
        e->reconnect(cluster_st.second, e->to());
      }
      std::stringstream suffix;
      suffix << "__" << e;
      e->update_autolabel("", suffix.str());
    }
    structure_graph.remove_node(n);

    std::unordered_set<Edge*> edges = cluster.export_edges();
    core_network.insert(edges.begin(), edges.end());
    aggregated_subnetwork_cores.insert(local_core_network.begin(), local_core_network.end());
  }

  std::cout << "generation resulted in networks of size:" << std::endl;
  std::cout << "core_network: " << core_network.size() << std::endl;
  std::cout << "aggregated_subnetwork_cores: " << aggregated_subnetwork_cores.size() << std::endl;
  std::cout << "aggregated_subnetworks: " << aggregated_subnetworks.size() << std::endl;
  std::cout << "network_connectors: " << network_connectors.size() << std::endl;
  std::cout << "complete.size(): " << structure_graph.size().nodes << "," << structure_graph.size().edges << std::endl;

  return {std::move(st), std::move(core_network), std::move(aggregated_subnetwork_cores), std::move(aggregated_subnetworks), std::move(network_connectors), std::move(structure_graph)};
}

std::tuple<std::pair<Node*, Node*>, std::unordered_set<Edge*>, Graph> random_graph_generator::generate_flow_network(tipping_policy& tipping_parameters, edge_step_fuzzing_plugin& edge_generator, node_step_plugin& node_generator, random_attribute_generator& edge_attribute_generator, random_attribute_generator& node_attribute_generator, size_t node_index_start){
  Graph g(this->_default_edge_attributes, this->_default_node_attributes);
  g.lifetime_node_counter() = node_index_start;

  auto [node_steps, added_edges] = grow_random_in_steps(g, edge_generator, node_generator);

  std::unordered_set<Edge*> core_network = g.export_edges();

  Node* source = nullptr;
  Node* target = nullptr;

  if(!tipping_parameters.tip){
    // choose those as source / target that offer the best forward/backward reachibility
    size_t best_forward_reachability = 0;
    g.for_nodes([&g, &source, &best_forward_reachability](Node* n){
      size_t counter = 0;
      g.conditional_bfs_all_reachable(
        [](Node* from, Edge* via, bool used_in_traversal){},
        [&counter](Edge* via, Node* node){
          ++counter;
          return false;
        },
        {n},
        false,
        [](const Node* from, const Edge* via){return (via->from() == from);}
      );
      if(counter >= best_forward_reachability){
        best_forward_reachability = counter;
        source = n;
      }
    });

    size_t best_backwards_reachability = 0;
    g.for_nodes([&g, &target, &best_backwards_reachability](Node* n){
      size_t counter = 0;
      g.conditional_bfs_all_reachable(
        [](Node* from, Edge* via, bool used_in_traversal){},
        [&counter](Edge* via, Node* node){
          ++counter;
          return false;
        },
        {n},
        false,
        [](const Node* from, const Edge* via){return (via->to() == from);}
      );
      if(counter >= best_backwards_reachability){
        best_backwards_reachability = counter;
        target = n;
      }
    });

    if(!g.directed_admissible_st_path(source,target).exists()){
      std::cerr << "source / target combination is a unconnected pair; restarting local network generation" << std::endl;
      return generate_flow_network(tipping_parameters, edge_generator, node_generator, edge_attribute_generator, node_attribute_generator, node_index_start);
    }
  }else{
    if(tipping_parameters.only_tip_fringes){
      // connect those nodes to start that have no incoming edges / connect those nodes to target that have no outgoing edges

      if(tipping_parameters.only_tip_extreme_layer){
        // tips first and last layer
        try{
          auto [source_, target_] = g.tip_fringes(node_steps.front(), node_steps.back(), edge_attribute_generator, node_attribute_generator, tipping_parameters.shelter_orphans);
          source = source_;
          target = target_;
        }catch(std::invalid_argument& e){
          size_t longest_path = 0;
          for(Node* n1 : node_steps.front()){
            for(Node* n2 : node_steps.back()){
              Path curr_path = g.directed_admissible_st_path(n1,n2);
              if(curr_path.exists() && curr_path.number_of_edges() > longest_path){
                longest_path = curr_path.number_of_edges();
                source = n1;
                target = n2;
              }
            }
          }
        }
      }else{
        // tips all fringes
        try{
          auto [source_, target_] = g.tip_fringes(edge_attribute_generator, node_attribute_generator, tipping_parameters.shelter_orphans);
          source = source_;
          target = target_;
        }catch(std::invalid_argument& e){
          size_t longest_path = 0;
          for(Node* n1 : g.nodes()){
            for(Node* n2 : g.nodes()){
              Path curr_path = g.directed_admissible_st_path(n1,n2);
              if(curr_path.exists() && curr_path.number_of_edges() > longest_path){
                longest_path = curr_path.number_of_edges();
                source = n1;
                target = n2;
              }
            }
          }
        }
      }
      if( (source == nullptr) || (target == nullptr) ) {source = *node_steps.front().begin(); target = *node_steps.back().begin();}
    }else{
      // tips nodes regardless of if they have incoming or outgoing edges
      assert(tipping_parameters.only_tip_extreme_layer);
      // only those nodes of the first and last generated layers (tipping just everything doesn't make sense)

      Node* s = g.add_node(std::to_string(g.lifetime_node_count()), node_attribute_generator.next());
      Node* t = g.add_node(std::to_string(g.lifetime_node_count()), node_attribute_generator.next());
      for(Node* n : node_steps.front()){
        if((n == s) || (n == t)) continue;
        if(!tipping_parameters.shelter_orphans && (n->incident().size() == 0)) continue;
        std::stringstream name;
        name << s->description() << "_" << n->description();
        g.add_edge(s, n, name.str(), edge_attribute_generator.next());
      }
      for(Node* n : node_steps.back()){
        if((n == s) || (n == t)) continue;
        if(!tipping_parameters.shelter_orphans && (n->incident().size() == 0)) continue;
        std::stringstream name;
        name << n->description() << "_" << t->description();
        g.add_edge(n, t, name.str(), edge_attribute_generator.next());
      }

      source = s;
      target = t;
    }
  }


  source->add_attribute("Source", Attribute({fix, 1, Integral}) );
  target->add_attribute("Target", Attribute({fix, 1, Integral}) );

  return {std::pair{source, target}, std::move(core_network), std::move(g)};
}


std::unordered_set<Edge*> random_graph_generator::connect_random(
  SubGraph& subgraph_superunionunion,
  SubGraph& subgraph_from,
  SubGraph& subgraph_to,
  random_attribute_generator& edge_attribute_generator,
  bool at_least_strongly_connected,
  bool at_least_weakly_connected,
  bool acyclic,
  bool simple,
  bool anti_reflexive,
  size_t at_least_number_of_edges
) {
  if(at_least_strongly_connected && acyclic) throw std::logic_error("can't generate strongly connected acyclic graph");

  std::unordered_set<Edge*> added_edges;
  added_edges.reserve(at_least_number_of_edges);
  random_set_element_generator<Node*> rand_stream_from(subgraph_from.export_nodes(), random_all_static);
  random_set_element_generator<Node*> rand_stream_to(subgraph_to.export_nodes(), random_all_static);

  /**
    We opt for brute force because resticting the generation to only edges that conform to generation parameters while also keeping the distribution uniform is connected with considerable overhead.

    DO NOT ATTEMPT TO GENERATE (close to) FULL GRAPHS OF ANY SIGNIFICANT SIZE WITH THIS!!!
  */

  Node* from;
  Node* to;
  bool warned = false;
  while(
      (added_edges.size() < at_least_number_of_edges)
      || (at_least_strongly_connected && !subgraph_superunionunion.is_strongly_connected())
      || (at_least_weakly_connected && !subgraph_superunionunion.is_weakly_connected())
    ) {
    size_t attempt = 0;
    redo:
    {
      size_t problem_size_coeff = subgraph_from.size().nodes + subgraph_to.size().nodes;
      if(attempt > problem_size_coeff && !warned){
        warned = true;
        std::cerr << "bad performance when generating graph" << std::endl;
      }
      if(attempt > problem_size_coeff*problem_size_coeff*problem_size_coeff){
        std::cerr << "giving up on graph generation" << std::endl;
        goto failed;
      };
    }

    try{
      rand_stream_from >> from;
      rand_stream_to >> to;
    }catch (std::range_error& e){
      goto failed;
    }

    if(
      ( (simple && ((from == to) || (subgraph_superunionunion.directed_admissible_st_path(from, to).number_of_edges()) == 1)) )
      || (anti_reflexive && (from == to))
      || (acyclic && subgraph_superunionunion.directed_admissible_st_path(to, from).exists())
    ) {
      attempt++;
      goto redo;
    }

    std::stringstream name;
    name << from->description() << "_" << to->description() << "__" << subgraph_superunionunion.lifetime_edge_count();
    added_edges.insert(subgraph_superunionunion.add_edge(from, to, name.str(), edge_attribute_generator.next()));
  }
  return added_edges;

  failed:
  /*
    cleanup: restore state; due to failure: remove the already added components
  */
  for(Edge* e : added_edges){
    subgraph_superunionunion.remove_edge(e);
  }

  throw excessive_graph_generation_runtime();
  assert(false);
  return {};
}

//TODO: add generate_random_edge and random node to node, edge and use additional parameters to generate random attributes for edges, nodes
std::pair<std::vector<std::unordered_set<Node*>>, std::unordered_set<Edge*>> random_graph_generator::grow_random_in_steps(Graph& g, edge_step_fuzzing_plugin& edge_generator, node_step_plugin& node_generator){

  std::vector<std::unordered_set<Node*>> node_steps = node_generator.add_node_steps(g);
  std::unordered_set<Edge*> added_edges;

  try{
    added_edges = edge_generator.add_edges(g, node_steps);
  }catch(excessive_graph_generation_runtime& except){
    for(auto node_step : node_steps){
      for(Node* n : node_step){
        g.remove_node(n);
      }
    }
    throw except;
  }

  return {node_steps, added_edges};
}
