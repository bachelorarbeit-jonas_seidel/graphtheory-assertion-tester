#include "uniform_node_steps.h"

void uniform_node_steps::constr(){
  this->_get_node_step_plugin_type = [](){
    return "uniform_node_steps";
  };
  this->_init = [](node_step_plugin* uncast_here){
    uniform_node_steps* here = static_cast<uniform_node_steps*>(uncast_here);
    here->_add_node_steps = [here](Graph& g){
      std::vector<std::unordered_set<Node*>> node_steps;

      for(size_t curr_step = 0; curr_step < here->_number_of_steps; ++curr_step){
        node_steps.push_back(std::unordered_set<Node*>());

        for(size_t i = 0; i < ceil((double)here->_number_of_nodes/here->_number_of_steps); ++i){
          std::stringstream name;
          name << g.lifetime_node_count();
          node_steps[curr_step].insert(g.add_node(name.str(), here->_node_attribute_generator.next()));
        }
      }

      return node_steps;
    };
  };
}
