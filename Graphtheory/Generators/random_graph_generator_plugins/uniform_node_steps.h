#ifndef UNIFORM_NODE_STEPS
#define UNIFORM_NODE_STEPS

#include "node_steps_plugin.h"

class uniform_node_steps : public node_step_plugin {
  void constr();
public:
  uniform_node_steps(size_t number_of_nodes, size_t number_of_steps, random_attribute_generator node_attribute_generator) : node_step_plugin(number_of_nodes, number_of_steps, node_attribute_generator) {
    constr();
    init();
  }
};

#endif
