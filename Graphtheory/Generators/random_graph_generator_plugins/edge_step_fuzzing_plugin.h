#ifndef EDGE_STEP_FUZZING_PLUGIN_H
#define EDGE_STEP_FUZZING_PLUGIN_H

#include <string>
#include <unordered_set>
#include <exception>
#include <stdexcept>

#include "../../../Common/random_set_element_generator.h"
#include "../../Graph.h"
#include "../../SubGraph.h"

class edge_step_fuzzing_plugin{
  bool _initialized;
protected:
  size_t _number_of_edges;
  size_t _fuzzing_distance_from;
  size_t _fuzzing_distance_to;
  random_attribute_generator _edge_attribute_generator;
  bool _at_least_strongly_connected;
  bool _at_least_weakly_connected;
  bool _acyclic;
  bool _simple;
  bool _anti_reflexive;

  std::function<std::string()> _get_edge_step_fuzzing_plugin_type;
  std::function<std::unordered_set<Edge*>(Graph& g, const std::vector<std::unordered_set<Node*>>& node_steps)> _add_edges;
  std::function<void(edge_step_fuzzing_plugin* here)> _init;

  edge_step_fuzzing_plugin(size_t number_of_edges, size_t fuzzing_distance_from, size_t fuzzing_distance_to, random_attribute_generator edge_attribute_generator, bool at_least_strongly_connected, bool at_least_weakly_connected, bool acyclic, bool simple, bool anti_reflexive)
  : _initialized(false),
    _number_of_edges(number_of_edges),
    _fuzzing_distance_from(fuzzing_distance_from), _fuzzing_distance_to(fuzzing_distance_to),
    _edge_attribute_generator(edge_attribute_generator),
    _at_least_strongly_connected(at_least_strongly_connected), _at_least_weakly_connected(at_least_weakly_connected),
    _acyclic(acyclic), _simple(simple), _anti_reflexive(anti_reflexive),
    _get_edge_step_fuzzing_plugin_type([]() -> std::string { return "temporary_non_descript_edge_step_fuzzing_instance"; }),
    _add_edges([](Graph& g, const std::vector<std::unordered_set<Node*>>& node_steps) -> std::unordered_set<Edge*> {throw std::logic_error("all instances should be derived"); }),
    _init([](edge_step_fuzzing_plugin* here){  })
  {}

public:
  edge_step_fuzzing_plugin()
  : _initialized(false),
    _number_of_edges(0),
    _fuzzing_distance_from(0), _fuzzing_distance_to(0),
    _edge_attribute_generator(),
    _at_least_strongly_connected(false), _at_least_weakly_connected(false),
    _acyclic(false), _simple(false), _anti_reflexive(false),
    _get_edge_step_fuzzing_plugin_type([]() -> std::string { return "temporary_non_descript_edge_step_fuzzing_instance"; }),
    _add_edges([](Graph& g, const std::vector<std::unordered_set<Node*>>& node_steps) -> std::unordered_set<Edge*> {throw std::logic_error("all instances should be derived"); }),
    _init([](edge_step_fuzzing_plugin* here){  })
  {}

  std::string get_edge_step_fuzzing_plugin_type() const {return this->_get_edge_step_fuzzing_plugin_type();}
  std::unordered_set<Edge*> add_edges(Graph& g, const std::vector<std::unordered_set<Node*>>& node_steps){if(!this->_initialized) throw std::runtime_error("initialize before use"); return this->_add_edges(g, node_steps);}
  void init(){this->_init(this); this->_initialized = true;}
  void uninit(){this->_initialized = false;}

  edge_step_fuzzing_plugin(const edge_step_fuzzing_plugin& other){
    *this = other;
  }
  edge_step_fuzzing_plugin(edge_step_fuzzing_plugin&& other){
    *this = std::move(other);
  }

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  void operator=(const edge_step_fuzzing_plugin& other);
  void operator=(edge_step_fuzzing_plugin&& other);

  bool operator==(const edge_step_fuzzing_plugin& other) const ;
  bool operator!=(const edge_step_fuzzing_plugin& other) const {
    return !(*this == other);
  }

  edge_step_fuzzing_plugin(std::istream& is);

  friend std::ostream& operator<<(std::ostream& os, const edge_step_fuzzing_plugin& edge_step_gen);
  friend std::istream& operator>>(std::istream& is, edge_step_fuzzing_plugin& edge_step_gen);
};

#endif
