#ifndef TIPPING_POLICY_H
#define TIPPING_POLICY_H

#include <sstream>
#include <iostream>
#include <exception>
#include <stdexcept>

#include "../../Common/io_format.h"

class tipping_policy{
public:
  bool tip;
  bool shelter_orphans;
  bool only_tip_fringes;
  bool only_tip_extreme_layer;

  tipping_policy() : tip(false), shelter_orphans(false), only_tip_fringes(false), only_tip_extreme_layer(false) {}
  tipping_policy(bool tip) : tip(tip), shelter_orphans(false), only_tip_fringes(false), only_tip_extreme_layer(false) {if(tip) throw std::invalid_argument("you need to specify what tipping policy is to be used");}
  tipping_policy(bool shelter_orphans, bool only_tip_fringes, bool only_tip_extreme_layer) : tip(true), shelter_orphans(shelter_orphans), only_tip_fringes(only_tip_fringes), only_tip_extreme_layer(only_tip_extreme_layer) {}

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  bool operator==(const tipping_policy& other) const ;
  bool operator!=(const tipping_policy& other) const {
    return !(*this == other);
  }

  tipping_policy(std::istream& is);
  friend std::ostream& operator<<(std::ostream& os, const tipping_policy& tipping_parameters);
  friend std::istream& operator>>(std::istream& is, tipping_policy& tipping_parameters);
};

std::ostream& operator<<(std::ostream& os, const tipping_policy& tipping_parameters);
std::istream& operator>>(std::istream& is, tipping_policy& tipping_parameters);

#endif
