bool SubGraph::can_borrow(Node* node){
  if(this->obj_type() == MasterGraph) return this->contains(node);
  if(this->supergraph()->obj_type() == MasterGraph) {
    if(this != this->supergraph()->borrower()) throw std::runtime_error("detected invalid subgraph succession");
    return this->supergraph()->contains(node);
  }

  auto search = this->supergraph()->node_ledger().find(node);
  if(search == this->supergraph()->node_ledger().end()){
    if(this->supergraph()->contains(node)){
      return true;
    }else{
      return this->supergraph()->can_borrow(node);
    }
  }else{
    if(search->second != this){
      return false;
    }else{
      return true;
    }
  }

  assert(false);
}

bool SubGraph::can_borrow(Edge* edge){
  if(this->obj_type() == MasterGraph) return this->contains(edge);
  if(this->supergraph()->obj_type() == MasterGraph) {
    if(this != this->supergraph()->borrower()) throw std::runtime_error("detected invalid subgraph succession");
    return this->supergraph()->contains(edge);
  }

  auto search = this->supergraph()->edge_ledger().find(edge);
  if(search == this->supergraph()->edge_ledger().end()){
    if(this->supergraph()->contains(edge)){
      return true;
    }else{
      return this->supergraph()->can_borrow(edge);
    }
  }else{
    return (search->second == this);
  }

  assert(false);
}

// we alias contains in order to easily disallow these function for MasterGraph at a later time
bool SubGraph::can_settle(Node* node){
  return this->contains(node);
}

bool SubGraph::can_settle(Edge* edge){
  return this->contains(edge);
}

Node* SubGraph::borrow(Node* node, bool unconditionally){
  if(!unconditionally && !this->can_borrow(node)) throw std::runtime_error("node has been borrowed by another parallel subgraph or isn't part of the graph in the first place");

  if(this->obj_type() == MasterGraph) return node;

  if(!this->contains(node)) {
    Node* insert = this->supergraph()->borrow(node, true);
    this->local_nodes().insert(insert);
    if(this->supergraph()->obj_type() == SlaveSubGraph){
      this->supergraph()->node_ledger().insert({insert, this});
    }
  }

  return node;
}

Edge* SubGraph::borrow(Edge* edge, bool unconditionally){
  if(!unconditionally){
    this->borrow(edge->from());
    this->borrow(edge->to());

    if(!this->can_borrow(edge)) throw std::runtime_error("node has been borrowed by another parallel subgraph or isn't part of the graph in the first place");
  }

  if(this->obj_type() == MasterGraph) return edge;


  if(!this->contains(edge)) {
    Edge* insert = this->supergraph()->borrow(edge, true);
    this->local_edges().insert(insert);
    if(this->supergraph()->obj_type() == SlaveSubGraph){
      this->supergraph()->edge_ledger().insert({insert, this});
    }
  }

  return edge;
}

void SubGraph::settle(Node* node, bool unconditionally){
  if(!this->can_settle(node)) throw std::runtime_error("trying to settle a node that hasn't been borrowed");
  if(this->obj_type() == MasterGraph) return;

  if(!unconditionally){
    for(Edge* e : node->incident()){
      if(this->can_settle(e)) this->settle(e);
    }
  }

  // recursively close position
  auto search = this->node_ledger().find(node);
  if(search != this->node_ledger().end()) {
    search->second->settle(node, true);
  }

  // closing our local position
  this->local_nodes().erase(node);
  if(this->supergraph()->obj_type() == SlaveSubGraph) {
    this->supergraph()->node_ledger().erase(node);
  }
}

void SubGraph::settle(Edge* edge){
  /**
    note that settling is not the inverse of borrowing. If we were to just borrow some edges and immediately settle them this would leave the connecting nodes still in the graph.
    If you want to invert your actions you need to make sure to borrow the connecting nodes separately.

    TODO: perhaps return the included nodes in borrow(Edge*)?
  */
  if(!this->can_settle(edge)) throw std::runtime_error("trying to settle a edge that hasn't been borrowed");
  if(this->obj_type() == MasterGraph) return;

  // recursively close position
  auto search = this->edge_ledger().find(edge);
  if(search != this->edge_ledger().end()) search->second->settle(edge);

  // closing our local position
  this->local_edges().erase(edge);
  if(this->supergraph()->obj_type() == SlaveSubGraph) this->supergraph()->edge_ledger().erase(edge);
}

std::unordered_set<Node*> SubGraph::borrow(const std::unordered_set<Node*>& nodes){
  std::unordered_set<Node*>::const_iterator it = nodes.begin();
  try{
    for(; it != nodes.end(); ++it){
      this->borrow(*it);
    }
  }catch(std::runtime_error& except){
    for(auto loc_it = nodes.begin(); loc_it != it; ++loc_it){
      this->settle(*it);
    }
    throw except;
  }
  return nodes;
}

std::unordered_set<Edge*> SubGraph::borrow(const std::unordered_set<Edge*>& edges){
  std::unordered_set<Edge*>::const_iterator it = edges.begin();
  try{
    for(; it != edges.end(); ++it){
      this->borrow(*it);
    }
  }catch(std::runtime_error& except){
    for(auto loc_it = edges.begin(); loc_it != it; ++loc_it){
      this->settle(*it);
    }
    throw except;
  }
  return edges;
}

std::unordered_set<Node*> SubGraph::settle(const std::unordered_set<Node*> nodes){
  // TODO: custom rt_error class that allows for aggregation of error and storage of relavent node data.
  std::pair<bool, std::runtime_error*> rtime_error;

  for(Node* n : nodes){
    try{
      this->settle(n);
    }catch(std::runtime_error& except){
      rtime_error = {true, &except};
    }
  }
  if(rtime_error.first) throw *rtime_error.second;

  return nodes;
}

std::unordered_set<Edge*> SubGraph::settle(const std::unordered_set<Edge*> edges){
  std::pair<bool, std::runtime_error*> rtime_error;

  for(Edge* e : edges){
    try{
      this->settle(e);
    }catch(std::runtime_error& except){
      rtime_error = {true, &except};
    }
  }
  if(rtime_error.first) throw *rtime_error.second;

  return edges;
}
