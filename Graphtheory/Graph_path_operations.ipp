// TODO: impl as special case of shortest path
Path Graph::directed_admissible_st_path(Node* s, Node* t, const std::function<bool(const Node* from, const Edge* via)>& guide, const std::function<bool(const Node* node)>& mask){
  // TODO: impl using bfs (terminate bfs preemtively?)
  if(!mask(s) || !mask(t)) throw std::invalid_argument("can't find find path in graph between nodes that are not contained within");

  std::unordered_map<Node*, Edge*> predecessor;
  std::queue<Node*> active;
  active.push(s);

  while(!active.empty()){
    Node* n = active.front(); active.pop();

    if(predecessor.find(t) != predecessor.end()){
      break;
    }

    for(Edge* e : n->incident()){
      if( !this->locally_contains(e->to(n)) || !this->locally_contains(e) || !guide(n, e) || !mask(e->to(n)) ) continue;
      if(predecessor.find(e->to(n)) == predecessor.end() && e->to(n) != s){
        predecessor.insert({e->to(n), e});
        active.push(e->to(n));
      }
    }
  }

  Path p = Path(this, predecessor, t, s);

  return p;
}

const Path Graph::directed_admissible_st_path(const Node* s, const Node* t, const std::function<bool(const Node* from, const Edge* via)>& guide, const std::function<bool(const Node* node)>& mask) const {
  return const_cast<Graph*>(this)->directed_admissible_st_path(const_cast<Node*>(s), const_cast<Node*>(t), guide, mask);
}

Path Graph::directed_admissible_st_path(Node* s, Node* t, std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr){
  std::function<bool(const Node*, const Edge*)> f = [opt_attr, upper_limit_attr, lower_limit_attr](const Node* n, const Edge* e)->bool {

    double slack = e->attribute_throwing(upper_limit_attr).value() - e->attribute_throwing(opt_attr).value();
    if( (e->to(n) != e->to() && e->attribute_throwing(opt_attr).optimization_direction() == max) || (e->to(n) == e->to() && e->attribute_throwing(opt_attr).optimization_direction() == min) ){
      slack = e->attribute_throwing(opt_attr).value() - e->attribute_throwing(lower_limit_attr).value();
    }

    return 0 < slack;

  };
  return this->directed_admissible_st_path(s, t, f);
}
