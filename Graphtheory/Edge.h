#ifndef EDGE_H
#define EDGE_H

#include <cstddef>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <cassert>
#include <stdexcept>

#include "Attribute.h"
#include "Node.h"

class Node;

class Edge{
  std::string _description;
  Node* _from;
  Node* _to;

  std::unordered_map<std::string, Attribute> _attributes;
public:
  Edge(const Edge& local_edge) : _description(local_edge._description), _from(local_edge._from), _to(local_edge._to), _attributes(local_edge._attributes) {}
  Edge(Edge&& local_edge) : _description(std::move(local_edge._description)), _from(std::move(local_edge._from)), _to(std::move(local_edge._to)), _attributes(std::move(local_edge._attributes)) {}
  Edge(const Edge& remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup);

  Edge(Node* from, Node* to, std::string description, std::unordered_map<std::string, Attribute> attributes = {});
  Edge(std::istream& is, const std::unordered_map<std::string, Node* >& name_lookup);

  const std::string& description() const {
    return this->_description;
  }

  std::string& description() {
    return this->_description;
  }

  void update_autolabel(std::string prefix = "", std::string suffix = "");

  Node* from() const {
    return this->_from;
  }
  Node*& from() {
    return this->_from;
  }

  Node* to() const {
    return this->_to;
  }
  Node*& to() {
    return this->_to;
  }

  Node* to(const Node* node) const {
    if(node != this->to() && node != this->from()) throw std::range_error("node not incident to edge");
    return node == this->from() ? this->to() : this->from();
  }
  Node*& to(const Node* node) {
    if(node != this->to() && node != this->from()) throw std::range_error("node not incident to edge");
    return node == this->from() ? this->to() : this->from();
  }

  const std::unordered_map<std::string, Attribute>& attributes() const {
    return this->_attributes;
  }

  std::unordered_map<std::string, Attribute>& attributes() {
    return this->_attributes;
  }

  std::pair<bool, Attribute> attribute(const std::string& attr) const {
    auto search = this->_attributes.find(attr);
    if(search == this->_attributes.end()){
      return {false, {fix, 0}};
    }
    return {true, search->second};
  }

  Attribute& attribute_throwing(const std::string& attr){
    auto search = this->_attributes.find(attr);
    if(search == this->_attributes.end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined for Node " << this->description();
      throw std::range_error(text.str());
    }
    return search->second;
  }

  const Attribute& attribute_throwing(const std::string& attr) const {
    auto search = this->_attributes.find(attr);
    if(search == this->_attributes.end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined for Node " << this->description();
      throw std::range_error(text.str());
    }
    return search->second;
  }

  void disconnect();

  void reconnect(Node* from, Node* to);
  void reconnect();

  void operator=(const Edge& edge) {
    this->_description = edge._description;
    this->_from = edge._from;
    this->_to = edge._to;
  }

  void operator=(Edge&& edge){
    this->_description = std::move(edge._description);
    this->_from = std::move(edge._from);
    this->_to = std::move(edge._to);
  }

  ~Edge(){
    this->disconnect();
  }

  // _debug_helpers
  void dump();
};

std::ostream& operator<<(std::ostream& os, const Edge& e);

#endif
