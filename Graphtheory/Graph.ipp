#include "SubGraph.h"

#include <cmath>

Edge* Graph::add_edge(Node* from, Node* to, const std::string& description, const std::unordered_map<std::string, Attribute>& edge_attributes){
  if( !this->locally_contains(from) || !this->locally_contains(to) ) throw std::invalid_argument("attempting to add edge which connects nodes that aren't contained in graph/subgraph");
  std::unordered_map<std::string, Attribute> unified_attr = this->template_edge_attributes();
  for(std::pair<std::string, Attribute> overriding_attr : edge_attributes){
    auto new_or_not = unified_attr.insert(overriding_attr);
    if(new_or_not.second == false){
      new_or_not.first->second = overriding_attr.second; // test whether this actually works as intended
    }
  }

  Edge* ptr = new Edge(from, to, description, unified_attr);

  this->edges().insert(ptr);

  ++this->lifetime_edge_counter();
  return ptr;
}

Edge* Graph::add_edge(const Edge* remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup){
  auto from_search = node_lookup.find(remote_edge->from());
  auto to_search = node_lookup.find(remote_edge->to());
  if( from_search == node_lookup.end() || to_search == node_lookup.end() ) throw std::invalid_argument("lookup does not contain edge to be added");
  if( !this->locally_contains(from_search->second) || !this->locally_contains(to_search->second) ) throw std::invalid_argument("attempting to add edge which connects nodes that aren't contained in graph/subgraph");

  Edge* ptr = new Edge(*remote_edge, node_lookup);
  this->edges().insert(ptr);

  ++this->lifetime_edge_counter();
  return ptr;
}

std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> Graph::add_edges(const std::unordered_set<Edge*>& remote_edges, const std::unordered_map<const Node*, Node*>& node_lookup){
  return this->add_edges(std::unordered_set<const Edge*>(remote_edges.begin(), remote_edges.end()), node_lookup);
}

std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>>  Graph::add_edges(const std::unordered_set<const Edge*>& remote_edges, const std::unordered_map<const Node*, Node*>& node_lookup){
  std::unordered_set<Edge*> added_edges;
  std::unordered_map<const Edge*, Edge*> edge_lookup;

  for(const Edge* edge : remote_edges){
    Edge* new_edge = this->add_edge(edge, node_lookup);
    added_edges.insert(new_edge);
    edge_lookup.insert({edge, new_edge});
  }

  return {added_edges, edge_lookup};
}


Node* Graph::add_node(std::string description, const std::unordered_map<std::string, Attribute>& node_attributes){
  std::unordered_map<std::string, Attribute> unified_attr = this->template_node_attributes();
  for(std::pair<std::string, Attribute> overriding_attr : node_attributes){
    auto new_or_not = unified_attr.insert(overriding_attr);
    if(new_or_not.second == false){
      new_or_not.first->second = overriding_attr.second;
    }
  }

  Node* ptr = new Node(description, unified_attr);
  this->nodes().insert(ptr);

  ++this->lifetime_node_counter();
  return ptr;
}

Node* Graph::add_node(const Node* remote_node){
  Node* ptr = new Node(*remote_node);
  this->nodes().insert(ptr);

  ++this->lifetime_node_counter();
  return ptr;
}

std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*>> Graph::add_nodes(const std::unordered_set<Node*>& remote_nodes){
  return this->add_nodes( std::unordered_set<const Node*>(remote_nodes.begin(), remote_nodes.end()) );
}

std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*>> Graph::add_nodes(const std::unordered_set<const Node*>& remote_nodes){
  std::unordered_set<Node*> new_nodes;
  std::unordered_map<const Node*, Node*> node_lookup;

  for(const Node* node : remote_nodes){
      Node* new_node = this->add_node(node);
      new_nodes.insert(new_node);
      node_lookup.insert({node, new_node});
  }

  return {new_nodes, node_lookup};
}

void Graph::remove_node(Node* node){
  if(this->is_borrowed()) this->borrower()->settle(node);
  while(!node->incident().empty()){
    this->remove_edge(*node->incident().begin());
  }
  this->nodes().erase(node); // remove node pointer in graph
  delete node; // removes pointers in node and edges & dealloc
}

void Graph::remove_edge(Edge* edge){
  if(this->is_borrowed()) this->borrower()->settle(edge);

  this->edges().erase(edge);
  delete edge;
}

void Graph::remove_nodes(const std::unordered_set<Node*> nodes){
  for(Node* n : nodes){
    this->remove_node(n);
  }
}
void Graph::remove_edges(const std::unordered_set<Edge*> edges){
  for(Edge* e : edges){
    this->remove_edge(e);
  }
}

void Graph::reset_attribute_values(const std::unordered_set<std::string>& edge_attr, const std::unordered_set<std::string>& node_attr){
  for(Node* n : this->nodes()){
    for(const std::string attr : node_attr){
      n->attribute_throwing(attr).value() = this->node_template_throwing(attr).value();
    }
  }

  for(Edge* e : this->edges()){
    for(const std::string attr : edge_attr){
      e->attribute_throwing(attr).value() = this->edge_template_throwing(attr).value();
    }
  }
}

bool Graph::has_compatible_default_attributes(const Graph& other) const {
  return (this->template_node_attributes() == other.template_node_attributes() && this->template_edge_attributes() == other.template_edge_attributes());
}

std::tuple<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*>, std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> Graph::join_with(const Graph& graph){
  if(this->obj_type() != GraphtheoryObjType::MasterGraph) throw std::logic_error("calling join_with on non-master obj");
  if(!this->has_compatible_default_attributes(graph)) throw std::invalid_argument("trying to join with graph that has differing default attributes");

  auto [added_nodes, node_lookup] = this->add_nodes(graph.export_nodes());
  auto [added_edges, edge_lookup] = this->add_edges(graph.export_edges(), node_lookup);

  return {std::move(added_nodes), std::move(node_lookup), std::move(added_edges), std::move(edge_lookup)};
}

void Graph::join_with(Graph&& graph){
  if(this->obj_type() != GraphtheoryObjType::MasterGraph) throw std::logic_error("calling join_with on non-master obj");
  if(graph.is_borrowed()) throw std::logic_error("cannot move out of graph with active subgraphs");
  if(!this->has_compatible_default_attributes(graph)) throw std::invalid_argument("trying to join with graph that has differing default attributes");

  typedef std::unordered_set<Node*>::iterator Iter_Node;
  typedef std::unordered_set<Edge*>::iterator Iter_Edge;
  this->nodes().insert(std::move_iterator<Iter_Node>(graph.nodes().begin()), std::move_iterator<Iter_Node>(graph.nodes().end()));
  graph.nodes() = {};
  this->lifetime_node_counter() = std::max(graph.lifetime_node_count(), this->lifetime_node_count());
  this->edges().insert(std::move_iterator<Iter_Edge>(graph.edges().begin()), std::move_iterator<Iter_Edge>(graph.edges().end()));
  graph.edges() = {};
  this->lifetime_edge_counter() = std::max(graph.lifetime_edge_count(), this->lifetime_edge_count());
}
