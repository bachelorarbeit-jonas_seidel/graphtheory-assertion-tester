#include "Edge.h"
#include "Node.h"

#include "../Common/io_format.h"

Edge::Edge(const Edge& remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup) : _description(remote_edge._description), _from(node_lookup.find(remote_edge._from)->second), _to(node_lookup.find(remote_edge._to)->second), _attributes(remote_edge._attributes) {
  this->_from->add_incident(this);
  this->_to->add_incident(this);
}

Edge::Edge(Node* from, Node* to, std::string description, std::unordered_map<std::string, Attribute> attributes) : _description(std::move(description)), _from(from), _to(to), _attributes(std::move(attributes)){
  from->add_incident(this);
  to->add_incident(this);
};

Edge::Edge(std::istream& is, const std::unordered_map<std::string, Node* >& name_lookup){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "Edge");

  is >> this->_description;

  is >> curr;
  IO_THROW(curr, "(");
  is >> curr;
  while(curr != ")"){
    std::string attr_name = curr;
    is >> curr;
    IO_THROW(curr, "=");
    Attribute attr(is);
    this->_attributes.insert({attr_name, attr});
    is >> curr;
  }
  is >> curr;
  IO_THROW(curr, "{");
  is >> curr;
  this->_from = name_lookup.find(curr)->second;
  this->_from->add_incident(this);
  is >> curr;
  this->_to = name_lookup.find(curr)->second;
  this->_to->add_incident(this);
  is >> curr;
  IO_THROW(curr, "}");
}

void Edge::update_autolabel(std::string prefix, std::string suffix){
  if((this->from() == nullptr) || (this->to() == nullptr)) throw std::runtime_error("cannot set autolabel if edge has dangleing connections");
  std::stringstream name;
  name << prefix << this->from()->description() << "_" << this->to()->description() << suffix;
  this->description() = name.str();
}

void Edge::disconnect(){
  this->from()->incident().erase(this);
  this->to()->incident().erase(this);
}

void Edge::reconnect(Node* from, Node* to){
  this->disconnect();
  this->from() = from;
  this->to() = to;
  this->from()->incident().insert(this);
  this->to()->incident().insert(this);
}

void Edge::reconnect(){
  this->disconnect();
  this->from()->incident().insert(this);
  this->to()->incident().insert(this);
}

std::ostream& operator<<(std::ostream& os, const Edge& e){
  if(&os == &std::cout){
    os << "\033[0;32m";
    os << "Edge " << e.description() << " ( ";
    for(std::pair<std::string, Attribute> pair : e.attributes()){
      os << pair.first << " = " << pair.second.value() << ", ";
    }
    os << ") {" << e.from()->description() << ", " << e.to()->description() << "}";
    os << "\033[0m";
  }else{
    os << "Edge " << e.description() << " ( ";
    for(std::pair<std::string, Attribute> pair : e.attributes()){
      os << pair.first << " = " << pair.second << " ";
    }
    os << ") { " << e.from()->description() << " " << e.to()->description() << " }";
  }


  return os;
}

void Edge::dump(){
  std::cout << "\033[0;32m";
  std::cout << "Edge " << this->description() << "; " << this << " ( ";
  for(std::pair<std::string, Attribute> pair : this->attributes()){
    std::cout << pair.first << " = ";
    pair.second.dump();
    std::cout << ", ";
  }
  std::cout << ") {" << this->from()->description() << ", " << this->to()->description() << "}";
  std::cout << "\033[0m";
}
