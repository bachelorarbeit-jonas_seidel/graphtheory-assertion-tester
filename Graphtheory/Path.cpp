#include "Path.h"
#include <cfloat>

Path::Path(Graph* graph, std::unordered_map<Node*, Edge*>& successor_matrix, Node* start, bool invert_order) : Path(graph, successor_matrix, start, nullptr, invert_order) {}

Path::Path(Graph* graph, std::unordered_map<Node*, Edge*>& successor_matrix, Node* start, Node* end, bool invert_order) : _owner(graph), _exists(true), _is_loop(false) {
  std::unordered_map<Node*, Edge*>* given_dir = &this->_next_edge;
  std::unordered_map<Node*, Edge*>* opposing_dir = &this->_prev_edge;

  if(invert_order){
    given_dir = &this->_prev_edge;
    opposing_dir = &this->_next_edge;
  }

  Node* curr = start;
  auto curr_iter = successor_matrix.find(curr);
  while(curr_iter != successor_matrix.end()){
    given_dir->insert({curr, curr_iter->second});
    opposing_dir->insert({curr_iter->second->to(curr), curr_iter->second});

    curr = curr_iter->second->to(curr);
    curr_iter = successor_matrix.find(curr);

    if(end == nullptr){
      if(curr == start){
        break;
      }
    }else{
      if(curr == end) {
        break;
      }
    }
  }

  if(end != nullptr) this->_exists = (curr == end);
  this->_is_loop = (start == curr);

  if(invert_order){
    this->_first = curr;
    this->_last = start;
  }else{
    this->_first = start;
    this->_last = curr;
  }
}

double Path::worst_along_path(std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr){
  opt_dir dir = this->graph()->edge_template_throwing(opt_attr).optimization_direction();

  auto slack = [this, opt_attr, lower_limit_attr, upper_limit_attr](Node* n) -> double {

    Edge* e = this->_next_edge.find(n)->second;
    double slack = e->attribute_throwing(upper_limit_attr).value() - e->attribute_throwing(opt_attr).value();
    if( (e->to(n) != e->to() && e->attribute_throwing(opt_attr).optimization_direction() == max) || (e->to(n) == e->to() && e->attribute_throwing(opt_attr).optimization_direction() == min) ){
      slack = e->attribute_throwing(opt_attr).value() - e->attribute_throwing(lower_limit_attr).value();
    }

    return slack;

  };

  if(this->number_of_edges() == 0) return 0;


  double curr = DBL_MAX;
  for(Node* n = this->_first; this->_next_edge.find(n) != this->_next_edge.end(); n = this->_next_edge.find(n)->second->to(n)){
    curr = std::min(curr, slack(n));
  }

  return dir*curr;
}

double Path::augment_edges_along_path(std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr){
  double best = this->worst_along_path(opt_attr, lower_limit_attr, upper_limit_attr);

  for(Node* n = this->_first; this->_next_edge.find(n) != this->_next_edge.end(); n = this->_next_edge.find(n)->second->to(n)){
    this->_next_edge.find(n)->second->attribute_throwing(opt_attr).value() += best;
  }

  return best;
}

std::ostream& operator<<(std::ostream& os, const Path& p){
  os << "\033[0;31m";
  std::cout << "Path: {" << "\n";
  for(Node* n = p._first; p._next_edge.find(n) != p._next_edge.end(); n = p._next_edge.find(n)->second->to(n)){
    std::cout << *n << "\n";
  }
  std::cout << *p.last() << "\n";
  os << "\033[0;31m";
  std::cout << "}";
  os << "\033[0m";
  return os;
}
