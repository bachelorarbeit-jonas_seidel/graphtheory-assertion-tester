#include "Attribute.h"
#include "../Common/io_format.h"
#include <climits>

Attribute::Attribute(const Attribute& attr) : _dir(attr._dir), _integrality(attr._integrality), _value(attr._value) {}

Attribute::Attribute(Attribute&& attr) : _dir(std::move(attr._dir)), _integrality(std::move(attr._integrality)), _value(std::move(attr._value)) {}

Attribute::Attribute(opt_dir optimization_direction, double attribute, integrality integrality)
  : _dir(optimization_direction), _integrality(integrality), _value(attribute) {}

Attribute::Attribute(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "Attribute");
  is >> curr;
  IO_THROW(curr,"{");
  is >> this->_dir
      >> this->_integrality
      >> this->_value;
  is >> curr;
  IO_THROW(curr, "}");
}

void Attribute::operator=(const Attribute& attr){
  this->_dir = attr._dir;
  this->_integrality = attr._integrality;
  this->_value = attr._value;
}

void Attribute::operator=(Attribute&& attr){
  this->_dir = std::move(attr._dir);
  this->_integrality = std::move(attr._integrality);
  this->_value = std::move(attr._value);
}

bool Attribute::operator!=(const Attribute& other) const {
  if(this->_dir != other._dir) return true;
  if(this->_integrality != other._integrality) return true;
  if(this->_value != other._value) return true;
  return false;
}

bool Attribute::operator==(const Attribute& other) const {
  return !(*this != other);
}

const opt_dir& Attribute::optimization_direction() const {
  return this->_dir;
}

opt_dir& Attribute::optimization_direction(){
  return this->_dir;
}

const integrality& Attribute::is_integral() const {
  return this->_integrality;
}

integrality& Attribute::is_integral(){
  return this->_integrality;
}

const double& Attribute::value() const {
  return this->_value;
}

double& Attribute::value(){
  return this->_value;
}

std::string Attribute::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "attributes";
  return csv_columns_stream.str();
}
std::string Attribute::csv_data(const std::unordered_map<std::string, Attribute>& attributes, std::string separator){
  std::stringstream csv_data;
  csv_data << attributes;
  return csv_data.str();
}

std::ostream& operator<<(std::ostream& os, const Attribute& attr){
  auto prev_precision = os.precision();
  os.precision(std::numeric_limits<double>::digits10 + 1);

  os << "Attribute { "
      << attr._dir << " "
      << attr._integrality << " "
      << attr._value << " "
      << "}";

  os.precision(prev_precision);
  return os;
}

std::ostream& operator<<(std::ostream& os, const std::unordered_map<std::string, Attribute>& attributes){
  os << "Attributes" << " {";
  for(auto& [name, attr] : attributes){
    os << " " << name << " = " << attr;
  }
  os << " } ";
  return os;
}

std::istream& operator>>(std::istream& is, Attribute& attr){ attr = Attribute(is); return is;}

std::istream& operator>>(std::istream& is, std::unordered_map<std::string, Attribute>& attributes){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "Attributes");
  is >> curr;
  IO_THROW(curr, "{");
  while( (is >> curr) && (curr != "}") ){
    std::string name = curr;
    is >> curr;
    IO_THROW(curr, "=");
    Attribute new_attr(is);
    attributes.insert({name, new_attr});
  }
  if(!is) throw std::invalid_argument("stream did not contain complete attribute map");
  return is;
}


void Attribute::dump(){
  std::cout << *this;
}
