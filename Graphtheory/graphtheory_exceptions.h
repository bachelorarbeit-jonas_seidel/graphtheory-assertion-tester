#ifndef GRAPHTHEORY_EXCEPTIONS_H
#define GRAPHTHEORY_EXCEPTIONS_H

#include <exception>
#include <string>
#include <sstream>

#include "Node.h"

class nodes_unconnected_exception : public std::exception {
    Node* _a;
    Node* _b;

public:
    nodes_unconnected_exception(Node* a, Node* b) : _a(a), _b(b) {}

    ~nodes_unconnected_exception(){}

    const char* what() {
      std::stringstream error_message;
      error_message << "cannot find path between unconnected nodes: \n" << *this->_a << "\n" << *this->_b;
      return error_message.str().c_str();
    }
};

class excessive_graph_generation_runtime : public std::exception {
public:
  // TODO: store partial generation?
  excessive_graph_generation_runtime() {}

  ~excessive_graph_generation_runtime(){}

  const char* what() {
    std::stringstream error_message;
    error_message << "given up on graph generation due to excessive runtime. You might want to decrease number_of_edges_per_step or increase fuzzing for improved chance of success.";
    return error_message.str().c_str();
  }

};
#endif
