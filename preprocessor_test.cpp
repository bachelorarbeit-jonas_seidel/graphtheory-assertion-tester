#include <iostream>

#define m(container, x) \
if(x){ \
  std::cout << container.x << #x << std::endl; \
}

struct Data{
  int member;
};

int main(){
  Data data;
  bool member = true;
  m(data, member)
  double test;
  std::cin >> test;
  std::cout << test;
}
