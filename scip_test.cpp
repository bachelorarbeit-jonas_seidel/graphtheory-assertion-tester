#include <scip/scip.h>
#include <scip/scipdefplugins.h>

#include <iostream>
#include <vector>

struct Test{
  std::pair<bool,double> test;
};

std::vector<Test> test_f(std::vector<Test>& test_p){
  std::vector<Test> test = test_p;
  test.push_back(Test{{true, 2}});

  return test;
}


int main(){
  SCIP* scip;
  SCIPcreate(&scip);
  SCIPcreateProbBasic(scip, "test");
  SCIP_VAR* test_var;
  SCIPcreateVarBasic(scip, &test_var, "test_var", 0, 1, 0, SCIP_VARTYPE_BINARY);
  SCIPaddVar(scip, test_var);

  std::cout << test_var << std::endl;
  std::cout << SCIPgetVars(scip)[0] << std::endl;

  SCIPreleaseVar(scip, &test_var);
  SCIPfree(&scip);


  std::vector<Test> test1;
  std::vector<Test> test2 = test_f(test1);

  return 0;
}
