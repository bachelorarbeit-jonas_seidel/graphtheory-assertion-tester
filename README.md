Building from Source
===
Prerequisites
---
- g++-10
- libboost 1.71.0 or later
- libscipopt installed to /usr/lib
- libortools installed to /usr/lib

Making (relevant targets)
---
**make maintenance_problem_test**: contains logic to easily switch generated base graph type and automatically starts execution of the corresponding models. Also attempts to start visualization if available.

**make tests**: created the preset tests and necessary tools for further processing
- the resulting test binaries can be used as follows: *./$testname $datafolder $number_of_instances_per_coord"*.
- *gather_data_to_csv* does what its name suggest, though it outputs averages for each coordinate. Use is as follows: *./gather_data_to_csv $datafolder $axis_label_x $axis_label_y $list_of_formulations* with option of *-o $output_path* (at the end).
