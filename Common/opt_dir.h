#ifndef OPT_DIR_H
#define OPT_DIR_H

#include <iostream>

enum opt_dir : int {max = 1, fix = 0, min = -1};

std::ostream& operator<<(std::ostream& os, const opt_dir& direction);
std::istream& operator>>(std::istream& is, opt_dir& direction);

#endif
