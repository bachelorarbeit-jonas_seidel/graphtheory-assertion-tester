#ifndef INTEGRALITY_H
#define INTEGRALITY_H

#include <iostream>

enum integrality : bool {Continuous = false, Integral = true};

std::ostream& operator<<(std::ostream& os, const integrality& integr);
std::istream& operator>>(std::istream& is, integrality& integr);

#endif
