#include <scip/scip.h>
#include <scip/scipshell.h>
#include <scip/scipdefplugins.h>
#include <scip/cons_benders.h>
#include <scip/cons_benderslp.h>
#include <scip/benders_default.h>
#include <scip/struct_var.h>
#include <scip/struct_cons.h>
#include <scip/struct_scip.h>



#include <iostream>
#include <sstream>

int main(){
  int nedges = 5;
  int nepochs =  3;

  double capacities[] = {31,4,10,26,22,3,5,76,8,8,31,4,10,26,22,3,5,76,8,8,31,4,10,26,22,3,5,76,8,8,31,4,10,26,22,3,5,76,8,8};

  std::cout << "creating master" << std::endl;
  SCIP* scip;

  SCIP_CALL_ABORT( SCIPcreate(&scip));
  SCIP_CALL_ABORT( SCIPincludeDefaultPlugins(scip));
  SCIP_CALL_ABORT( SCIPcreateProbBasic(scip, "mp"));
  SCIP_CALL_ABORT( SCIPsetObjsense(scip, SCIP_OBJSENSE_MAXIMIZE));

  SCIP_CALL_ABORT( SCIPsetIntParam(scip, "presolving/maxrestarts",0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(scip, "heuristics/trysol/freq", 1) );

  SCIP_VAR*** mp_decisions = new SCIP_VAR**[nedges];
  for(int i = 0; i < nedges; i++){
    mp_decisions[i] = new SCIP_VAR*[nepochs];
    for(int j = 0; j < nepochs; j++){
      std::stringstream name;
      name << "decision_edge_" << i << "_epoch_" << j;
      char* namec = new char[name.str().size()+1];
      SCIP_CALL_ABORT( SCIPcreateVarBasic(scip, &mp_decisions[i][j], strcpy(namec, name.str().c_str()), 0, 1, 0, SCIP_VARTYPE_BINARY));
      SCIP_CALL_ABORT( SCIPaddVar(scip, mp_decisions[i][j]));
    }
  }
  SCIP_VAR* target;
  SCIP_CALL_ABORT( SCIPcreateVarBasic(scip, &target, "target", 0, 50000, 1, SCIP_VARTYPE_CONTINUOUS));
  SCIP_CALL_ABORT( SCIPaddVar(scip, target));


  // const
  SCIP_CONS** epoch_dec = new SCIP_CONS*[nedges];
  for(int i = 0; i < nedges; i++){
    std::stringstream name;
    name << "decision_dec" << i;
    char* namec = new char[name.str().size()+1];
    SCIP_Real* ones = new SCIP_Real[nepochs];
    for(int index = 0; index < nepochs; index++){
      ones[index] = 1;
    }
    SCIP_CALL_ABORT( SCIPcreateConsBasicLinear(scip, &epoch_dec[i], strcpy(namec, name.str().c_str()), nepochs, mp_decisions[i], ones, 1, 1));
    SCIP_CALL_ABORT( SCIPaddCons(scip, epoch_dec[i]));
  }

  std::cout << "creating subproblems" << std::endl;
  SCIP** subproblems;
  SCIP_CALL_ABORT( SCIPallocBufferArray(scip, &subproblems, nepochs));
  SCIP_VAR*** flow_vars = new SCIP_VAR**[nepochs];
  for(int epoch = 0; epoch < nepochs; epoch++){
    flow_vars[epoch] = new SCIP_VAR*[nedges];
    SCIP_CALL_ABORT( SCIPcreate(&subproblems[epoch]));
    SCIP_CALL_ABORT( SCIPincludeDefaultPlugins(subproblems[epoch]));

    std::stringstream name;
    name << "sub" << epoch;
    char* namec = new char[name.str().size()+1];
    SCIP_CALL_ABORT( SCIPcreateProbBasic(subproblems[epoch], strcpy(namec, name.str().c_str())));
    SCIP_CALL_ABORT( SCIPsetObjsense(subproblems[epoch], SCIP_OBJSENSE_MAXIMIZE));

    //flow vars
    for(int edge = 0; edge < nedges; edge++){
      std::stringstream name;
      name << "flow_edge_" << edge;
      char* namec = new char[name.str().size()+1];

      SCIP_CALL_ABORT( SCIPcreateVarBasic(subproblems[epoch], &flow_vars[epoch][edge], strcpy(namec, name.str().c_str()), 0, SCIPinfinity(subproblems[epoch]), 0, SCIP_VARTYPE_CONTINUOUS));
      SCIP_CALL_ABORT( SCIPaddVar(subproblems[epoch], flow_vars[epoch][edge]));
    }
  }

  SCIP_VAR*** sub_decisions = new SCIP_VAR**[nedges];
  // master vars:
  for(int i = 0; i < nedges; i++){
    sub_decisions[i] = new SCIP_VAR*[nepochs];
    for(int j = 0; j < nepochs; j++){
      std::stringstream name;
      name << "decision_edge_" << i << "_epoch_" << j;
      char* namec = new char[name.str().size()+1];

      SCIP_CALL_ABORT( SCIPcreateVarBasic(subproblems[j], &sub_decisions[i][j], strcpy(namec, name.str().c_str()), 0, 1, 0, SCIP_VARTYPE_BINARY));
      SCIP_CALL_ABORT( SCIPaddVar(subproblems[j], sub_decisions[i][j]));
    }
  }
  SCIP_VAR** target_subp = new SCIP_VAR*[nepochs];
  for(int epoch = 0; epoch < nepochs; epoch++){
    SCIP_CALL_ABORT( SCIPcreateVarBasic(subproblems[epoch], &target_subp[epoch], "target", 0, 50000, 1, SCIP_VARTYPE_CONTINUOUS));
    SCIP_CALL_ABORT( SCIPaddVar(subproblems[epoch], target_subp[epoch]));
  }


  //const
  for(int epoch = 0; epoch < nepochs; epoch++){
    for(int edge = 0; edge < nedges; edge++){
      // edge capacity constraints
      std::stringstream name;
      name << "capacity_edge_" << edge;
      char* namec = new char[name.str().size()+1];

      SCIP_CONS* cons;
      SCIP_CALL_ABORT( SCIPcreateConsBasicLinear(subproblems[epoch], &cons, strcpy(namec, name.str().c_str()), 0, NULL, NULL, -SCIPinfinity(subproblems[epoch]), capacities[edge]));
      SCIP_CALL_ABORT( SCIPaddCoefLinear(subproblems[epoch], cons, flow_vars[epoch][edge], 1));
      SCIP_CALL_ABORT( SCIPaddCoefLinear(subproblems[epoch], cons, sub_decisions[edge][epoch], capacities[edge]));

      SCIP_CALL_ABORT( SCIPaddCons(subproblems[epoch], cons));
    }
    std::stringstream name;
    name << "flow_epoch_" << epoch;
    char* namec = new char[name.str().size()+1];

    SCIP_CONS* flow_cons;
    SCIP_CALL_ABORT( SCIPcreateConsBasicLinear(subproblems[epoch], &flow_cons, strcpy(namec, name.str().c_str()), 0, NULL, NULL, -SCIPinfinity(subproblems[epoch]), 0));
    SCIP_CALL_ABORT( SCIPaddCoefLinear(subproblems[epoch], flow_cons, target_subp[epoch], 1));
    for(int edge = 0; edge < nedges; edge++){
      SCIP_CALL_ABORT( SCIPaddCoefLinear(subproblems[epoch], flow_cons, flow_vars[epoch][edge], -1));
    }

    SCIP_CALL_ABORT( SCIPaddCons(subproblems[epoch], flow_cons));
  }

  std::cout << "creating benders" << std::endl;
  SCIP_CALL_ABORT( SCIPcreateBendersDefault(scip, subproblems, nepochs));
  SCIP_CALL_ABORT( SCIPsetBoolParam(scip, "constraints/benders/active", TRUE) );
  SCIP_CALL_ABORT( SCIPsetBoolParam(scip, "constraints/benderslp/active", TRUE) );
  SCIP_CALL_ABORT( SCIPsetIntParam(scip, "constraints/benders/maxprerounds", 0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(scip, "presolving/maxrounds", 0) );

  SCIP_CALL_ABORT( SCIPsolve(scip));

  SCIP_CALL_ABORT( SCIPprintTransProblem(scip, NULL, NULL, FALSE));
  for(int i = 0; i < nepochs; i++){
    SCIP_CALL_ABORT( SCIPprintTransProblem(subproblems[i], NULL, NULL, FALSE));
    SCIPprintBestSol(subproblems[i], NULL, TRUE);
  }

  SCIPprintBestSol(scip, NULL, TRUE);
  SCIPprintBestTransSol(scip, NULL, TRUE);

  SCIPprintBendersStatistics(scip, NULL);
}
