#include "Monomial.h"

Monomial::Monomial(const Monomial& other) : _vars(other._vars) {}
Monomial::Monomial(Monomial&& other) : _vars(std::move(other._vars)) {}

Monomial::Monomial(std::set<Variable*> vars) : _vars(vars) {}


Monomial::Monomial(const Monomial& other, const std::unordered_map<const Variable*, Variable*>& variable_lookup){
  for(Variable* var : other._vars){
    auto variable_search = variable_lookup.find(var);
    if(variable_search == variable_lookup.end()) throw std::invalid_argument("variable_lookup didn't include all relevant variables");
    this->_vars.insert(variable_search->second);
  }
}

double Monomial::value(){
  double curr = 1;
  for(Variable* var : this->_vars){
    curr *= var->value();
  }
  return curr;
}
bool Monomial::less::operator()(const Monomial a, const Monomial b) const {
  std::stringstream key_a;
  for(auto var_ptr : a._vars){
    key_a << var_ptr;
  }
  std::stringstream key_b;
  for(auto var_ptr : b._vars){
    key_b << var_ptr;
  }
  return key_a.str() < key_b.str();
}

Monomial operator*(const Monomial a, const Monomial b) {
  Monomial tmp (a);
  tmp._vars.insert(b._vars.begin(), b._vars.end());
  return tmp;
}

std::ostream& operator<<(std::ostream& os, const Monomial m){
  for(Variable* v : m._vars){
    os << v->description() << " ";
  }

  return os;
}
