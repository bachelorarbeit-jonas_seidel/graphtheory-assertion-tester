#ifndef LINEAR_PROGRAM_H
#define LINEAR_PROGRAM_H

#include <cstddef>
#include <unordered_map>
#include <vector>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <cstdlib>

#include <boost/bimap.hpp>
#include <scip/scip.h>
#include <scip/scipdefplugins.h>

#include "Polyeder.h"
#include "../Common/integrality.h"

class Linear_Program{
  bool _maximum;
  std::string _description;
  Polyeder _polyeder;
  std::unordered_map<Variable*, Coefficient> _direction;
public:
  Linear_Program(const Linear_Program& lp);
  Linear_Program(Linear_Program&& lp);
  Linear_Program(std::string description, bool maximum);

  std::string description() const ;

  bool is_maximum() const ;
  std::unordered_map<Variable*, Coefficient>& direction();
  void add_direction_coefficient(std::pair<Variable*, Coefficient> summand);
  Coefficient direction_coefficient(Variable* index);
  Polyeder& polyeder();
  const Polyeder& polyeder() const ;

  std::pair<SCIP*, std::unordered_map<Variable*, SCIP_VAR*> > computational_model();

  void operator=(const Linear_Program& lp);
  void operator=(Linear_Program&& lp);

  Linear_Program relaxation_dual();
  Linear_Program Danzig_Wolfe_IPM_complete_optimal_inequality_relaxation();
  Linear_Program Benders_Reformulierung_integral_relaxation();

  friend std::ostream& operator<<(std::ostream& os, const Linear_Program& linear_program);
};

std::ostream& operator<<(std::ostream& os, const Linear_Program& linear_program);
#endif
