#include "lp_generator.h"

std::unordered_map<Variable*, Coefficient> lp_generator::lhs_from_data(
  lhs_constraint_data& data,

  const std::map<
    std::pair<const Edge*, std::string>,
    std::pair<Variable*, size_t>
  >& edge_variable_lookup,

  const std::map<
    std::pair<const Node*, std::string>,
    std::pair<Variable*, size_t>
  >& node_variable_lookup
){
  std::unordered_map<Variable*, Coefficient> lhs;

  for(std::pair<std::pair<const Node*, std::string>, double> w : data.first){
    auto search_result = node_variable_lookup.find(w.first);
    if(search_result == node_variable_lookup.end()) throw std::range_error("lp_generator::lhs_from_data use of undeclared variable in constraint generation! (node)");
    lhs.insert({search_result->second.first, {w.second}});
  }
  for(std::pair<std::pair<const Edge*, std::string>, double> w : data.second){
    auto search_result = edge_variable_lookup.find(w.first);
    if(search_result == edge_variable_lookup.end()) throw std::range_error("lp_generator::lhs_from_data use of undeclared variable in constraint generation! (edge)");
    lhs.insert({search_result->second.first, {w.second}});
  }

  return lhs;
}


std::pair<
  std::map<
    std::pair<const Node*, std::string>,
    std::pair<Variable*, size_t>
  >,
  std::map<
    std::pair<const Edge*, std::string>,
    std::pair<Variable*, size_t>
  >
>
  lp_generator::grow_from_graph
(
  Linear_Program& lp,

  const Graph& g,

  edge_constraint_data_generator edge_generator,

  node_constraint_data_generator node_generator,

  const std::vector<
    std::pair<
      std::string,
      std::tuple<
        integrality,
        std::pair<bool, double>,
        std::pair<bool, double>
      >
    >
  > edge_var_data,

  const std::vector<
    std::pair<
      std::string,
      std::tuple<
        integrality,
        std::pair<bool, double>,
        std::pair<bool, double>
      >
    >
  > node_var_data,

  std::string name_appendix
){
  Polyeder& p = lp.polyeder();
  std::map<std::pair<const Node*, std::string>, std::pair<Variable*, size_t>> node_lookup;
  std::map<std::pair<const Edge*, std::string>, std::pair<Variable*, size_t>> edge_lookup;

  // put inside conditional_bfs_all_components call as parameters;
  std::function<void(const Node* from, const Edge* via, bool used_in_traversal)> edge_function = [&p, &edge_lookup, &edge_var_data, name_appendix](const Node* from, const Edge* via, bool used_in_traversal) -> void {
    for(std::pair<std::string, std::tuple<integrality, std::pair<bool, double>, std::pair<bool, double>>> edge_prop : edge_var_data){
      std::stringstream name;
      name << "edge_" << via->description() << "_" << edge_prop.first << "_" << name_appendix;
      edge_lookup.insert({{via, edge_prop.first}, p.add_variable(Variable(name.str(), std::get<0>(edge_prop.second), std::get<1>(edge_prop.second), std::get<2>(edge_prop.second)))});
    }
  };
  std::function<bool(const Edge*, const Node*)> node_function = [&p, &node_lookup, &node_var_data, name_appendix](const Edge* via, const Node* n) -> bool {
    for(std::pair<std::string, std::tuple<integrality, std::pair<bool, double>, std::pair<bool, double>>> node_prop : node_var_data){
      std::stringstream name;
      name << "node_" << n->description() << "_" << node_prop.first << "_" << name_appendix;
      node_lookup.insert({{n,node_prop.first}, p.add_variable(Variable(name.str(), std::get<0>(node_prop.second), std::get<1>(node_prop.second), std::get<2>(node_prop.second)))});
    }
    return false;
  };

  g.conditional_bfs_all_components(
    edge_function,
    node_function
  );

  g.for_nodes([&node_generator, &p, &edge_lookup, &node_lookup](const Node* n){
    for(auto data : node_generator(n)){
      p.add_constraint(
        Constraint(
          std::get<0>(data),
          std::get<1>(data),
          lp_generator::lhs_from_data(std::get<2>(data), edge_lookup, node_lookup),
          std::get<3>(data)
        )
      );
    }
  });

  g.for_edges([&edge_generator, &p, &edge_lookup, &node_lookup](const Edge* e){
    for(auto data : edge_generator(e)){
      p.add_constraint(
        Constraint(
          std::get<0>(data),
          std::get<1>(data),
          lp_generator::lhs_from_data(std::get<2>(data), edge_lookup, node_lookup),
          std::get<3>(data)
        )
      );
    }
  });

  return {node_lookup, edge_lookup};
}
