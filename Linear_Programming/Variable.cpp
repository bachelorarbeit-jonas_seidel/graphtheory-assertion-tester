#include "Variable.h"

bool Variable::VERBOSE_IDENT = true;


Variable::Variable(const Variable& var) : _description(var._description), _integrality(var._integrality), _lower_bound(var._lower_bound), _upper_bound(var._upper_bound), _fixed(false), _value(var._value) {}

Variable::Variable(Variable&& var) : _description(std::move(var._description)), _integrality(std::move(var._integrality)), _lower_bound(std::move(var._lower_bound)), _upper_bound(std::move(var._upper_bound)), _fixed(false), _value(std::move(var._value)) {}

//Variable::Variable(integrality integrality, std::pair<bool, double> lower_bound, std::pair<bool, double> upper_bound, double value) : _integrality(integrality), _lower_bound(lower_bound), _upper_bound(upper_bound), _value(value) {}

Variable::Variable(std::string description, integrality integrality, std::pair<bool, double> lower_bound, std::pair<bool, double> upper_bound, double value) : _description(description), _integrality(integrality), _lower_bound(lower_bound), _upper_bound(upper_bound), _fixed(false), _value(value) {}


std::string Variable::description() const {
  return this->_description;
}

integrality Variable::is_integral() const {
  return this->_integrality;
}

std::pair<bool, double> Variable::lower_bound() const {
  return this->_lower_bound;
}

std::pair<bool, double> Variable::upper_bound() const {
  return this->_upper_bound;
}

SCIP_Real Variable::computational_lower_bound(SCIP* scip) const {
  auto lower = this->lower_bound();
  if(!lower.first){
    return -SCIPinfinity(scip);
  }
  return lower.second;
}

SCIP_Real Variable::computational_upper_bound(SCIP* scip) const {
  auto upper = this->upper_bound();
  if(!upper.first){
    return SCIPinfinity(scip);
  }
  return upper.second;
}


bool Variable::is_fixed() const {
  return this->_fixed;
}

double& Variable::value() {
  assert(this->is_fixed());
  return this->_value;
}

double Variable::value() const {
  assert(this->is_fixed());
  return this->_value;
}

SCIP_VAR* Variable::computational_var(SCIP* scip, double direction_coefficient) const {
  char name[SCIP_MAXSTRLEN];

  SCIP_VAR* var;
  SCIPsnprintf(name, SCIP_MAXSTRLEN, this->description().c_str());
  SCIP_CALL_ABORT( SCIPcreateVarBasic(scip, &var, name, this->computational_lower_bound(scip), this->computational_upper_bound(scip), direction_coefficient,
                          (this->is_integral() ? SCIP_VARTYPE_INTEGER : SCIP_VARTYPE_CONTINUOUS)));
  return var;
}

std::ostream& operator<<(std::ostream& os, const Variable& var){
  if(var.is_fixed()){
    os << var.value();
  }else{
    os << var.description();
  }
  return os;
}
