#ifndef COEFFICIENT_H
#define COEFFICIENT_H

#include <cstddef>
#include <map>
#include <iostream>
#include <string>
#include <cassert>
#include <stdexcept>
#include <cmath>

#include "Monomial.h"
#include "Variable.h"

class Coefficient{
  std::map<Monomial, double, Monomial::less> _coeff;

  Coefficient(std::map<Monomial, double, Monomial::less> coeff);
public:
  friend Coefficient operator+(double val, Coefficient coeff);
  friend Coefficient operator+(Coefficient coeffa, Coefficient coeffb);
  friend Coefficient operator*(double val, Coefficient coef);
  friend Coefficient operator*(Coefficient ceoffa, Coefficient coeffb);

  friend Coefficient operator-(double val, Coefficient coeff);
  friend Coefficient operator-(Coefficient coeffa, Coefficient coeffb);
  friend Coefficient operator-(Coefficient);

  friend std::ostream& operator<<(std::ostream& os, Coefficient coeff);

  Coefficient();
  Coefficient(const Coefficient& other) : _coeff(other._coeff){}
  Coefficient(double initial_val);
  Coefficient(const Coefficient& other, const std::unordered_map<const Variable*, Variable*>& variable_lookup){
    for(auto [monom, coeff_sec_order] : other._coeff){
      this->_coeff.insert({Monomial(monom, variable_lookup), coeff_sec_order});
    }
  }
  Coefficient(Coefficient&& other) : _coeff(std::move(other._coeff)){}

  void operator=(const Coefficient& other){
    this->_coeff = other._coeff;
  }
  void operator=(Coefficient&& other){
    this->_coeff = std::move(other._coeff);
  }

  bool is_non_zero() const ;
  bool is_unity() const ;
  double value() const ;

  static const bool always_add_sign = true;
};

std::ostream& operator<<(std::ostream& os, Coefficient coeff);

#endif
