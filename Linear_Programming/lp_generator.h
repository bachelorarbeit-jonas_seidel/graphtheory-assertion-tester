#ifndef LP_GENERATOR_H
#define LP_GENERATOR_H

#include <string>
#include <sstream>
#include <tuple>
#include <vector>
#include <map>
#include <unordered_map>
#include <functional>

#include "../Linear_Programming/Linear_Program.h"
#include "../Graphtheory/Graph.h"

using lhs_constraint_data =
std::pair<
  std::vector<
    std::pair<
      std::pair<const Node*,std::string>,
      double
    >
  >,
  std::vector<
    std::pair<
      std::pair<const Edge*,std::string>,
      double
    >
  >
>;

using constraint_data =
std::tuple<
  std::string,
  relation,
  lhs_constraint_data,
  double
>;


using node_constraint_data_generator =
std::function<
  std::vector<constraint_data>(const Node*)
>;

using edge_constraint_data_generator =
std::function<
  std::vector<constraint_data>(const Edge*)
>;

class lp_generator{
  static std::unordered_map<Variable*, Coefficient> lhs_from_data(
    lhs_constraint_data& data,

    const std::map<
      std::pair<const Edge*,std::string>,
      std::pair<Variable*, size_t>
    >& edge_variable_lookup,

    const std::map<
      std::pair<const Node*,std::string>,
      std::pair<Variable*, size_t>
    >& node_variable_lookup
  );

public:

  static
  std::pair<
    std::map<
      std::pair<const Node*, std::string>,
      std::pair<Variable*, size_t>
    >,
    std::map<
      std::pair<const Edge*, std::string>,
      std::pair<Variable*, size_t>
    >
  >
  grow_from_graph(
    Linear_Program& lp,

    const Graph& g,

    edge_constraint_data_generator edge_generator,

    node_constraint_data_generator node_generator,

    const std::vector<
      std::pair<
        std::string,
        std::tuple<
          integrality,
          std::pair<bool, double>,
          std::pair<bool, double>
        >
      >
    > edge_var_data,

    const std::vector<
      std::pair<
        std::string,
        std::tuple<
          integrality,
          std::pair<bool, double>,
          std::pair<bool, double>
        >
      >
    > node_var_data,

    std::string name_appendix
  );

};

#endif
