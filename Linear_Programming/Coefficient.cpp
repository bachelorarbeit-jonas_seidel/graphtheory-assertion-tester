#include "Coefficient.h"

Coefficient::Coefficient(std::map<Monomial, double, Monomial::less> coeff) : _coeff(coeff) {}

Coefficient::Coefficient(){}
Coefficient::Coefficient(double initial_val) : _coeff({{{}, initial_val}}) {}


bool Coefficient::is_non_zero() const {
  for(const std::pair<const Monomial, double>& m : this->_coeff){
    if(std::abs(m.second) > 1e-100){
      return true;
    }
  }
  return false;
}

bool Coefficient::is_unity() const {
  auto search_result = this->_coeff.find({});
  if(search_result != this->_coeff.end()){
    if(std::abs(search_result->second - 1) > 1e-100){
      return false;
    }else{

      if( (Coefficient{-1}+(*this)).is_non_zero() ){
        return false;
      }
    }
  }
  return true;
}


double Coefficient::value() const {
  double curr = 0;
  for(std::pair<Monomial, double> pair : this->_coeff){
    curr += pair.second*pair.first.value();
  }
  return curr;
}


// Coefficient operator+(double val, Coefficient coeff){
//   Coefficient tmp (coeff);
//
//   for(std::pair<Monomial, double>& m : tmp){
//     m.second += val;
//   }
//
//   return tmp;
// }
Coefficient operator+(Coefficient coeffa, Coefficient coeffb){
  Coefficient tmp (coeffa);

  for(std::pair<const Monomial, double>& m : coeffb._coeff){
    auto search_result = tmp._coeff.find(m.first);
    if(search_result != coeffb._coeff.end()){
      search_result->second += m.second;
    }else{
      tmp._coeff.insert(m);
    }
  }

  return tmp;
}
Coefficient operator*(double val, Coefficient coeff){
  Coefficient tmp (coeff);

  for(std::pair<const Monomial, double>& m : tmp._coeff){
    m.second *= val;
  }
  return tmp;
}
Coefficient operator*(Coefficient coeffa, Coefficient coeffb){
  Coefficient tmp (coeffa);

  for(std::pair<const Monomial, double>& m : coeffb._coeff){
    for(std::pair<const Monomial, double>& n : coeffa._coeff){
      tmp._coeff.insert({m.first*n.first, m.second*n.second});
    }
  }

  return tmp;
}

//Coefficient operator-(double val, Coefficient coeff);
Coefficient operator-(Coefficient coeffa, Coefficient coeffb){
  Coefficient tmp (coeffa);

  for(std::pair<const Monomial, double>& m : coeffb._coeff){
    auto search_result = tmp._coeff.find(m.first);
    if(search_result != coeffb._coeff.end()){
      search_result->second -= m.second;
    }else{
      tmp._coeff.insert(m);
    }
  }

  return tmp;
}

Coefficient operator-(Coefficient coeff){
  Coefficient tmp;
  for(std::pair<const Monomial, double> m : coeff._coeff){
    m.second = -m.second;
    tmp._coeff.insert(m);
  }

  return tmp;
}

std::ostream& operator<<(std::ostream& os, Coefficient coeff){
  if(coeff.is_non_zero()){
    bool parenthesis = coeff._coeff.size() > 1 /*|| (coeff._coeff.size() > 0 && coeff._coeff.begin()->second < 1e-100)*/;
    if(parenthesis){
      os << "(";
    }
    bool add_plus = Coefficient::always_add_sign;
    for(std::pair<const Monomial, double>& m : coeff._coeff){
      if(std::abs(m.second) > 1e-100){
        if(m.second < 1e-100){
          if(add_plus){
            os << " - ";
          }else{
            os << "-";
          }
        }else if(add_plus){
          os << " + ";
        }
        add_plus = true;

        if(std::abs(m.second-1) < 1e-100){
          os << m.first;
        }else{
          os << std::abs(m.second) << " " << m.first;
        }
      }
    }
    if(parenthesis){
      os << ")";
    }
  }

  return os;
}
